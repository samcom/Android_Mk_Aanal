package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class RateData {

	@SerializedName("data")
	var data: Data? = null

	@SerializedName("error")
	var error: Boolean? = null

	@SerializedName("message")
	var message: String? = null

	class Data {

		@SerializedName("result")
		var result: ArrayList<Array<String>>? = null
	}
}


