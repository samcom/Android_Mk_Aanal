package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class WithdrawData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: Result? = null

		class Result {

			@SerializedName("per_page")
			val perPage: Int? = null

			@SerializedName("data")
			val data: List<DataItem>? = null

			@SerializedName("last_page")
			val lastPage: Int? = null

			@SerializedName("next_page_url")
			val nextPageUrl: Any? = null

			@SerializedName("prev_page_url")
			val prevPageUrl: Any? = null

			@SerializedName("first_page_url")
			val firstPageUrl: String? = null

			@SerializedName("path")
			val path: String? = null

			@SerializedName("total")
			val total: Int? = null

			@SerializedName("last_page_url")
			val lastPageUrl: String? = null

			@SerializedName("from")
			val from: Int? = null

			@SerializedName("links")
			val links: List<LinksItem?>? = null

			@SerializedName("to")
			val to: Int? = null

			@SerializedName("current_page")
			val currentPage: Int? = null

			class DataItem {

				@SerializedName("amount")
				val amount: String? = null

				@SerializedName("transfer_number")
				val transferNumber: String? = null

				@SerializedName("created_at")
				val createdAt: String? = null

				@SerializedName("remark")
				val remark: Any? = null

				@SerializedName("request_number")
				val requestNumber: String? = null

				@SerializedName("accept_date")
				val acceptDate: Any? = null

				@SerializedName("updated_at")
				val updatedAt: String? = null

				@SerializedName("user_id")
				val userId: Int? = null

				@SerializedName("request_date")
				val requestDate: String? = null

				@SerializedName("receipt")
				val receipt: Any? = null

				@SerializedName("id")
				val id: Int? = null

				@SerializedName("payment_method")
				val paymentMethod: String? = null

				@SerializedName("status")
				val status: String? = null
			}

			class LinksItem {

				@SerializedName("active")
				val active: Boolean? = null

				@SerializedName("label")
				val label: String? = null

				@SerializedName("url")
				val url: Any? = null
			}
		}
	}
}


