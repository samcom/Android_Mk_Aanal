package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class LoginData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {
		@SerializedName("userDetails")
		val userDetails: UserDetails? = null

		class UserDetails {

			@SerializedName("account_number")
			val accountNumber: String? = null

			@SerializedName("role")
			val role: String? = null

			@SerializedName("account_holder_name")
			val accountHolderName: String? = null

			@SerializedName("pin_code")
			val pinCode: String? = null

			@SerializedName("available_balance")
			val availableBalance: String? = null

			@SerializedName("phonepay_number")
			val phonepayNumber: String? = null

			@SerializedName("district_name")
			val districtName: String? = null

			@SerializedName("googlepay_number")
			val googlepayNumber: String? = null

			@SerializedName("flat_number")
			val flatNumber: String? = null

			@SerializedName("address_lane2")
			val addressLane2: String? = null

			@SerializedName("bank_name")
			val bankName: String? = null

			@SerializedName("address_lane1")
			val addressLane1: String? = null

			@SerializedName("id")
			val id: Int? = null

			@SerializedName("login_msg")
			val loginMsg: String? = null

			@SerializedName("state")
			val state: String? = null

			@SerializedName("email")
			val email: String? = null

			@SerializedName("area")
			val area: String? = null

			@SerializedName("paytm_number")
			val paytmNumber: String? = null

			@SerializedName("security_pin")
			val securityPin: String? = null

			@SerializedName("betting")
			val betting: String? = null

			@SerializedName("noti_status")
			val notiStatus: String? = null

			@SerializedName("email_verified_at")
			val emailVerifiedAt: String? = null

			@SerializedName("token")
			val token: String? = null

			@SerializedName("ifsc_code")
			val ifscCode: String? = null

			@SerializedName("transfer")
			val transfer: String? = null

			@SerializedName("name")
			val name: String? = null

			@SerializedName("branch_address")
			val branchAddress: String? = null

			@SerializedName("phone_number")
			val phoneNumber: String? = null

			@SerializedName("username")
			val username: String? = null

			@SerializedName("status")
			val status: String? = null

			@SerializedName("profile_picture")
			val profilePicture: String? = null
		}
	}
}




