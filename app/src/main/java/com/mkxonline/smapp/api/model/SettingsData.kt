package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class SettingsData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: Result? = null

		class Result {

			@SerializedName("app_link")
			val appLink: String? = null

			@SerializedName("website")
			val website: String? = null

			@SerializedName("youtube")
			val youtube: Any? = null

			@SerializedName("account_number")
			val accountNumber: Any? = null

			@SerializedName("min_transfer")
			val minTransfer: Int? = null

			@SerializedName("account_holder_name")
			val accountHolderName: Any? = null

			@SerializedName("latitude")
			val latitude: Any? = null

			@SerializedName("min_bid_amount")
			val minBidAmount: String? = null

			@SerializedName("created_at")
			val createdAt: String? = null

			@SerializedName("is_maintainence")
			val isMaintainence: Int? = null

			@SerializedName("withdrawal_open_time")
			val withdrawalOpenTime: String? = null

			@SerializedName("instagram")
			val instagram: Any? = null

			@SerializedName("google_upi_id")
			val googleUpiId: String? = null

			@SerializedName("global_betting")
			val globalBetting: String? = null

			@SerializedName("max_transfer")
			val maxTransfer: Int? = null

			@SerializedName("email_2")
			val email2: Any? = null

			@SerializedName("twitter")
			val twitter: Any? = null

			@SerializedName("max_bid_amount")
			val maxBidAmount: String? = null

			@SerializedName("email_1")
			val email1: String? = null

			@SerializedName("updated_at")
			val updatedAt: String? = null

			@SerializedName("id")
			val id: Int? = null

			@SerializedName("telegram_number")
			val telegramNumber: String? = null

			@SerializedName("whatsapp_number")
			val whatsappNumber: String? = null

			@SerializedName("longitude")
			val longitude: Any? = null

			@SerializedName("address")
			val address: String? = null

			@SerializedName("max_withdrawal")
			val maxWithdrawal: Int? = null

			@SerializedName("facebook")
			val facebook: Any? = null

			@SerializedName("phonepe_upi_id")
			val phonepeUpiId: String? = null

			@SerializedName("google_plus")
			val googlePlus: Any? = null

			@SerializedName("min_deposit")
			val minDeposit: Int? = null

			@SerializedName("withdrawal_close_time")
			val withdrawalCloseTime: String? = null

			@SerializedName("other_upi_id")
			val otherUpiId: String? = null

			@SerializedName("ifsc_code")
			val ifscCode: Any? = null

			@SerializedName("landline_1")
			val landline1: Any? = null

			@SerializedName("landline_2")
			val landline2: Any? = null

			@SerializedName("account_id")
			val accountId: Int? = null

			@SerializedName("welcome_bonus")
			val welcomeBonus: String? = null

			@SerializedName("maintainence_msg")
			val maintainenceMsg: String? = null

			@SerializedName("share_message")
			val shareMessage: String? = null

			@SerializedName("min_withdrawal")
			val minWithdrawal: Int? = null

			@SerializedName("mobile_number")
			val mobileNumber: String? = null

			@SerializedName("max_deposit")
			val maxDeposit: Int? = null
		}
	}
}




