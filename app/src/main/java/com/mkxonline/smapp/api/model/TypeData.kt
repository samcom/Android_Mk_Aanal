package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TypeData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: List<ResultItem>? = null

		class ResultItem : Serializable {

			@SerializedName("updated_at")
			val updatedAt: String? = null

			@SerializedName("created_at")
			val createdAt: String? = null

			@SerializedName("id")
			val id: Int? = null

			@SerializedName("type")
			val type: String? = null
		}
	}
}
