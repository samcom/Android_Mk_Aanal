package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class WinHistoryData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {
		@SerializedName("result")
		val result: Result? = null

		class Result {

			@SerializedName("per_page")
			val perPage: Int? = null

			@SerializedName("data")
			val data: List<DataItem>? = null

			@SerializedName("last_page")
			val lastPage: Int? = null

			@SerializedName("next_page_url")
			val nextPageUrl: Any? = null

			@SerializedName("prev_page_url")
			val prevPageUrl: Any? = null

			@SerializedName("first_page_url")
			val firstPageUrl: String? = null

			@SerializedName("path")
			val path: String? = null

			@SerializedName("total")
			val total: Int? = null

			@SerializedName("last_page_url")
			val lastPageUrl: String? = null

			@SerializedName("from")
			val from: Int? = null

			@SerializedName("links")
			val links: List<LinksItem?>? = null

			@SerializedName("to")
			val to: Int? = null

			@SerializedName("current_page")
			val currentPage: Int? = null

			class LinksItem {

				@SerializedName("active")
				val active: Boolean? = null

				@SerializedName("label")
				val label: String? = null

				@SerializedName("url")
				val url: Any? = null
			}

			class DataItem {

				@SerializedName("close_digit")
				val closeDigit: String? = null

				@SerializedName("open_digit")
				val openDigit: String? = null

				@SerializedName("wining_amount")
				val winingAmount: String? = null

				@SerializedName("session")
				val session: String? = null

				@SerializedName("is_win")
				val isWin: String? = null

				@SerializedName("created_at")
				val createdAt: String? = null

				@SerializedName("tx_id")
				val txId: Int? = null

				@SerializedName("result_declare")
				val resultDeclare: String? = null

				@SerializedName("type")
				val type: String? = null

				@SerializedName("game_type_id")
				val gameTypeId: Int? = null

				@SerializedName("points")
				val points: String? = null

				@SerializedName("open_pana")
				val openPana: String? = null

				@SerializedName("game_name")
				val gameName: String? = null

				@SerializedName("updated_at")
				val updatedAt: String? = null

				@SerializedName("user_id")
				val userId: Int? = null

				@SerializedName("result_id")
				val resultId: Int? = null

				@SerializedName("close_pana")
				val closePana: String? = null

				@SerializedName("id")
				val id: Int? = null

				@SerializedName("game_id")
				val gameId: Int? = null

				@SerializedName("bid_date")
				val bidDate: String? = null
			}
		}
	}
}








