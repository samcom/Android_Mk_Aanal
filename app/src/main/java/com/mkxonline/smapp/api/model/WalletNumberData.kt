package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class WalletNumberData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: Result? = null

		class Result {

			@SerializedName("account_number")
			val accountNumber: Any? = null

			@SerializedName("role")
			val role: String? = null

			@SerializedName("account_holder_name")
			val accountHolderName: Any? = null

			@SerializedName("pin_code")
			val pinCode: Any? = null

			@SerializedName("created_at")
			val createdAt: String? = null

			@SerializedName("available_balance")
			val availableBalance: String? = null

			@SerializedName("phonepay_number")
			val phonepayNumber: Any? = null

			@SerializedName("district_name")
			val districtName: Any? = null

			@SerializedName("googlepay_number")
			val googlepayNumber: Any? = null

			@SerializedName("password")
			val password: String? = null

			@SerializedName("updated_at")
			val updatedAt: String? = null

			@SerializedName("flat_number")
			val flatNumber: Any? = null

			@SerializedName("address_lane2")
			val addressLane2: Any? = null

			@SerializedName("bank_name")
			val bankName: Any? = null

			@SerializedName("address_lane1")
			val addressLane1: Any? = null

			@SerializedName("id")
			val id: Int? = null

			@SerializedName("state")
			val state: Any? = null

			@SerializedName("email")
			val email: String? = null

			@SerializedName("area")
			val area: Any? = null

			@SerializedName("device_id")
			val deviceId: String? = null

			@SerializedName("paytm_number")
			val paytmNumber: String? = null

			@SerializedName("security_pin")
			val securityPin: String? = null

			@SerializedName("betting")
			val betting: String? = null

			@SerializedName("email_verified_at")
			val emailVerifiedAt: Any? = null

			@SerializedName("profile_picture")
			val profilePicture: String? = null

			@SerializedName("deleted_at")
			val deletedAt: Any? = null

			@SerializedName("ifsc_code")
			val ifscCode: Any? = null

			@SerializedName("transfer")
			val transfer: String? = null

			@SerializedName("device_token")
			val deviceToken: String? = null

			@SerializedName("name")
			val name: String? = null

			@SerializedName("branch_address")
			val branchAddress: Any? = null

			@SerializedName("phone_number")
			val phoneNumber: String? = null

			@SerializedName("username")
			val username: String? = null

			@SerializedName("status")
			val status: String? = null
		}
	}

}




