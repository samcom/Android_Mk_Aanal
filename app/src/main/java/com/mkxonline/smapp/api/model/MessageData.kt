package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class MessageData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data
}


