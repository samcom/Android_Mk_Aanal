package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ListData {

	@SerializedName("error")
	var error: Boolean? = null

	@SerializedName("message")
	var message: String? = null

	@SerializedName("data")
	var data: Data? = null

	class Data {

		@SerializedName("result")
		var result: List<ResultItem>? = null

		class ResultItem : Serializable {

			@SerializedName("game_name")
			var gameName: String? = null

			@SerializedName("game_name_hindi")
			var gameNameHindi: String? = null

			@SerializedName("open_time")
			var openTime: String? = null

			@SerializedName("open_today")
			var openToday: String? = null

			@SerializedName("id")
			var id: Int? = null

			@SerializedName("close_time")
			var closeTime: String? = null

			@SerializedName("result")
			var result: String? = null
		}
	}
}