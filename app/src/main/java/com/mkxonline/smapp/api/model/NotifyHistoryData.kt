package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class NotifyHistoryData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("per_page")
		val perPage: Int? = null

		@SerializedName("data")
		val data: List<DataItem>? = null

		@SerializedName("last_page")
		val lastPage: Int? = null

		@SerializedName("next_page_url")
		val nextPageUrl: Any? = null

		@SerializedName("prev_page_url")
		val prevPageUrl: Any? = null

		@SerializedName("first_page_url")
		val firstPageUrl: String? = null

		@SerializedName("path")
		val path: String? = null

		@SerializedName("total")
		val total: Int? = null

		@SerializedName("last_page_url")
		val lastPageUrl: String? = null

		@SerializedName("from")
		val from: Int? = null

		@SerializedName("links")
		val links: List<LinksItem?>? = null

		@SerializedName("to")
		val to: Int? = null

		@SerializedName("current_page")
		val currentPage: Int? = null

		class DataItem {

			@SerializedName("msg")
			val msg: String? = null

			@SerializedName("updated_at")
			val updatedAt: String? = null

			@SerializedName("user_id")
			val userId: Int? = null

			@SerializedName("created_at")
			val createdAt: String? = null

			@SerializedName("id")
			val id: Int? = null

			@SerializedName("type")
			val type: String? = null

			@SerializedName("title")
			val title: String? = null
		}

		class LinksItem {

			@SerializedName("active")
			val active: Boolean? = null

			@SerializedName("label")
			val label: String? = null

			@SerializedName("url")
			val url: Any? = null
		}
	}
}






