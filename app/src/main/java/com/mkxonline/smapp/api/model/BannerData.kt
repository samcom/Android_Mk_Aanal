package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class BannerData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: ArrayList<ResultItem>? = null

		class ResultItem {

			@SerializedName("image")
			val image: String? = null

			@SerializedName("updated_at")
			val updatedAt: String? = null

			@SerializedName("image_order")
			val imageOrder: String? = null

			@SerializedName("created_at")
			val createdAt: String? = null

			@SerializedName("id")
			val id: Int? = null

			@SerializedName("status")
			val status: String? = null
		}
	}
}




