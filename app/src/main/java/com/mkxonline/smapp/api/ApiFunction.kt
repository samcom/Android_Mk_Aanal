package com.mkxonline.smapp.api

import com.mkxonline.smapp.api.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
interface ApiFunction {
    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("login")
    fun login(@Field("phone_number") phone : String,
              @Field("password") password : String,
              @Field("device_token") device_token : String,
              @Field("device_id") device_type : String): Call<LoginData?>?
    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("register")
    fun register(@Field("username") username : String,
                 @Field("phone_number") phone_number : String,
                 @Field("password") password : String,
                 @Field("device_token") device_token : String,
                 @Field("device_id") device_type : String): Call<LoginData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("forgotPassword")
    fun forgotPassword(@Field("phone_number") phone : String,
              @Field("new_password") password : String): Call<MessageData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("changePassword")
    fun changePassword(@Header("Authorization") auth: String,
                       @Field("old_password") old_password : String,
                       @Field("new_password") new_password : String): Call<MessageData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("sendotp")
    fun sendotp(@Field("phone_number") phone_number : String,
                @Field("type") type : String): Call<MessageData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("verifyotp")
    fun verifyotp(@Field("phone_number") phone_number : String,
                  @Field("otp_string") otp_string : String): Call<MessageData?>?

    @Headers("Accept: application/json")
    @GET("slideBanner")
    fun slideBanner(@Header("Authorization") auth : String): Call<BannerData?>?

    @Headers("Accept: application/json")
    @GET("gameList")
    fun gameList(@Header("Authorization") auth : String): Call<ListData?>?

    @Headers("Accept: application/json")
    @GET("starlineGameList")
    fun starlineGameList(@Header("Authorization") auth : String): Call<ListData?>?

    @Headers("Accept: application/json")
    @GET("gameType")
    fun gameType(@Header("Authorization") auth : String): Call<TypeData?>?

    @Headers("Accept: application/json")
    @GET("starlineGameType")
    fun starlineGameType(@Header("Authorization") auth : String): Call<TypeData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("gameDetails")
    fun gameDetails(@Header("Authorization") auth : String,
                    @Field("game_id") game_id : Int): Call<ListDetailData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("starlineGameDetails")
    fun starlineGameDetails(@Header("Authorization") auth : String,
                            @Field("game_id") game_id : Int): Call<ListDetailData?>?
    @Headers("Accept: application/json")
    @GET("gameRate")
    fun gameRate(@Header("Authorization") auth : String): Call<RateData?>?

    @Headers("Accept: application/json")
    @GET("starlineGameRate")
    fun starlineGameRate(@Header("Authorization") auth : String): Call<RateData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("resultChart")
    fun resultChart(@Header("Authorization") auth: String,
                    @Field("game_id") game_id : String): Call<ResultChartData?>?

    @Headers("Accept: application/json")
    @GET("howtoPlay")
    fun howtoPlay(@Header("Authorization") auth: String): Call<HowtoplayData?>?

    @Headers("Accept: application/json")
    @GET("starlineResultChart")
    fun starlineResultChart(@Header("Authorization") auth: String): Call<StarLineChartData?>?

    @Headers("Accept: application/json")
    @GET("singleDigit")
    fun singleDigit(@Header("Authorization") auth : String): Call<DigitPanaData?>?

    @Headers("Accept: application/json")
    @GET("jodiDigit")
    fun jodiDigit(@Header("Authorization") auth : String): Call<DigitPanaData?>?

    @Headers("Accept: application/json")
    @GET("singlePana")
    fun singlePana(@Header("Authorization") auth : String): Call<DigitPanaData?>?

    @Headers("Accept: application/json")
    @GET("doublePana")
    fun doublePana(@Header("Authorization") auth : String): Call<DigitPanaData?>?

    @Headers("Accept: application/json")
    @GET("triplePana")
    fun triplePana(@Header("Authorization") auth : String): Call<DigitPanaData?>?

    @Headers("Accept: application/json")
    @GET("halfSangam")
    fun halfSangam(@Header("Authorization") auth : String): Call<SangamData?>?

    @Headers("Accept: application/json")
    @GET("fullSangam")
    fun fullSangam(@Header("Authorization") auth : String): Call<SangamData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("createBid")
    fun createBid(@Header("Authorization") auth : String,
                 @Field("game_id") game_id : Int,
                 @Field("game_type_id") game_type_id : Int,
                 @Field("bid_list") bid_list : String): Call<MessageData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("starlineCreateBid")
    fun starlineCreateBid(@Header("Authorization") auth : String,
                 @Field("game_id") game_id : Int,
                 @Field("game_type_id") game_type_id : Int,
                 @Field("bid_list") bid_list : String): Call<MessageData?>?

    @Headers("Accept: application/json")
    @GET("dashboard")
    fun dashboard(@Header("Authorization") auth: String): Call<DashboardData?>?

    @Headers("Accept: application/json")
    @GET("settings")
    fun settings(@Header("Authorization") auth: String): Call<SettingsData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("bidHistory")
    fun bidHistory(@Header("Authorization") auth : String,
                   @Query("page") page : String,
                   @Field("from_date") from_date : String,
                   @Field("to_date") to_date : String): Call<BidHistoryData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("starlineBidHistory")
    fun starlineBidHistory(@Header("Authorization") auth : String,
                           @Query("page") page : String,
                           @Field("from_date") from_date : String,
                           @Field("to_date") to_date : String): Call<BidHistoryData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("winHistory")
    fun winHistory(@Header("Authorization") auth : String,
                   @Query("page") page : String,
                   @Field("from_date") from_date : String,
                   @Field("to_date") to_date : String): Call<WinHistoryData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("starlineWinHistory")
    fun starlineWinHistory(@Header("Authorization") auth : String,
                           @Query("page") page : String,
                           @Field("from_date") from_date : String,
                           @Field("to_date") to_date : String): Call<WinHistoryData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("WalletsNumber")
    fun walletsNumber(@Header("Authorization") auth: String,
                       @Field("wallet_type") wallet_type : String,
                       @Field("phone_number") phone_number : String): Call<WalletNumberData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("transfer")
    fun transfer(@Header("Authorization") auth: String,
                       @Field("phone_number") phone_number : String,
                       @Field("amount") amount : String): Call<MessageData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("addFund")
    fun addFund(@Header("Authorization") auth: String,
                       @Field("transaction_id") transaction_id : String,
                       @Field("payment_method") payment_method : Int,
                       @Field("amount") amount : String): Call<MessageData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("withdrawRequest")
    fun withdrawRequest(@Header("Authorization") auth: String,
                       @Field("payment_method") payment_method : String,
                       @Field("transfer_number") transfer_number : String,
                       @Field("amount") amount : String): Call<MessageData?>?

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("updateBankDetails")
    fun updateBankDetails(@Header("Authorization") auth: String,
                       @Field("account_holder_name") account_holder_name : String,
                       @Field("account_number") account_number : String,
                       @Field("ifsc_code") ifsc_code : String,
                       @Field("bank_name") bank_name : String,
                       @Field("branch_address") branch_address : String): Call<MessageData?>?

    @Headers("Accept: application/json")
    @GET("walletHistory")
    fun walletHistory(@Header("Authorization") auth: String,@Query("page") page : String): Call<WalletHistoryData?>?

    @Headers("Accept: application/json")
    @GET("withdrawHistory")
    fun withdrawHistory(@Header("Authorization") auth: String,@Query("page") page : String): Call<WithdrawData?>?

    @Headers("Accept: application/json")
    @Multipart
    @POST("updateProfile")
    fun updateProfile(@Header("Authorization") auth: String,
                      @Part("user_name") user_name: RequestBody,
                      @Part image: MultipartBody.Part): Call<MessageData?>?

}
