package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class ResultChartData{

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: List<ResultItem>? = null

		class ResultItem {

			@SerializedName("date")
			val date: String? = null

			@SerializedName("color")
			val color: String? = null

			@SerializedName("left")
			val left: String? = null

			@SerializedName("center")
			val center: String? = null

			@SerializedName("right")
			val right: String? = null
		}
	}
}
