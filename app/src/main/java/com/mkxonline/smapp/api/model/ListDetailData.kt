package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class ListDetailData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: Result? = null

		class Result {

			@SerializedName("game_name")
			val gameName: String? = null

			@SerializedName("game_name_hindi")
			val gameNameHindi: String? = null

			@SerializedName("open_time")
			val openTime: String? = null

			@SerializedName("id")
			val id: Int? = null

			@SerializedName("close_time")
			val closeTime: String? = null
		}
	}
}



