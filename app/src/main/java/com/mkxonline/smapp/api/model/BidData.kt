package com.mkxonline.smapp.api.model

class BidData {

    var session: String = ""
        get() = field
        set(value) {
            field = value
        }

    var open_pana: String = ""
        get() = field
        set(value) {
            field = value
        }

    var close_pana: String = ""
        get() = field
        set(value) {
            field = value
        }

    var open_digit: String = ""
        get() = field
        set(value) {
            field = value
        }

    var close_digit: String = ""
        get() = field
        set(value) {
            field = value
        }

    var points: String = ""
        get() = field
        set(value) {
            field = value
        }
}