package com.mkxonline.smapp.api.model

class StarLineBidData {

    var open_pana: String = ""
        get() = field
        set(value) {
            field = value
        }

    var open_digit: String = ""
        get() = field
        set(value) {
            field = value
        }

    var points: String = ""
        get() = field
        set(value) {
            field = value
        }
}