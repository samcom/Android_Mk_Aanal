package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class DigitPanaData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: List<ResultItem>? = null

		@SerializedName("min_point")
		val minPoint: String? = null

		@SerializedName("max_point")
		val maxPoint: String? = null

		class ResultItem {

			@SerializedName("number")
			val number: String? = null
		}
	}
}




