package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class HowtoplayData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: Result? = null

		class Result {

			@SerializedName("link")
			val link: String? = null

			@SerializedName("text")
			val text: String? = null
		}
	}
}
