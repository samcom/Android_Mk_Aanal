package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class WalletHistoryData {

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: Result? = null

		class Result {

			@SerializedName("per_page")
			val perPage: Int? = null

			@SerializedName("data")
			val data: List<DataItem>? = null

			@SerializedName("last_page")
			val lastPage: Int? = null

			@SerializedName("next_page_url")
			val nextPageUrl: String? = null

			@SerializedName("prev_page_url")
			val prevPageUrl: Any? = null

			@SerializedName("first_page_url")
			val firstPageUrl: String? = null

			@SerializedName("path")
			val path: String? = null

			@SerializedName("total")
			val total: Int? = null

			@SerializedName("last_page_url")
			val lastPageUrl: String? = null

			@SerializedName("from")
			val from: Int? = null

			@SerializedName("links")
			val links: List<LinksItem?>? = null

			@SerializedName("to")
			val to: Int? = null

			@SerializedName("current_page")
			val currentPage: Int? = null

			class DataItem {

				@SerializedName("transaction_id")
				val transactionId: Any? = null

				@SerializedName("date")
				val date: String? = null

				@SerializedName("tx_req_number")
				val txReqNumber: String? = null

				@SerializedName("amount")
				val amount: String? = null

				@SerializedName("transaction_note")
				val transactionNote: String? = null

				@SerializedName("created_at")
				val createdAt: String? = null

				@SerializedName("reject_reason")
				val rejectReason: Any? = null

				@SerializedName("type")
				val type: String? = null

				@SerializedName("auto_deposit")
				val autoDeposit: String? = null

				@SerializedName("transfer_note")
				val transferNote: String? = null

				@SerializedName("updated_at")
				val updatedAt: String? = null

				@SerializedName("user_id")
				val userId: Int? = null

				@SerializedName("result_id")
				val resultId: Int? = null

				@SerializedName("admin_id")
				val adminId: Int? = null

				@SerializedName("id")
				val id: Int? = null

				@SerializedName("payment_method")
				val paymentMethod: String? = null

				@SerializedName("game_flag")
				val gameFlag: String? = null

				@SerializedName("status")
				val status: String? = null
			}

			class LinksItem {

				@SerializedName("active")
				val active: Boolean? = null

				@SerializedName("label")
				val label: String? = null

				@SerializedName("url")
				val url: Any? = null
			}
		}
	}
}
