package com.mkxonline.smapp.api.model

import com.google.gson.annotations.SerializedName

class StarLineChartData {

	@SerializedName("game_list")
	val gameList: List<GameListItem>? = null

	@SerializedName("data")
	val data: Data? = null

	@SerializedName("error")
	val error: Boolean? = null

	@SerializedName("message")
	val message: String? = null

	class Data {

		@SerializedName("result")
		val result: List<ResultItem>? = null

		class ResultItem {

			@SerializedName("date")
			val date: String? = null

			@SerializedName("chart")
			val chart: List<ChartItem?>? = null

			class ChartItem{

				@SerializedName("pana")
				val pana: String? = null

				@SerializedName("game_name")
				val gameName: String? = null

				@SerializedName("digit")
				val digit: String? = null
			}
		}
	}

	class GameListItem{

		@SerializedName("game_name")
		val gameName: String? = null

		@SerializedName("id")
		val id: Int? = null
	}
}








