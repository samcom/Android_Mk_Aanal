package com.mkxonline.smapp.apputil.fcm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.mkxonline.smapp.R
import com.mkxonline.smapp.ui.activity.MainActivity
import com.mkxonline.smapp.apputil.SessionManager
import java.util.*

class NotifyingService : FirebaseMessagingService() {

    private val TAG = "NotifyingService"
    private lateinit var helper: SessionManager
    private lateinit var notificationManager: NotificationManager
    private val NOTIFICATION_CHANNEL_ID = "121"

    companion object {
        fun subscribeTopic() {
            FirebaseMessaging.getInstance().subscribeToTopic("isWithdraw").addOnSuccessListener {
                Log.d("NotifyingService","Subscribed")
            }.addOnFailureListener {
                Log.d("NotifyingService","Fail")
            }
        }

        fun unsubscribeTopic() {
            FirebaseMessaging.getInstance().unsubscribeFromTopic("isWithdraw").addOnSuccessListener {
                Log.d("NotifyingService","unSubscribed")
            }.addOnFailureListener {
                Log.d("NotifyingService","Fail")
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        helper = SessionManager(this)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "Bundle data: " + remoteMessage.data)
        Log.d(TAG, "Notification data: " + remoteMessage.notification)

        if (remoteMessage.data.isNotEmpty()) {
            val data = remoteMessage.data
            val title = data["title"]
            val body = data["body"]
            sendNotification(title,body)
        } else if (remoteMessage.notification != null) {
            sendNotification(remoteMessage.notification!!.title, remoteMessage.notification!!.body)
        }
    }

    private fun sendNotification(title: String?, body: String?) {

        Log.e(TAG, "sendNotification:  $title")

        var intent = Intent(applicationContext, MainActivity::class.java)
        if(title.equals("refresh")){
            intent.action = "REFRESH_BALANCE"
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
            return
        }
//        else if(title.equals("pincode")){
//            intent = Intent(applicationContext, PinActivity::class.java)
//            intent.action = "CHANGE_PIN"
//            LocalBroadcastManager.getInstance(this).sendBroadca1st(intent)
//            return
//        }

        intent = Intent(applicationContext, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

        val defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        Log.e(TAG, "sendNotification:  $defaultSound")
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, resources.getString(
                R.string.app_name), NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.description = "Notifiation"
            notificationChannel.enableLights(true)
            notificationChannel.vibrationPattern = longArrayOf(0, 1000, 500, 1000)
            notificationChannel.lightColor = getColor(R.color.colorAccent)
            notificationChannel.enableVibration(true)
            notificationManager.createNotificationChannel(notificationChannel)
        }

        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notifications)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setOngoing(false)
                .setContentText(body)
                .setSound(defaultSound)
                .setColor(resources.getColor(R.color.colorAccent))
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_MAX)

        val id= Random(System.currentTimeMillis()).nextInt(1000)

        notificationManager.notify(id, notificationBuilder.build())

        Log.e(TAG, "sendNotification:  ${notificationBuilder.build()}")
    }
}