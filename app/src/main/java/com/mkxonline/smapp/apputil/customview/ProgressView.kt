package com.mkxonline.smapp.apputil.customview

import android.app.Activity
import android.app.Dialog
import android.content.Context

import android.view.Gravity
import com.mkxonline.smapp.R

class ProgressView(ctx: Context) {

    val context: Context = ctx
    private var progressDialog: Dialog = Dialog(ctx, R.style.MaterialDialogSheet)

    init {
        progressDialog.setContentView(R.layout.dialog_progress)
        progressDialog.setCancelable(false)
        progressDialog.window!!.setLayout(400, 400)
        progressDialog.window!!.setGravity(Gravity.CENTER)
    }

    fun view() {
        if (!(context as Activity).isDestroyed && !context.isFinishing) {
            if(progressDialog!=null && !progressDialog.isShowing)
                progressDialog.show()
        }
    }

    fun hide() {
        if (!(context as Activity).isDestroyed && !context.isFinishing) {
            if(progressDialog!=null && progressDialog.isShowing)
                progressDialog.dismiss()
        }

    }
}