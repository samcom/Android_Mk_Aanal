package com.mkxonline.smapp.apputil

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import com.mkxonline.smapp.R
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.messaging.FirebaseMessaging
import com.mkxonline.smapp.ui.activity.SignInActivity
import java.io.IOException
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class Utility {
    companion object {

        const val MAIN_URL:String="https://milankalyan.com/api/"
        fun isStringNull(s: String): Boolean{
            return s.trim { it <= ' ' }.isEmpty()
        }

        fun isValidPhoneNo(s: String): Boolean{
            return s.trim { it <= ' ' }.length < 10
        }

        fun isValidPassword(s: String): Boolean{
            return s.trim { it <= ' ' }.length < 4
        }

        fun isInternetConnection(context: Context): Boolean {
            val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            val networkInfo = connMgr!!.activeNetworkInfo
            return networkInfo != null
        }

        fun errorSnackBar(view: View, toastText: String) {
            val snackBar = Snackbar.make(view, toastText, Snackbar.LENGTH_LONG)

            val sView = snackBar.view
            sView.setBackgroundColor(Color.parseColor("#F44336"))
            val params = sView.layoutParams as FrameLayout.LayoutParams
            params.gravity = Gravity.BOTTOM
            params.setMargins(0, 0, 0, 0)
            sView.layoutParams = params
            snackBar.show()
        }

        fun successSnackBar(view: View, toastText: String) {
            val snackBar = Snackbar.make(view, toastText, Snackbar.LENGTH_LONG)

            val sView = snackBar.view
            sView.setBackgroundColor(Color.parseColor("#5AA65D"))
            val params = sView.layoutParams as FrameLayout.LayoutParams
            params.gravity = Gravity.BOTTOM
            params.setMargins(0, 0, 0, 0)
            sView.layoutParams = params
            snackBar.show()
        }

        fun unAuthPopUp(context: Context) {
            val helper = SessionManager(context)
            val builder = AlertDialog.Builder(context)
            builder.setTitle(R.string.title_auth)
            builder.setCancelable(false)
            builder.setMessage(R.string.msg_auth)
            builder.setPositiveButton(android.R.string.ok) { dialog, which ->
                try {
                    FirebaseMessaging.getInstance().deleteToken()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                helper.clearAllPrefs()
                context.startActivity(Intent(context, SignInActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
                dialog.dismiss()
            }
            if (!builder.create().isShowing) builder.create().show()
        }

        fun decimalFormat(s: String): String {
            val formatter = DecimalFormat("0.00")
            return formatter.format(s.toDouble())
        }

        val mFormatDate = SimpleDateFormat("HH:mm", Locale.getDefault())
        val mFormatDate1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val mFormatDate2 = SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.getDefault())
        val mFormatDate3 = SimpleDateFormat("hh:mm a", Locale.getDefault())
        val mFormatDate4 = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())

        const val SINGLE_DIGIT = "Single Digit"
        const val JODI_DIGIT = "Jodi Digit"
        const val SINGLE_PANA = "Single Pana"
        const val DOUBLE_PANA = "Double Pana"
        const val TRIPLE_PANA = "Triple Pana"
        const val HALF_SANGAM = "Half Sangam"
        const val FULL_SANGAM = "Full Sangam"
    }
}