package com.mkxonline.smapp.apputil

import android.content.Context
import android.content.SharedPreferences

class SessionManager(ctx: Context) {
    private var prefs: SharedPreferences? = null

    init {
        prefs = ctx.getSharedPreferences(ctx.packageName, Context.MODE_PRIVATE)
    }

    fun isFirst(): Boolean {
        return prefs!!.getBoolean("first", false)
    }

    fun setFirst(b: Boolean) {
        prefs!!.edit().putBoolean("first", b).apply()
    }

    fun isAppLaunched(): Boolean {
        return prefs!!.getBoolean("firstRun", false)
    }

    fun setAppLaunched(b: Boolean) {
        prefs!!.edit().putBoolean("firstRun", b).apply()
    }

    fun isLogin(): Boolean {
        return prefs!!.getBoolean("isLogin", false)
    }

    fun setLogin(b: Boolean) {
        prefs!!.edit().putBoolean("isLogin", b).apply()
    }

    fun getName(): String? {
        return prefs!!.getString("Name", "")
    }

    fun setName(user: String?) {
        prefs!!.edit().putString("Name", user).apply()
    }

    fun getLoginMsg(): String? {
        return prefs!!.getString("LoginMsg", "")
    }

    fun setLoginMsg(user: String?) {
        prefs!!.edit().putString("LoginMsg", user).apply()
    }

    fun getBalance(): String? {
        return prefs!!.getString("AvailableBalance", "")
    }

    fun setBalance(balance: String?) {
        prefs!!.edit().putString("AvailableBalance", balance).apply()
    }

    fun setPhone(phone: String?) {
        prefs!!.edit().putString("Phone", phone).apply()
    }

    fun getPhone(): String? {
        return prefs!!.getString("Phone", "")
    }

    fun setEmail(email: String?) {
        prefs!!.edit().putString("Email", email).apply()
    }

    fun setProfilePic(profilePic: String?) {
        prefs!!.edit().putString("ProfilePic", profilePic).apply()
    }

    fun setUserPin(pic: String?) {
        prefs!!.edit().putString("UserPin", pic).apply()
    }

    fun getAuth(): String? {
        return prefs!!.getString("UserAuth", "")
    }

    fun setAuth(user: String?) {
        prefs!!.edit().putString("UserAuth", user).apply()
    }

    fun setTransfer(transfer: String?) {
        prefs!!.edit().putString("Transfer", transfer).apply()
    }

    fun setBetting(transfer: String?) {
        prefs!!.edit().putString("Betting", transfer).apply()
    }

    fun isBetting(): String? {
        return prefs!!.getString("Betting", "")
    }

   fun setNotifyStatus(transfer: String?) {
        prefs!!.edit().putString("NotifyStatus", transfer).apply()
    }

    fun isNotifyStatus(): String? {
        return prefs!!.getString("NotifyStatus", "")
    }

    fun setWithdraw(withdraw: String?) {
        prefs!!.edit().putString("Withdraw", withdraw).apply()
    }

    fun isWithdraw(): String? {
        return prefs!!.getString("Withdraw", "")
    }

    fun clearAllPrefs() {
        prefs!!.edit().clear().apply()
    }
}
