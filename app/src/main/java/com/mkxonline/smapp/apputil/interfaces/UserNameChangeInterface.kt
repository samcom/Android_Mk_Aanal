package com.mkxonline.smapp.apputil.interfaces

internal interface UserNameChangeInterface {
    fun changeUserName( s : String)
}