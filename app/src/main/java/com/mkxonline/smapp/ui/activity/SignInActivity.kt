package com.mkxonline.smapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.provider.Settings.Secure
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.messaging.FirebaseMessaging
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.LoginData
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.customview.ProgressView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignInActivity : AppCompatActivity() {

    private lateinit var etPhone: TextInputEditText
    private lateinit var etPassword: TextInputEditText
    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        etPhone = findViewById(R.id.et_phone)
        etPassword = findViewById(R.id.et_password)

        helper = SessionManager(this@SignInActivity)
        progressView = ProgressView(this@SignInActivity)

        findViewById<TextView>(R.id.txt_forgot_password).setOnClickListener {
            startActivity(Intent(this@SignInActivity, ForgotPasswordActivity::class.java))
        }

        BounceView.addAnimTo(findViewById<TextView>(R.id.txt_sign_up))
        findViewById<TextView>(R.id.txt_sign_up).setOnClickListener {
            finish()
        }

        BounceView.addAnimTo(findViewById<AppCompatButton>(R.id.btn_sign_in))
        findViewById<AppCompatButton>(R.id.btn_sign_in).setOnClickListener {

            if (Utility.isValidPhoneNo(etPhone.text.toString())) {
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_phone))
                return@setOnClickListener
            }

            if(Utility.isStringNull(etPassword.text.toString())){
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_password))
                return@setOnClickListener
            }

            if(Utility.isValidPassword(etPassword.text.toString())){
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_password_min))
                return@setOnClickListener
            }

            if (Utility.isInternetConnection(this@SignInActivity)) {
                FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Utility.errorSnackBar(findViewById(android.R.id.content),"Server Error please try later")
                        return@OnCompleteListener
                    }
                    val fcmToken = task.result
                    login(fcmToken)
                    Log.e("onComplete: ", fcmToken)
                })
            }else{
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internet_msg))
            }

        }
    }

    private fun login(fcmToken: String) {
        val androidId = Secure.getString(contentResolver, Secure.ANDROID_ID)

        progressView.view()
        val call: Call<LoginData?>? = com.mkxonline.smapp.api.ApiMain.getClient.login(etPhone.text.toString(), etPassword.text.toString(), fcmToken,androidId)

        call!!.enqueue(object : Callback<LoginData?> {
            override fun onResponse(call: Call<LoginData?>, response: Response<LoginData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.body()!!.error == false){
                    Utility.successSnackBar(findViewById(android.R.id.content),response.body()!!.message!!)
                    if(response.body()!!.
                        data!!.userDetails!!.status.equals("active")){
                        helper.setLogin(true)
                        helper.setUserPin(response.body()!!.data!!.userDetails!!.securityPin)
                        helper.setName(response.body()!!.data!!.userDetails!!.name)
                        helper.setPhone(response.body()!!.data!!.userDetails!!.phoneNumber)
                        helper.setProfilePic(response.body()!!.data!!.userDetails!!.profilePicture)
                        helper.setEmail(response.body()!!.data!!.userDetails!!.email)
                        helper.setBalance(response.body()!!.data!!.userDetails!!.availableBalance)
                        helper.setAuth(response.body()!!.data!!.userDetails!!.token)
                        helper.setTransfer(response.body()!!.data!!.userDetails!!.transfer)
                        helper.setBetting(response.body()!!.data!!.userDetails!!.betting)
                        helper.setNotifyStatus(response.body()!!.data!!.userDetails!!.notiStatus)
                        helper.setLoginMsg(response.body()!!.data!!.userDetails!!.loginMsg)

                        startActivity(Intent(this@SignInActivity, MainActivity::class.java))
                        finish()
                    }else{
                        Utility.errorSnackBar(findViewById(android.R.id.content),"User Inactive,please contact admin.")
                    }
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<LoginData?>, t: Throwable) {
                progressView.hide()

            }
        })

    }
}