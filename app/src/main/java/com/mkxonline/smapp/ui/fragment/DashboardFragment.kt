package com.mkxonline.smapp.ui.fragment

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.github.hariprasanths.bounceview.BounceView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.ApiMain
import com.mkxonline.smapp.api.model.BannerData
import com.mkxonline.smapp.api.model.DashboardData
import com.mkxonline.smapp.api.model.ListData
import com.mkxonline.smapp.api.model.SettingsData
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.customview.NonSwipeableViewPager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.apputil.interfaces.ToolbarTitleInterface
import com.mkxonline.smapp.apputil.interfaces.UserNameChangeInterface
import com.mkxonline.smapp.ui.activity.GameActivity
import com.mkxonline.smapp.ui.adapter.MainListAdapter
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DashboardFragment : Fragment() {

    private val dataDetails: ArrayList<ListData.Data.ResultItem> = ArrayList()
    private lateinit var mainListAdapter: MainListAdapter
    lateinit var progressView: ProgressView
    lateinit var rvList : RecyclerView

    private lateinit var txtCurrentBalance : TextView
    private lateinit var menuWallet : MenuItem

    lateinit var helper: SessionManager

    private lateinit var imgWithdraw : ImageView
    private lateinit var imgAddPoint: ImageView
    private lateinit var imgWhatsapp : ImageView

    private lateinit var llSocial : LinearLayout

    private lateinit var swipeContainer: SwipeRefreshLayout

    private lateinit var viewPager: NonSwipeableViewPager
    private lateinit var myViewPagerAdapter: MyViewPagerAdapter

    private lateinit var toolbarTitleInterface: ToolbarTitleInterface
    private lateinit var userNameChangeInterface: UserNameChangeInterface

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_dashboard, container, false)

        progressView = ProgressView(requireActivity())
        helper = SessionManager(requireActivity())

        activity?.let {
            instantiateNavigationInterface(it)
        }

        llSocial = view.findViewById(R.id.ll_social)
        imgWhatsapp = view.findViewById(R.id.img_whatsapp)
        imgWithdraw = view.findViewById(R.id.img_withdraw)
        imgAddPoint = view.findViewById(R.id.img_addpoint)

        BounceView.addAnimTo(imgAddPoint)
        BounceView.addAnimTo(imgWhatsapp)
        BounceView.addAnimTo(imgWithdraw)

        rvList = view.findViewById(R.id.rv_list)
        rvList.layoutManager = LinearLayoutManager(activity)
        rvList.itemAnimator = DefaultItemAnimator()

        viewPager = view.findViewById(R.id.view_pager)
        viewPager.disableScroll(true)
        viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int) {
            }
            override fun onPageSelected(position: Int) {
                slider()
            }
            override fun onPageScrollStateChanged(state: Int) {
            }
        })

        swipeContainer = view.findViewById(R.id.swipeContainer)
        swipeContainer.setOnRefreshListener {
            if (Utility.isInternetConnection(requireActivity())) {
                getList()
                getBanner()
                dashboard()
            }else{
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
            }
        }

        imgWhatsapp.setOnClickListener {
            getSettings()
        }

        imgWithdraw.setOnClickListener {
//            startActivity(Intent(requireActivity(), StarLineActivity::class.java))

            val bundle = Bundle()
            bundle.putString("page", "2")

            val fragment = WalletFragment()
            fragment.arguments = bundle

            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame,fragment)
                .commit()
        }

        imgAddPoint.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("page", "1")

            val fragment = WalletFragment()
            fragment.arguments = bundle

            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame,fragment)
                .commit()
        }

        mainListAdapter = MainListAdapter(requireActivity(),dataDetails,object : MainListAdapter.Callbacks {
            override fun onClick(s: ListData.Data.ResultItem) {
                if(helper.isBetting().equals("yes")){
                    startActivity(Intent(requireActivity(), GameActivity::class.java).putExtra("GameDetails",s))
                }
            }

            override fun onChartClick(s: ListData.Data.ResultItem) {
                val bundle = Bundle()
                bundle.putSerializable("GameDetails", s)

                val fragment = ChartFragment()
                fragment.arguments = bundle

                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit()
            }
        })

        if (Utility.isInternetConnection(requireActivity())) {
            dashboard()
            getList()
            getBanner()
        }else{
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
        }

        return view
    }

    private fun slider() {
        Handler().postDelayed({
            if (viewPager.currentItem != myViewPagerAdapter.count - 1) {
                viewPager.setCurrentItem(viewPager.currentItem + 1, true)
            } else {
                viewPager.setCurrentItem(0, true)
            }
        }, 4000)
    }

    override fun onResume() {
        super.onResume()
        toolbarTitleInterface.changeTitle("Dashboard")
    }

    private fun instantiateNavigationInterface(context: FragmentActivity) {
        toolbarTitleInterface = context as ToolbarTitleInterface
        userNameChangeInterface = context as UserNameChangeInterface
    }

    private fun getList(){
        progressView.view()
        dataDetails.clear()
        val call: Call<ListData?>? = com.mkxonline.smapp.api.ApiMain.getClient.gameList("Bearer "+helper.getAuth().toString())
        call!!.enqueue(object : Callback<ListData?> {
            override fun onResponse(call: Call<ListData?>, response: Response<ListData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false
                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    if (isAttachedToActivity()) {
                        dataDetails.addAll(response.body()!!.data!!.result!!)
                        rvList.adapter = mainListAdapter
                    }
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<ListData?>, t: Throwable) {
                progressView.hide()
                if(isAdded) {
                    Utility.errorSnackBar(
                        requireActivity().findViewById(android.R.id.content),
                        t.message!!
                    )
                }
            }
        })
    }

    private fun getBanner(){
        progressView.view()

        val call: Call<BannerData?>? = com.mkxonline.smapp.api.ApiMain.getClient.slideBanner("Bearer "+helper.getAuth().toString())
        call!!.enqueue(object : Callback<BannerData?> {
            override fun onResponse(call: Call<BannerData?>, response: Response<BannerData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    if(isAdded) {
                        myViewPagerAdapter = MyViewPagerAdapter(requireActivity(), response.body()!!.data!!.result!!)
                        viewPager.adapter = myViewPagerAdapter
                        slider()
                    }
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<BannerData?>, t: Throwable) {
                progressView.hide()
                if(isAdded) {
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), t.message!!)
                }
            }
        })
    }

    private fun dashboard() {
        progressView.view()
        val call: Call<DashboardData?>? = com.mkxonline.smapp.api.ApiMain.getClient.dashboard("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<DashboardData?> {
            override fun onResponse(call: Call<DashboardData?>, response: Response<DashboardData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    helper.setName(response.body()!!.data!!.result!!.username)
                    helper.setPhone(response.body()!!.data!!.result!!.phoneNumber)
                    helper.setBalance(response.body()!!.data!!.result!!.availableBalance)
                    helper.setTransfer(response.body()!!.data!!.result!!.transfer)
                    helper.setBetting(response.body()!!.data!!.result!!.betting)
                    helper.setNotifyStatus(response.body()!!.data!!.result!!.notiStatus)
                    helper.setWithdraw(response.body()!!.data!!.result!!.withdraw)
                    updateBetting()
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<DashboardData?>, t: Throwable) {
                progressView.hide()
            }
        })

    }

    private fun getSettings() {
        progressView.view()
        val call: Call<SettingsData?>? = ApiMain.getClient.settings("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<SettingsData?> {
            override fun onResponse(call: Call<SettingsData?>, response: Response<SettingsData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){

                    if(!response.body()!!.data!!.result!!.whatsappNumber.isNullOrBlank()){
                        try {
                            val intent = Intent(Intent.ACTION_VIEW, Uri.parse( "https://api.whatsapp.com/send?phone="+response.body()!!.data!!.result!!.whatsappNumber!!))
                            startActivity(intent)
                        } catch (e: ActivityNotFoundException) {
                            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), e.toString())
                        }
                    }

                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<SettingsData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    internal class MyViewPagerAdapter(var context: Context, private var stringArrayList: ArrayList<BannerData.Data.ResultItem>) : PagerAdapter() {
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val layoutInflater = LayoutInflater.from(context)
            val view = layoutInflater.inflate(R.layout.item_slider, container, false)
            val imageBanner = view.findViewById<ImageView>(R.id.image_banner)

            Picasso.get().load(stringArrayList[position].image!!)
                .into(imageBanner)

            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return stringArrayList.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            container.removeView(view)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_main, menu)
        menuWallet = menu.findItem(R.id.menu_wallet)
        menuWallet.isVisible = false
        txtCurrentBalance = menuWallet.actionView!!.findViewById(R.id.txt_current_balance)
        txtCurrentBalance.setOnClickListener {
            if(helper.isBetting().equals("yes")) {
                val bundle = Bundle()
                bundle.putString("page", "1")

                val fragment = WalletFragment()
                fragment.arguments = bundle

                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame,fragment)
                    .commit()
            }
        }
    }

    fun isAttachedToActivity(): Boolean {
        return isVisible && activity != null
    }

    fun updateBetting() {
        if(isAdded) {
            Handler(Looper.getMainLooper()).postDelayed({
                txtCurrentBalance.text = Utility.decimalFormat(helper.getBalance()!!)
                if (helper.isBetting().equals("no")) {
                    llSocial.visibility = View.GONE
                    menuWallet.isVisible = false
                } else {
                    llSocial.visibility = View.VISIBLE
                    menuWallet.isVisible = true
                }
            }, 500)
            userNameChangeInterface.changeUserName(helper.getName()!!)
        }
    }
}