package com.mkxonline.smapp.ui.activity

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.api.model.BidHistoryData
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.ui.adapter.BidHistoryAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class BidHistoryActivity : AppCompatActivity() {

    private lateinit var etStartDate: TextInputEditText
    private lateinit var etEndDate: TextInputEditText
    private lateinit var btnSearch: AppCompatButton
    private lateinit var swipeContainer: SwipeRefreshLayout
    private val dataDetails: ArrayList<BidHistoryData.Data.Result.DataItem> = ArrayList()
    private lateinit var bidHistoryAdapter: BidHistoryAdapter
    private lateinit var progressView: ProgressView
    private lateinit var rvBidHistory : RecyclerView
    private lateinit var helper: SessionManager

    private var startDate = ""
    private var endDate = ""

    private var currentPage = 1
    private var lastPage = 0

    private lateinit var toolbar: Toolbar
    private lateinit var toolbarTitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bid_win_history)

        progressView = ProgressView(this@BidHistoryActivity)
        helper = SessionManager(this@BidHistoryActivity)

        swipeContainer = findViewById(R.id.swipeContainer)
        etStartDate = findViewById(R.id.et_start_date)
        etEndDate = findViewById(R.id.et_end_date)
        btnSearch = findViewById(R.id.btn_search)

        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.toolbar_title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""
        toolbarTitle.text = getString(R.string.bid_history)

        swipeContainer.setOnRefreshListener {
            onResume()
        }

        val df = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val formattedDate: String = df.format(Calendar.getInstance().time)
        etStartDate.setText(formattedDate)
        etEndDate.setText(formattedDate)

        val df1 = SimpleDateFormat("yyyy-MM-ddd", Locale.getDefault())
        startDate = df1.format(Calendar.getInstance().time)
        endDate = df1.format(Calendar.getInstance().time)

        etStartDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(this@BidHistoryActivity, { view, year, monthOfYear, dayOfMonth ->
                etStartDate.setText(dayOfMonth.toString() + "-" + (monthOfYear+1) + "-" + year.toString())
                startDate = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth.toString()
                }, Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
            datePickerDialog.show()
        }

        etEndDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(this@BidHistoryActivity, { view, year, monthOfYear, dayOfMonth ->
                etEndDate.setText(dayOfMonth.toString() + "-" + (monthOfYear+1) + "-" + year.toString())
                endDate = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth.toString()
                }, Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
            datePickerDialog.show()
        }

        rvBidHistory = findViewById(R.id.rv_bid_win_history)
        rvBidHistory.layoutManager = LinearLayoutManager(this@BidHistoryActivity)
        rvBidHistory.itemAnimator = DefaultItemAnimator()

        bidHistoryAdapter = BidHistoryAdapter(dataDetails)
        rvBidHistory.adapter = bidHistoryAdapter

        rvBidHistory.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    if (currentPage == lastPage) {
                        return
                    }
                    getBidHistory(++currentPage)
                }
            }
        })

        BounceView.addAnimTo(btnSearch)
        btnSearch.setOnClickListener {
            onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        if (Utility.isInternetConnection(this@BidHistoryActivity)) {
            getBidHistory(currentPage)
        }else{
            Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internet_msg))
        }
    }

    private fun getBidHistory(page: Int){
        if (page == 1) {
            dataDetails.clear()
            bidHistoryAdapter.notifyDataSetChanged()
        }

        progressView.view()

        val call: Call<BidHistoryData?>? = com.mkxonline.smapp.api.ApiMain.getClient.starlineBidHistory("Bearer "+helper.getAuth().toString(),page.toString(),startDate,endDate)
        call!!.enqueue(object : Callback<BidHistoryData?> {
            override fun onResponse(call: Call<BidHistoryData?>, response: Response<BidHistoryData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@BidHistoryActivity)
                }else if(response.body()!!.error == false){
                    currentPage = response.body()!!.data!!.result!!.currentPage!!
                    lastPage = response.body()!!.data!!.result!!.lastPage!!
                    dataDetails.addAll(response.body()!!.data!!.result!!.data!!)
                    bidHistoryAdapter.notifyDataSetChanged()
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<BidHistoryData?>, t: Throwable) {
                progressView.hide()
                swipeContainer.isRefreshing = false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}