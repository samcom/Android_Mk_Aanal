package com.mkxonline.smapp.ui.activity

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.WinHistoryData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.ui.adapter.WinHistoryAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class WinHistoryActivity : AppCompatActivity() {

    private lateinit var etStartDate: TextInputEditText
    private lateinit var etEndDate: TextInputEditText
    private lateinit var btnSearch: AppCompatButton

    private val dataDetails: ArrayList<WinHistoryData.Data.Result.DataItem> = ArrayList()
    private lateinit var winHistoryAdapter: WinHistoryAdapter
    lateinit var progressView: ProgressView
    private lateinit var rvWinHistory : RecyclerView
    lateinit var helper: SessionManager

    private lateinit var swipeContainer: SwipeRefreshLayout

    private var startDate = ""
    private var endDate = ""

    private lateinit var toolbar: Toolbar
    private lateinit var toolbarTitle: TextView

    private var currentPage = 1
    private var lastPage = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bid_win_history)

        etStartDate = findViewById(R.id.et_start_date)
        etEndDate = findViewById(R.id.et_end_date)
        btnSearch = findViewById(R.id.btn_search)

        progressView = ProgressView(this)
        helper = SessionManager(this)

        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.toolbar_title)
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""
        toolbarTitle.text = getString(R.string.win_history)

        swipeContainer = findViewById(R.id.swipeContainer)
        swipeContainer.setOnRefreshListener {
            onResume()
        }

        val df = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val formattedDate: String = df.format(Calendar.getInstance().time)
        etStartDate.setText(formattedDate)
        etEndDate.setText(formattedDate)

        val df1 = SimpleDateFormat("yyyy-MM-ddd", Locale.getDefault())
        startDate = df1.format(Calendar.getInstance().time)
        endDate = df1.format(Calendar.getInstance().time)

        etStartDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(this@WinHistoryActivity, { view, year, monthOfYear, dayOfMonth ->
                etStartDate.setText(dayOfMonth.toString() + "-" + (monthOfYear+1) + "-" + year.toString())
                startDate = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth.toString()
            }, Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
            datePickerDialog.show()
        }

        etEndDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(this@WinHistoryActivity, { view, year, monthOfYear, dayOfMonth ->
                etEndDate.setText(dayOfMonth.toString() + "-" + (monthOfYear+1) + "-" + year.toString())
                endDate = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth.toString()
            }, Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
            datePickerDialog.show()
        }

        rvWinHistory = findViewById(R.id.rv_bid_win_history)
        rvWinHistory.layoutManager = LinearLayoutManager(this@WinHistoryActivity)
        rvWinHistory.itemAnimator = DefaultItemAnimator()

        winHistoryAdapter = WinHistoryAdapter(dataDetails)
        rvWinHistory.adapter = winHistoryAdapter

        rvWinHistory.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    if (currentPage == lastPage) {
                        return
                    }
                    getWinHistory(++currentPage)
                }
            }
        })

        BounceView.addAnimTo(btnSearch)
        btnSearch.setOnClickListener {
            onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        getWinHistory(currentPage)
    }

    private fun getWinHistory(page: Int){
        if (page == 1) {
            dataDetails.clear()
            winHistoryAdapter.notifyDataSetChanged()
        }

        progressView.view()

        val call: Call<WinHistoryData?>? = com.mkxonline.smapp.api.ApiMain.getClient.starlineWinHistory("Bearer "+helper.getAuth().toString(),page.toString(),startDate,endDate)
        call!!.enqueue(object : Callback<WinHistoryData?> {
            override fun onResponse(call: Call<WinHistoryData?>, response: Response<WinHistoryData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@WinHistoryActivity)
                }else if(response.body()!!.error == false) {
                    currentPage = response.body()!!.data!!.result!!.currentPage!!
                    lastPage = response.body()!!.data!!.result!!.lastPage!!
                    dataDetails.addAll(response.body()!!.data!!.result!!.data!!)
                    winHistoryAdapter.notifyDataSetChanged()
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<WinHistoryData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}