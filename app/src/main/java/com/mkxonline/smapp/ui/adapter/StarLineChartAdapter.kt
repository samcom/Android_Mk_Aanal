package com.mkxonline.smapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.ChartData
import com.mkxonline.smapp.api.model.StarLineChartData
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class StarLineChartAdapter(var context: Context, var arrayList: List<StarLineChartData.GameListItem>,
                           var result: List<StarLineChartData.Data.ResultItem>) : RecyclerView.Adapter<StarLineChartAdapter.ViewHolderContact>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_starline_chart, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return result.size+1
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {

        viewholder.subGrid.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        viewholder.subGrid.itemAnimator = DefaultItemAnimator()

        if(position == 0){
            viewholder.txtDate.text = "Date"

            val arrayList1: ArrayList<ChartData> = ArrayList()

            for (item in arrayList) {
                val c = ChartData()
                c.text1 = item.gameName!!
                c.text2 = item.id!!.toString()
                arrayList1.add(c)
            }

            viewholder.subGrid.adapter = SubChartAdapter(context,arrayList1,0)
        }else {
            val s = result[position-1]
            try {
                val calendarOpen = Calendar.getInstance()
                calendarOpen.time = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(s.date)

                viewholder.txtDate.text = calendarOpen.get(Calendar.DAY_OF_MONTH).toString()+"-\n"+
                        (calendarOpen.get(Calendar.MONTH)+1).toString()+"-\n"+calendarOpen.get(Calendar.YEAR).toString()

            } catch (e: ParseException) {
                e.printStackTrace()
            }

            val arrayList: ArrayList<ChartData> = ArrayList()

            for (item in s.chart!!) {
                val c = ChartData()
                c.text1 = item!!.pana!!
                c.text2 = item.digit!!
                arrayList.add(c)
            }

            viewholder.subGrid.adapter = SubChartAdapter(context,arrayList,1)
        }
    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {
        var txtDate: TextView = view.findViewById(R.id.txt_date)
        var subGrid: RecyclerView = view.findViewById(R.id.sub_grid)
    }
}
