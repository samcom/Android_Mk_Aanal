package com.mkxonline.smapp.ui.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.DashboardData
import com.mkxonline.smapp.api.model.MessageData
import com.mkxonline.smapp.api.model.SettingsData
import com.mkxonline.smapp.api.model.WithdrawData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.apputil.interfaces.ToolbarTitleInterface
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.ui.adapter.WithdrawHistoryAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.util.Calendar

class WithdrawFragment : Fragment() {

    private lateinit var btnWithdrawFund: AppCompatButton

    private lateinit var etPoints: TextInputEditText
    private lateinit var spnPayment: Spinner
    private lateinit var etPayment: TextInputEditText

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    private lateinit var txtCurrentBalance: AppCompatTextView

    private lateinit var rvWithdrawHistory : RecyclerView
    private lateinit var walletHistoryAdapter: WithdrawHistoryAdapter

    private var dataDetails: ArrayList<String> = ArrayList()
    private var dataNumber: ArrayList<String> = ArrayList()
    private var dataMethod: ArrayList<String> = ArrayList()
    private lateinit var txtOpenTime : TextView
    private lateinit var txtCloseTime : TextView

    var pos = 0

    private var currentPage = 1
    private var lastPage = 0

    private val dataDetail: ArrayList<WithdrawData.Data.Result.DataItem> = ArrayList()

    private lateinit var toolbarTitleInterface: ToolbarTitleInterface

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_withdraw, container, false)

        helper = SessionManager(requireActivity())
        progressView = ProgressView(requireActivity())

        activity?.let {
            instantiateNavigationInterface(it)
        }

        txtOpenTime = view.findViewById(R.id.txt_open_time)
        txtCloseTime = view.findViewById(R.id.txt_close_time)
        etPoints = view.findViewById(R.id.et_points)
        etPayment = view.findViewById(R.id.et_payment)
        spnPayment = view.findViewById(R.id.spn_payment)

        btnWithdrawFund = view.findViewById(R.id.btn_withdraw_fund)

        rvWithdrawHistory = view.findViewById(R.id.rv_withdraw_history)
        rvWithdrawHistory.layoutManager = LinearLayoutManager(activity)
        rvWithdrawHistory.itemAnimator = DefaultItemAnimator()

        walletHistoryAdapter = WithdrawHistoryAdapter(requireActivity(),dataDetail)
        rvWithdrawHistory.adapter = walletHistoryAdapter

        rvWithdrawHistory.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    if (currentPage == lastPage) {
                        return
                    }
                    getWalletHistory(++currentPage)
                }
            }
        })

        etPayment.setOnClickListener {
            spnPayment.performClick()
        }

        btnWithdrawFund.setOnClickListener {

            if (!validPoints()) {
                return@setOnClickListener
            }

            if (!validPayment()) {
                return@setOnClickListener
            }

            if (Utility.isInternetConnection(requireActivity())) {
                Handler(Looper.getMainLooper()).postDelayed({
                    dashboard(1)
                }, 500)
            }else{
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
            }
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, WalletFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        if (Utility.isInternetConnection(requireActivity())) {
            Handler(Looper.getMainLooper()).postDelayed({
                dashboard(0)
            }, 500)
            getWalletHistory(currentPage)
            toolbarTitleInterface.changeTitle("Withdraw Fund")
        }else{
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
        }

        return view
    }

    private fun instantiateNavigationInterface(context: FragmentActivity) {
        toolbarTitleInterface = context as ToolbarTitleInterface
    }

    private fun validPoints(): Boolean {
        val points: String = etPoints.text.toString().trim { it <= ' ' }
        return if (points.isEmpty()){
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Please Enter Valid Point")
            false
        }else if (points.toInt() == 0) {
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Please Enter Valid Point")
            false
        } else {
            true
        }
    }

    private fun validPayment(): Boolean {
        val points: String = etPayment.text.toString().trim { it <= ' ' }
        return if (points.isEmpty()) {
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Please Enter Phone Number First")
            false
        } else {
            true
        }
    }

    private fun dashboard(i: Int) {
        progressView.view()
        val call: Call<DashboardData?>? = com.mkxonline.smapp.api.ApiMain.getClient.dashboard("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<DashboardData?> {
            override fun onResponse(call: Call<DashboardData?>, response: Response<DashboardData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    if(i==0){
                        helper.setBalance(response.body()!!.data!!.result!!.availableBalance)
                        Handler(Looper.getMainLooper()).postDelayed({
                            txtCurrentBalance.text = Utility.decimalFormat(helper.getBalance()!!)
                        }, 500)

                        try {
                            val openDate = Utility.mFormatDate.parse(response.body()!!.data!!.result!!.withdrawalOpenTime!!)
                            val closeDate = Utility.mFormatDate.parse(response.body()!!.data!!.result!!.withdrawalCloseTime!!)
                            val calendarOpen = Calendar.getInstance()
                            val calendarClose = Calendar.getInstance()

                            calendarOpen.time = openDate!!
                            calendarOpen.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
                            calendarOpen.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                            calendarOpen.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

                            calendarClose.time = closeDate!!
                            calendarClose.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
                            calendarClose.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                            calendarClose.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

                            txtOpenTime.text = "Open "+ Utility.mFormatDate3.format(openDate)
                            txtCloseTime.text = "Close "+ Utility.mFormatDate3.format(closeDate)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }

                        if(response.body()!!.data!!.result!!.googlepayNumber!=null && response.body()!!.data!!.result!!.googlepayNumber!!.isNotEmpty()){
                            dataDetails.add("Google Pay("+response.body()!!.data!!.result!!.googlepayNumber!!+")")
                            dataNumber.add(response.body()!!.data!!.result!!.googlepayNumber!!)
                            dataMethod.add("Google Pay")
                        }

                        if(response.body()!!.data!!.result!!.phonepayNumber!=null && response.body()!!.data!!.result!!.phonepayNumber!!.isNotEmpty()){
                            dataDetails.add("Phone Pay("+response.body()!!.data!!.result!!.phonepayNumber!!+")")
                            dataNumber.add(response.body()!!.data!!.result!!.phonepayNumber!!)
                            dataMethod.add("Phone Pay")
                        }

                        if(response.body()!!.data!!.result!!.paytmNumber!=null && response.body()!!.data!!.result!!.paytmNumber!!.isNotEmpty()){
                            dataDetails.add("Paytm ("+response.body()!!.data!!.result!!.paytmNumber!!+")")
                            dataNumber.add(response.body()!!.data!!.result!!.paytmNumber!!)
                            dataMethod.add("Paytm")
                        }

                        if (isAttachedToActivity()) {
                            val dataAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, dataDetails)
                            spnPayment.adapter = dataAdapter
                        }

                        spnPayment.onItemSelectedListener = object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                                etPayment.setText(dataDetails[position])
                                pos = position
                            }
                            override fun onNothingSelected(parent: AdapterView<*>) {
                            }
                        }
                    }else{
                        if(etPoints.text.toString().toInt() > helper.getBalance()!!.toInt()){
                            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Not that much balance")
                            return
                        }

                        getSettings()
                    }

                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<DashboardData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    private fun getSettings() {
        progressView.view()
        val call: Call<SettingsData?>? = com.mkxonline.smapp.api.ApiMain.getClient.settings("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<SettingsData?> {
            override fun onResponse(call: Call<SettingsData?>, response: Response<SettingsData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    try {
                        val openDate = Utility.mFormatDate.parse(response.body()!!.data!!.result!!.withdrawalOpenTime!!)
                        val closeDate = Utility.mFormatDate.parse(response.body()!!.data!!.result!!.withdrawalCloseTime!!)
                        val calendarOpen = Calendar.getInstance()
                        val calendarClose = Calendar.getInstance()

                        calendarOpen.time = openDate!!
                        calendarOpen.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
                        calendarOpen.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                        calendarOpen.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

                        calendarClose.time = closeDate!!
                        calendarClose.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
                        calendarClose.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                        calendarClose.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

                        if(Calendar.getInstance().time.after(calendarOpen.time)  && Calendar.getInstance().time.before(calendarClose.time)) {
                            if(etPoints.text.toString().toInt() >= response.body()!!.data!!.result!!.minWithdrawal!!){
                                if(etPoints.text.toString().toInt() <= response.body()!!.data!!.result!!.maxWithdrawal!!){
                                    withdrawRequest()
                                }else{
                                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Maximum amount should be "+response.body()!!.data!!.result!!.maxWithdrawal!!)
                                }
                            }else{
                                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Minimum amount should be "+response.body()!!.data!!.result!!.minWithdrawal!!+" or more")
                            }
                        } else {
                            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Withdrawal Timing is Over")
                        }

                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }

                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<SettingsData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    private fun withdrawRequest() {
        progressView.view()
        val call: Call<MessageData?>? = com.mkxonline.smapp.api.ApiMain.getClient.withdrawRequest("Bearer "+helper.getAuth().toString()
            , dataMethod[pos], dataNumber[pos],etPoints.text.toString())
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    etPoints.setText("")
                    etPayment.setText("")
                    getWalletHistory(currentPage)
                    dashboard(0)
                    Utility.successSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    private fun getWalletHistory(page: Int){
        if (page == 1) {
            dataDetail.clear()
            walletHistoryAdapter.notifyDataSetChanged()
        }

        progressView.view()

        val call: Call<WithdrawData?>? = com.mkxonline.smapp.api.ApiMain.getClient.withdrawHistory("Bearer "+helper.getAuth().toString(),page.toString())
        call!!.enqueue(object : Callback<WithdrawData?> {
            override fun onResponse(call: Call<WithdrawData?>, response: Response<WithdrawData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    if (isAttachedToActivity()) {
                        currentPage = response.body()!!.data!!.result!!.currentPage!!
                        lastPage = response.body()!!.data!!.result!!.lastPage!!
                        dataDetail.addAll(response.body()!!.data!!.result!!.data!!)
                        walletHistoryAdapter.notifyDataSetChanged()
                    }
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<WithdrawData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_main, menu)
        txtCurrentBalance = menu.findItem(R.id.menu_wallet).actionView!!.findViewById(R.id.txt_current_balance)

    }

    fun isAttachedToActivity(): Boolean {
        return isVisible && activity != null
    }
}