package com.mkxonline.smapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.github.hariprasanths.bounceview.BounceView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.api.model.ListData
import com.mkxonline.smapp.api.model.ListDetailData
import com.mkxonline.smapp.api.model.TypeData
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.util.*

class GameActivity : AppCompatActivity() {

    private val dataDetails: ArrayList<TypeData.Data.ResultItem> = ArrayList()

    private lateinit var imgSingleDigit: ImageView
    private lateinit var imgJodiDigit: ImageView
    private lateinit var imgSinglePana: ImageView
    private lateinit var imgDoublePana: ImageView
    private lateinit var imgTriplePana: ImageView
    private lateinit var imgHalfSangam: ImageView
    private lateinit var imgFullSangam: ImageView

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    lateinit var listDetails: ListData.Data.ResultItem

    private lateinit var toolbar: Toolbar
    private lateinit var toolbarTitle: TextView

    private lateinit var swipeContainer: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_type)

        listDetails = intent?.extras!!.getSerializable("GameDetails") as ListData.Data.ResultItem

        imgSingleDigit = findViewById(R.id.img_single_digit)
        imgJodiDigit = findViewById(R.id.img_jodi_digit)
        imgSinglePana = findViewById(R.id.img_single_pana)
        imgDoublePana = findViewById(R.id.img_double_pana)
        imgTriplePana = findViewById(R.id.img_triple_pana)
        imgHalfSangam = findViewById(R.id.img_half_sangam)
        imgFullSangam = findViewById(R.id.img_full_sangam)

        progressView = ProgressView(this@GameActivity)
        helper = SessionManager(this@GameActivity)

        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.toolbar_title)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbarTitle.text = listDetails.gameName

        swipeContainer = findViewById(R.id.swipeContainer)
        swipeContainer.setOnRefreshListener {
            onResume()
        }

        BounceView.addAnimTo(imgSingleDigit)
        imgSingleDigit.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.SINGLE_DIGIT)){
                    onClickType(item)
                    break
                }
            }
        }

        BounceView.addAnimTo(imgJodiDigit)
        imgJodiDigit.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.JODI_DIGIT)){
                    onClickType(item)
                    break
                }
            }
        }

        BounceView.addAnimTo(imgSinglePana)
        imgSinglePana.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.SINGLE_PANA)){
                    onClickType(item)
                    break
                }
            }
        }

        BounceView.addAnimTo(imgDoublePana)
        imgDoublePana.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.DOUBLE_PANA)){
                    onClickType(item)
                    break
                }
            }
        }

        BounceView.addAnimTo(imgTriplePana)
        imgTriplePana.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.TRIPLE_PANA)){
                    onClickType(item)
                    break
                }
            }
        }

        BounceView.addAnimTo(imgHalfSangam)
        imgHalfSangam.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.HALF_SANGAM)){
                    onClickType(item)
                    break
                }
            }
        }

        BounceView.addAnimTo(imgFullSangam)
        imgFullSangam.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.FULL_SANGAM)){
                    onClickType(item)
                    break
                }
            }
        }
    }

    private fun onClickType(s: TypeData.Data.ResultItem){
        if(s.type.equals(Utility.JODI_DIGIT) || s.type.equals(Utility.HALF_SANGAM) || s.type.equals(
                Utility.FULL_SANGAM)){
            try {
                val openDate = Utility.mFormatDate.parse(listDetails.openTime!!)
                val calendarOpen = Calendar.getInstance()

                calendarOpen.time = openDate!!
                calendarOpen.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
                calendarOpen.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                calendarOpen.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

                if(Calendar.getInstance().time.before(calendarOpen.time)) {
                    startActivity(Intent(this@GameActivity, BidActivity::class.java)
                        .putExtra("GameType",s)
                        .putExtra("GameDetails",listDetails))
                } else {
                    Utility.errorSnackBar(findViewById(android.R.id.content),"Open Session is close now. you can't place bid!")
                }
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }else{
            startActivity(Intent(this@GameActivity, BidActivity::class.java)
                .putExtra("GameType",s)
                .putExtra("GameDetails",listDetails))
        }
    }

    override fun onResume() {
        super.onResume()

        if (Utility.isInternetConnection(this@GameActivity)) {
            getType()
            listDetails()
        }else{
            Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internet_msg))
        }
    }

    private fun listDetails(){
        progressView.view()
        val call: Call<ListDetailData?>? = com.mkxonline.smapp.api.ApiMain.getClient.gameDetails("Bearer "+helper.getAuth().toString(),listDetails.id!!)
        call!!.enqueue(object : Callback<ListDetailData?> {
            override fun onResponse(call: Call<ListDetailData?>, response: Response<ListDetailData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.code() == 401){
                    Utility.unAuthPopUp(this@GameActivity)
                }else if(response.body()!!.error == false){
                    listDetails.openTime = response.body()!!.data!!.result!!.openTime!!
                    listDetails.closeTime = response.body()!!.data!!.result!!.closeTime!!
                    try {
                        val endDate = Utility.mFormatDate.parse(listDetails.closeTime!!)
                        val calendarEnd = Calendar.getInstance()

                        calendarEnd.time = endDate!!
                        calendarEnd.set(
                            Calendar.MONTH,
                            Calendar.getInstance().get(Calendar.MONTH)
                        )
                        calendarEnd.set(
                            Calendar.DAY_OF_MONTH,
                            Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                        )
                        calendarEnd.set(
                            Calendar.YEAR,
                            Calendar.getInstance().get(Calendar.YEAR)
                        )

                        if (!Calendar.getInstance().time.before(calendarEnd.time)) {
                            onBackPressed()
                        }

                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<ListDetailData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    private fun getType(){
        progressView.view()
        dataDetails.clear()
        val call: Call<TypeData?>? = com.mkxonline.smapp.api.ApiMain.getClient.gameType("Bearer "+helper.getAuth().toString())
        call!!.enqueue(object : Callback<TypeData?> {
            override fun onResponse(call: Call<TypeData?>, response: Response<TypeData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@GameActivity)
                }else if(response.body()!!.error == false){
                    dataDetails.addAll(response.body()!!.data!!.result!!)
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<TypeData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}