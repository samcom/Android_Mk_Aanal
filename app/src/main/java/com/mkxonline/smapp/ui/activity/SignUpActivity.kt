package com.mkxonline.smapp.ui.activity

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.messaging.FirebaseMessaging
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.ApiMain
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.api.model.LoginData
import com.mkxonline.smapp.api.model.MessageData
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignUpActivity : AppCompatActivity() {

    private lateinit var etUsername: TextInputEditText
    private lateinit var etPhone: TextInputEditText
    private lateinit var etPassword: TextInputEditText

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView
    private var fcmToken: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        etUsername = findViewById(R.id.et_username)
        etPhone = findViewById(R.id.et_phone)
        etPassword = findViewById(R.id.et_password)

        helper = SessionManager(this@SignUpActivity)
        progressView = ProgressView(this@SignUpActivity)

        if(!helper.isFirst()){
            disclaimerDialog()
        }

        BounceView.addAnimTo(findViewById<TextView>(R.id.txt_sign_in))
        findViewById<TextView>(R.id.txt_sign_in).setOnClickListener {
            startActivity(Intent(this@SignUpActivity, SignInActivity::class.java))
        }

        BounceView.addAnimTo(findViewById<AppCompatButton>(R.id.btn_sign_up))
        findViewById<AppCompatButton>(R.id.btn_sign_up).setOnClickListener {

            if(Utility.isStringNull(etUsername.text.toString())){
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_username))
                return@setOnClickListener
            }

            if (Utility.isValidPhoneNo(etPhone.text.toString())) {
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_phone))
                return@setOnClickListener
            }

            if(Utility.isStringNull(etPassword.text.toString())){
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_password))
                return@setOnClickListener
            }

            if(Utility.isValidPassword(etPassword.text.toString())){
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_password_min))
                return@setOnClickListener
            }

            if (Utility.isInternetConnection(this@SignUpActivity)) {
                FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Utility.errorSnackBar(findViewById(android.R.id.content),"Server Error please try later")
                        return@OnCompleteListener
                    }
                    fcmToken = task.result
                    Log.e("onComplete: ", fcmToken)
                    sendOTP()
                })
            }else{
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internet_msg))
            }
        }
    }

    private fun disclaimerDialog() {
        val mBottomSheetDialog = Dialog(this@SignUpActivity, R.style.MaterialDialogSheet)
        mBottomSheetDialog.setContentView(R.layout.dialog_disclaimer)
        mBottomSheetDialog.setCancelable(false)
        mBottomSheetDialog.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        mBottomSheetDialog.setCanceledOnTouchOutside(false)

        val btnSubmit = mBottomSheetDialog.findViewById<AppCompatButton>(R.id.btn_submit)

        btnSubmit.setOnClickListener {

            helper.setFirst(true)
            mBottomSheetDialog.dismiss()
        }

        mBottomSheetDialog.show()
    }

    private fun sendOTP() {
        progressView.view()
        val call: Call<MessageData?>? = ApiMain.getClient.sendotp(etPhone.text.toString(),"signin")
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                } else if(response.body()!!.error == false){
                    val intent = Intent(this@SignUpActivity, OTPVerifyActivity::class.java)
                    intent.putExtra("number",etPhone.text.toString())
                    intent.putExtra("type","signin")
                    startActivityForResult(intent, 2)
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 2 && resultCode == RESULT_OK) {
            register()
        }
    }

    private fun register() {
        val androidId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        progressView.view()
        val call: Call<LoginData?>? = ApiMain.getClient.register(etUsername.text.toString(),
            etPhone.text.toString(), etPassword.text.toString(),
            fcmToken,androidId)

        call!!.enqueue(object : Callback<LoginData?> {
            override fun onResponse(call: Call<LoginData?>, response: Response<LoginData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.body()!!.error == false){
                    Utility.successSnackBar(findViewById(android.R.id.content),response.body()!!.message!!)
                    if(response.body()!!.data!!.userDetails!!.status.equals("active")){
                        helper.setLogin(true)
                        helper.setUserPin(response.body()!!.data!!.userDetails!!.securityPin)
                        helper.setName(response.body()!!.data!!.userDetails!!.name)
                        helper.setPhone(response.body()!!.data!!.userDetails!!.phoneNumber)
                        helper.setProfilePic(response.body()!!.data!!.userDetails!!.profilePicture)
                        helper.setEmail(response.body()!!.data!!.userDetails!!.email)
                        helper.setBalance(response.body()!!.data!!.userDetails!!.availableBalance)
                        helper.setAuth(response.body()!!.data!!.userDetails!!.token)
                        helper.setTransfer(response.body()!!.data!!.userDetails!!.transfer)
                        helper.setBetting(response.body()!!.data!!.userDetails!!.betting)
                        helper.setNotifyStatus(response.body()!!.data!!.userDetails!!.notiStatus)
                        helper.setLoginMsg(response.body()!!.data!!.userDetails!!.loginMsg)
                        val intent = Intent(this@SignUpActivity, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                    }else{
                        Utility.errorSnackBar(findViewById(android.R.id.content),"User Inactive,please contact admin.")
                    }
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<LoginData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }
}