package com.mkxonline.smapp.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.api.model.MessageData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResetPasswordActivity : AppCompatActivity() {

    lateinit var progressView: ProgressView

    private lateinit var etNewPassword: TextInputEditText
    private lateinit var etNewConfirmPassword: TextInputEditText
    var number = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        number = intent.getStringExtra("number").toString()
        progressView = ProgressView(this@ResetPasswordActivity)

        etNewPassword = findViewById(R.id.et_new_password)
        etNewConfirmPassword = findViewById(R.id.et_new_confirm_password)

        BounceView.addAnimTo(findViewById<AppCompatButton>(R.id.btn_reset))
        findViewById<AppCompatButton>(R.id.btn_reset).setOnClickListener {
            if(Utility.isStringNull(etNewPassword.text.toString())){
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_password))
                return@setOnClickListener
            }

            if(Utility.isValidPassword(etNewPassword.text.toString())){
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_password_min))
                return@setOnClickListener
            }

            if(Utility.isStringNull(etNewConfirmPassword.text.toString())){
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_password))
                return@setOnClickListener
            }

            if(Utility.isValidPassword(etNewConfirmPassword.text.toString())){
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_password_min))
                return@setOnClickListener
            }

            if(etNewConfirmPassword.text.toString() != etNewPassword.text.toString()){
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_password_not_match))
                return@setOnClickListener
            }

            resetPassword()
        }
    }

    private fun resetPassword() {
        progressView.view()
        val call: Call<MessageData?>? = com.mkxonline.smapp.api.ApiMain.getClient.forgotPassword(number,etNewPassword.text.toString())
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@ResetPasswordActivity)
                }else if(response.body()!!.error == false){
                    Utility.successSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                    startActivity(Intent(this@ResetPasswordActivity, SignInActivity::class.java))
                    finish()
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()

            }
        })
    }
}