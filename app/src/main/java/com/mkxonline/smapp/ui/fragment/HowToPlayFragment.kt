package com.mkxonline.smapp.ui.fragment

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.api.model.HowtoplayData
import com.github.hariprasanths.bounceview.BounceView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HowToPlayFragment : Fragment() {

    private lateinit var txt: TextView
    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    var link = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_how_to_play, container, false)

        helper = SessionManager(requireActivity())
        progressView = ProgressView(requireActivity())

        txt = view.findViewById(R.id.txt)

        BounceView.addAnimTo(view.findViewById<AppCompatButton>(R.id.btn_link))
        view.findViewById<AppCompatButton>(R.id.btn_link).setOnClickListener {
            if (Utility.isInternetConnection(requireActivity())){
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(link)
                startActivity(i)
            }
        }

        getSettings()

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    private fun getSettings() {
        progressView.view()
        val call: Call<HowtoplayData?>? = com.mkxonline.smapp.api.ApiMain.getClient.howtoPlay("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<HowtoplayData?> {
            override fun onResponse(call: Call<HowtoplayData?>, response: Response<HowtoplayData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        txt.text = Html.fromHtml(response.body()!!.data!!.result!!.text!!, Html.FROM_HTML_MODE_COMPACT)
                    } else {
                        txt.text = Html.fromHtml(response.body()!!.data!!.result!!.text!!)
                    }
                    link = response.body()!!.data!!.result!!.link!!

                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<HowtoplayData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }
}
