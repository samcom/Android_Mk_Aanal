package com.mkxonline.smapp.ui.fragment

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.MessageData
import com.mkxonline.smapp.api.model.SettingsData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.apputil.interfaces.ToolbarTitleInterface
import com.google.android.material.textfield.TextInputEditText
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddFundFragment : Fragment() {

    private lateinit var btnAddFunds: AppCompatButton
    private lateinit var etPoints: TextInputEditText

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    private lateinit var rgroup: RadioGroup

    private var paymentMethod = -1

    private var GOOGLE_PAY_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user"
    private var PHONE_PE_PACKAGE_NAME = "com.phonepe.app"
    private var PAYTM_PACKAGE_NAME = "net.one97.paytm"
    private var GOOGLE_PAY_REQUEST_CODE = 3
    private var PHONE_PE_REQUEST_CODE = 6
    private var PAYTM_REQUEST_CODE = 9
    private var PAYMENT_REQUEST_CODE = 12

    private lateinit var toolbarTitleInterface: ToolbarTitleInterface

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_add_fund, container, false)

        progressView = ProgressView(requireActivity())
        helper = SessionManager(requireActivity())

        activity?.let {
            instantiateNavigationInterface(it)
        }

        etPoints = view.findViewById(R.id.et_points)
        rgroup = view.findViewById(R.id.rgroup)

        btnAddFunds = view.findViewById(R.id.btn_add_fund)

        btnAddFunds.setOnClickListener {
            val selectedId: Int = rgroup.checkedRadioButtonId
            if(selectedId == R.id.google_pay){
                paymentMethod = 1
            }else if(selectedId == R.id.paytm){
                paymentMethod = 2
            }else{
                paymentMethod = 3
            }

            if (!validPoints()) {
                return@setOnClickListener
            }

            if (Utility.isInternetConnection(requireActivity())) {
                getSettings()
            }else{
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
            }
        }

        toolbarTitleInterface.changeTitle("Add Point")

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, WalletFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    private fun instantiateNavigationInterface(context: FragmentActivity) {
        toolbarTitleInterface = context as ToolbarTitleInterface
    }

    private fun validPoints(): Boolean {
        val points: String = etPoints.text.toString().trim { it <= ' ' }
        return if (points.isEmpty()){
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Please Enter Valid Point")
            false
        }else if (points.toInt() == 0) {
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), "Please Enter Valid Point")
            false
        } else {
            true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (Activity.RESULT_OK == resultCode) {
            if (data!!.getStringExtra("response") != null) {
                val trxt = data.getStringExtra("response")
                Log.d("UPI", "onActivityResult: $trxt")
                upiData(trxt)
            } else {
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Payment Failed , Please try again")
            }
        } else {
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Payment Failed , Please try again")
        }
    }

    private fun upiData(str: String?){
        var status = ""
        var approvalRefNo = ""

        val response = str!!.split("&")
        for (i in response.indices) {
            Log.e("tag", response[i])
            val equalStr = response[i].split("=")
            if (equalStr[0].equals("Status",true)) {
                status = equalStr[1]
            }

            if (equalStr[0].equals("txnId",true)) {
                approvalRefNo= equalStr[1]
            }
        }

        if (status.equals("success",true)) {
            addFund(approvalRefNo)
        }
    }

    private fun addFund(approvalRefNo: String) {
        progressView.view()

        val call: Call<MessageData?>? = com.mkxonline.smapp.api.ApiMain.getClient.addFund("Bearer "+helper.getAuth().toString()
            ,approvalRefNo,paymentMethod,etPoints.text.toString())
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    etPoints.setText("")
                    Utility.successSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    private fun getSettings() {
        progressView.view()
        val call: Call<SettingsData?>? = com.mkxonline.smapp.api.ApiMain.getClient.settings("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<SettingsData?> {
            override fun onResponse(call: Call<SettingsData?>, response: Response<SettingsData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    if(etPoints.text.toString().toInt() >= response.body()!!.data!!.result!!.minDeposit!!){
                        if(etPoints.text.toString().toInt() <= response.body()!!.data!!.result!!.maxDeposit!!){
                            upiMethod(response)
                        }else{
                            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Maximum amount should be "+response.body()!!.data!!.result!!.maxDeposit!!)
                        }
                    }else{
                        Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Minimum amount should be "+response.body()!!.data!!.result!!.minDeposit!!+" or more")
                    }
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<SettingsData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    private fun upiMethod(response: Response<SettingsData?>) {
        if (paymentMethod == 1){
            try{
                val uri: Uri = Uri.parse("upi://pay").buildUpon()
                    .appendQueryParameter("pa", response.body()!!.data!!.result!!.googleUpiId!!)
                    .appendQueryParameter("pn", R.string.app_name.toString())
                    .appendQueryParameter("tn", "Add fund to App")
                    .appendQueryParameter("am", etPoints.text.toString())
                    .appendQueryParameter("cu", "INR")
                    .build()
                val upiPayIntent = Intent(Intent.ACTION_VIEW)
                upiPayIntent.data = uri
                upiPayIntent.setPackage(GOOGLE_PAY_PACKAGE_NAME)
                startActivityForResult(upiPayIntent, GOOGLE_PAY_REQUEST_CODE)
            }catch (e : ActivityNotFoundException){
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Google Pay not installed on you device")
            }
        }else if(paymentMethod == 2){
            try{
                val uri: Uri = Uri.parse("upi://pay").buildUpon()
                    .appendQueryParameter("pa", response.body()!!.data!!.result!!.otherUpiId!!)
                    .appendQueryParameter("pn", R.string.app_name.toString())
                    .appendQueryParameter("tn", "Add fund to App")
                    .appendQueryParameter("am", etPoints.text.toString())
                    .appendQueryParameter("cu", "INR")
                    .build()

                val upiPayIntent = Intent(Intent.ACTION_VIEW)
                upiPayIntent.data = uri
                upiPayIntent.setPackage(PAYTM_PACKAGE_NAME)
                startActivityForResult(upiPayIntent, PAYTM_REQUEST_CODE)
            }catch (e : ActivityNotFoundException){
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"PAYTM not installed on you device")
            }
        }else if(paymentMethod == 3){
            try{
                val uri: Uri = Uri.parse("upi://pay").buildUpon()
                    .appendQueryParameter("pa",  response.body()!!.data!!.result!!.phonepeUpiId!!)
                    .appendQueryParameter("pn", R.string.app_name.toString())
                    .appendQueryParameter("tn", "Add fund to App")
                    .appendQueryParameter("am", etPoints.text.toString())
                    .appendQueryParameter("tr", "261433")
                    .appendQueryParameter("cu", "INR")
                    .build()
                val upiPayIntent = Intent(Intent.ACTION_VIEW)
                upiPayIntent.data = uri
                upiPayIntent.setPackage(PHONE_PE_PACKAGE_NAME)
                startActivityForResult(upiPayIntent, PHONE_PE_REQUEST_CODE)
            }catch (e : ActivityNotFoundException){
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Phone pe not installed on you device")
            }
        }else{
            try{
                val uri: Uri = Uri.parse("upi://pay").buildUpon()
                    .appendQueryParameter("pa", response.body()!!.data!!.result!!.googleUpiId!!)
                    .appendQueryParameter("pn", R.string.app_name.toString())
                    .appendQueryParameter("tn", "Add Points to App")
                    .appendQueryParameter("tr", "261433")
                    .appendQueryParameter("am", etPoints.text.toString())
                    .appendQueryParameter("cu", "INR")
                    .build()
                val upiPayIntent = Intent(Intent.ACTION_VIEW)
                upiPayIntent.data = uri
                val chooser = Intent.createChooser(upiPayIntent, "Pay with")
                startActivityForResult(chooser, PAYMENT_REQUEST_CODE)
            }catch (e : ActivityNotFoundException){
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Google Pay not installed on you device")
            }
        }
    }
}