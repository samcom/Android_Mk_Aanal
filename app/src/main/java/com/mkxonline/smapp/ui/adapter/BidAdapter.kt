package com.mkxonline.smapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.api.model.BidData

class BidAdapter(private var check: Int, private var arrayList: ArrayList<BidData>, private var type: String,
                 private val customClick: Callbacks) : RecyclerView.Adapter<BidAdapter.ViewHolderContact>() {
    interface Callbacks {
        fun onClick(s: BidData)
    }

    override fun onCreateViewHolder(parent
                                     : ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_point, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {
        val s = arrayList[viewholder.adapterPosition]

        viewholder.txtPoints.text = "Points: "+ Utility.decimalFormat(s.points)

        if(check == 1){
            viewholder.imgClose.visibility = View.GONE
        }else{
            viewholder.imgClose.visibility = View.VISIBLE
        }

        if(type == Utility.SINGLE_DIGIT){
            if(s.session == "open"){
                viewholder.txtOpenClose.text = "Open Digit: "+s.open_digit
            }else{
                viewholder.txtOpenClose.text = "Close Digit: "+s.close_digit
            }
        }else if (type == Utility.JODI_DIGIT){
            viewholder.txtOpenClose.text = "Open Digit: "+s.open_digit
        }else if(type == Utility.SINGLE_PANA || type == Utility.DOUBLE_PANA || type == Utility.TRIPLE_PANA){
            if(s.session == "open"){
                viewholder.txtOpenClose.text = "Open Pana: "+s.open_pana
            }else{
                viewholder.txtOpenClose.text = "Close Pana: "+s.close_pana
            }
        }else if(type == Utility.HALF_SANGAM){
            if(s.session == "open"){
                viewholder.txtOpenClose.text = "Open Digit: "+s.open_digit + "\nClose Pana: "+s.close_pana
            }else{
                viewholder.txtOpenClose.text = "Open Pana: "+s.open_pana +"\nClose Digit: "+s.close_digit
            }
        }else if(type == Utility.FULL_SANGAM){
            viewholder.txtOpenClose.text = "Open Pana: "+s.open_pana +"\nClose Pana: "+s.close_pana
        }

        viewholder.imgClose.setOnClickListener {
            customClick.onClick(arrayList[viewholder.adapterPosition])
        }
    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {
        var txtOpenClose: TextView = view.findViewById(R.id.txt_open_close)
        var txtPoints: TextView = view.findViewById(R.id.txt_points)
        var imgClose: ImageView = view.findViewById(R.id.img_close)
    }

}
