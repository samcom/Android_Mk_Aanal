package com.mkxonline.smapp.ui.fragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView

class WalletFragment : Fragment() {

    lateinit var progressView: ProgressView
    lateinit var helper: SessionManager

    private lateinit var txtAddPoint: ImageView
    private lateinit var txtWithdraw: ImageView
    private lateinit var txtMethod: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_wallet, container, false)

        progressView = ProgressView(requireActivity())
        helper = SessionManager(requireActivity())

        txtAddPoint = view.findViewById(R.id.txt_add_point)
        txtWithdraw = view.findViewById(R.id.txt_withdraw)
        txtMethod = view.findViewById(R.id.txt_method)

        if(arguments!=null) {
            redirection(arguments?.getString("page")!!)
        }else{
            redirection("0")
        }

        txtAddPoint.setOnClickListener {
            redirection("1")
        }

        txtWithdraw.setOnClickListener {
            redirection("2")
        }

        txtMethod.setOnClickListener {
            redirection("4")
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    private fun redirection(direct: String){
        if(direct == "1"){
            txtAddPoint.setImageResource(R.drawable.select_add_point)
            txtWithdraw.setImageResource(R.drawable.unselect_withdraw)
            txtMethod.setImageResource(R.drawable.unselect_method)

            childFragmentManager.beginTransaction()
                .replace(R.id.content_frame_sub, AddFundFragment())
                .commit()
        }else if(direct == "2"){
            txtAddPoint.setImageResource(R.drawable.unselect_add_point)
            txtWithdraw.setImageResource(R.drawable.select_withdraw)
            txtMethod.setImageResource(R.drawable.unselect_method)

            childFragmentManager.beginTransaction()
                .replace(R.id.content_frame_sub, WithdrawFragment())
                .commit()
        }else if(direct == "4"){
            txtAddPoint.setImageResource(R.drawable.unselect_add_point)
            txtWithdraw.setImageResource(R.drawable.unselect_withdraw)
            txtMethod.setImageResource(R.drawable.select_method)

            childFragmentManager.beginTransaction()
                .replace(R.id.content_frame_sub, MethodFragment())
                .commit()
        }else {
            childFragmentManager.beginTransaction()
                .replace(R.id.content_frame_sub, WalletHistoryFragment())
                .commit()
        }
    }

}