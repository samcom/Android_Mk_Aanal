package com.mkxonline.smapp.ui.adapter

 import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
 import com.mkxonline.smapp.apputil.Utility
 import com.mkxonline.smapp.api.model.StarLineBidData

class StarLineBidAdapter(private var arrayList: ArrayList<StarLineBidData>, private var type: String, private val customClick: Callbacks) : RecyclerView.Adapter<StarLineBidAdapter.ViewHolderContact>() {

    interface Callbacks {
        fun onClick(s: StarLineBidData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_point, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {
        val s = arrayList[position]

        viewholder.txtPoints.text = Utility.decimalFormat(s.points)+" Points"

        if(type == Utility.SINGLE_DIGIT){
            viewholder.txtOpenClose.text = "Open Digit: "+s.open_digit
        }else if(type == Utility.SINGLE_PANA || type == Utility.DOUBLE_PANA || type == Utility.TRIPLE_PANA){
            viewholder.txtOpenClose.text = "Open Pana: "+s.open_pana
        }

        viewholder.imgClose.setOnClickListener {
            customClick.onClick(arrayList[position])
        }
    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {

        var txtOpenClose: TextView = view.findViewById(R.id.txt_open_close)
        var txtPoints: TextView = view.findViewById(R.id.txt_points)
        var imgClose: ImageView = view.findViewById(R.id.img_close)
    }

}
