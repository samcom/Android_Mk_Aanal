package com.mkxonline.smapp.ui.fragment

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.api.model.BidHistoryData
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.ui.adapter.BidHistoryAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class BidHistoryFragment : Fragment() {

    private lateinit var etStartDate: TextInputEditText
    private lateinit var etEndDate: TextInputEditText
    private lateinit var btnSearch: AppCompatButton

    private lateinit var swipeContainer: SwipeRefreshLayout
    private val dataDetails: ArrayList<BidHistoryData.Data.Result.DataItem> = ArrayList()
    private lateinit var bidHistoryAdapter: BidHistoryAdapter
    lateinit var progressView: ProgressView
    private lateinit var rvBidHistory : RecyclerView
    lateinit var helper: SessionManager

    private var startDate = ""
    private var endDate = ""

    private var currentPage = 1
    private var lastPage = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_bid_history, container, false)

        progressView = ProgressView(requireActivity())
        helper = SessionManager(requireActivity())

        swipeContainer = view.findViewById(R.id.swipeContainer)
        etStartDate = view.findViewById(R.id.et_start_date)
        etEndDate = view.findViewById(R.id.et_end_date)
        btnSearch = view.findViewById(R.id.btn_search)

        swipeContainer.setOnRefreshListener {
            onResume()
        }

        val df = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val formattedDate: String = df.format(Calendar.getInstance().time)
        etStartDate.setText(formattedDate)
        etEndDate.setText(formattedDate)

        val df1 = SimpleDateFormat("yyyy-MM-ddd", Locale.getDefault())
        startDate = df1.format(Calendar.getInstance().time)
        endDate = df1.format(Calendar.getInstance().time)

        etStartDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(requireActivity(), { view, year, monthOfYear, dayOfMonth ->
                etStartDate.setText(dayOfMonth.toString() + "-" + (monthOfYear+1) + "-" + year.toString())
                startDate = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth.toString()
                }, Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
            datePickerDialog.show()
        }

        etEndDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(requireActivity(), { view, year, monthOfYear, dayOfMonth ->
                etEndDate.setText(dayOfMonth.toString() + "-" + (monthOfYear+1) + "-" + year.toString())
                endDate = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth.toString()
                }, Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
            datePickerDialog.show()
        }

        rvBidHistory = view.findViewById(R.id.rv_bid_history)
        rvBidHistory.layoutManager = LinearLayoutManager(activity)
        rvBidHistory.itemAnimator = DefaultItemAnimator()

        bidHistoryAdapter = BidHistoryAdapter(dataDetails)
        rvBidHistory.adapter = bidHistoryAdapter

        rvBidHistory.addOnScrollListener(object : OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    if (currentPage == lastPage) {
                        return
                    }
                    getBidHistory(++currentPage)
                }
            }
        })

        BounceView.addAnimTo(btnSearch)
        btnSearch.setOnClickListener {
            onResume()
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    override fun onResume() {
        super.onResume()
        if (Utility.isInternetConnection(requireActivity())) {
            getBidHistory(currentPage)
        }else{
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
        }
    }

    private fun getBidHistory(page: Int) {
        if (page == 1) {
            dataDetails.clear()
            bidHistoryAdapter.notifyDataSetChanged()
        }

        progressView.view()

        val call: Call<BidHistoryData?>? = com.mkxonline.smapp.api.ApiMain.getClient.bidHistory("Bearer "+helper.getAuth().toString(),page.toString(),startDate,endDate)
        call!!.enqueue(object : Callback<BidHistoryData?> {
            override fun onResponse(call: Call<BidHistoryData?>, response: Response<BidHistoryData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    if (isAttachedToActivity()) {
//                        currentPage = response.body()!!.data!!.result!!.currentPage!!
//                        lastPage = response.body()!!.data!!.result!!.lastPage!!
                        dataDetails.addAll(response.body()!!.data!!.result!!.data!!)
                        bidHistoryAdapter.notifyDataSetChanged()
                    }
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<BidHistoryData?>, t: Throwable) {
                t.printStackTrace()
                progressView.hide()
                
            }
        })
    }

    fun isAttachedToActivity(): Boolean {
        return isVisible && activity != null
    }
}