package com.mkxonline.smapp.ui.fragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.MessageData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.apputil.interfaces.ToolbarTitleInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddBankFragment : Fragment()  {

    private lateinit var etUsername: TextInputEditText
    private lateinit var etAccountNumber: TextInputEditText
    private lateinit var etConfirmAccountNumber: TextInputEditText
    private lateinit var etIfscCode: TextInputEditText
    private lateinit var etBankName: TextInputEditText
    private lateinit var etBankAddress: TextInputEditText

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    private lateinit var toolbarTitleInterface: ToolbarTitleInterface

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_add_bank, container, false)

        helper = SessionManager(requireActivity())
        progressView = ProgressView(requireActivity())

        etUsername = view.findViewById(R.id.et_username)
        etAccountNumber = view.findViewById(R.id.et_account_number)
        etConfirmAccountNumber = view.findViewById(R.id.et_confirm_account_number)
        etIfscCode = view.findViewById(R.id.et_ifsc_code)
        etBankName = view.findViewById(R.id.et_bank_name)
        etBankAddress = view.findViewById(R.id.et_bank_address)

        activity?.let {
            instantiateNavigationInterface(it)
        }

        view.findViewById<TextView>(R.id.btn_submit).setOnClickListener {
            if (!validUserName()) {
                return@setOnClickListener
            }

            if (!validNumber()) {
                return@setOnClickListener
            }

            if (!validConfirmNumber()) {
                return@setOnClickListener
            }

            if (!validIFSC()) {
                return@setOnClickListener
            }

            if (!validBankName()) {
                return@setOnClickListener
            }

            if (!validBankAddress()) {
                return@setOnClickListener
            }

            addBank()
        }

        toolbarTitleInterface.changeTitle("Methods")

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, WalletFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    private fun instantiateNavigationInterface(context: FragmentActivity) {
        toolbarTitleInterface = context as ToolbarTitleInterface
    }

    private fun validUserName(): Boolean {
        val str: String = etUsername.text.toString().trim {  it <= ' ' }
        return if (str.isEmpty()) {
            Snackbar.make(requireActivity().findViewById(android.R.id.content),"Please Enter User Name", Snackbar.LENGTH_LONG).show()
            false
        } else {
            true
        }
    }

    private fun validNumber(): Boolean {
        val str: String = etAccountNumber.text.toString().trim {  it <= ' ' }
        return if (str.isEmpty()) {
            Snackbar.make(requireActivity().findViewById(android.R.id.content),"Please Enter Account Number", Snackbar.LENGTH_LONG).show()
            false
        } else {
            true
        }
    }

    private fun validConfirmNumber(): Boolean {
        val str: String = etConfirmAccountNumber.text.toString().trim {  it <= ' ' }
        return if (str.isEmpty()) {
            Snackbar.make(requireActivity().findViewById(android.R.id.content),"Please Enter Confirm Account Number", Snackbar.LENGTH_LONG).show()
            false
        } else str == etAccountNumber.text.toString()
    }

    private fun validIFSC(): Boolean {
        val str: String = etIfscCode.text.toString().trim {  it <= ' ' }
        return if (str.isEmpty()) {
            Snackbar.make(requireActivity().findViewById(android.R.id.content),"Please Enter IFSC", Snackbar.LENGTH_LONG).show()
            false
        } else {
            true
        }
    }

    private fun validBankName(): Boolean {
        val str: String = etBankName.text.toString().trim {  it <= ' ' }
        return if (str.isEmpty()) {
            Snackbar.make(requireActivity().findViewById(android.R.id.content),"Please Enter Bank Name", Snackbar.LENGTH_LONG).show()
            false
        } else {
            true
        }
    }

    private fun validBankAddress(): Boolean {
        val str: String = etBankAddress.text.toString().trim {  it <= ' ' }
        return if (str.isEmpty()) {
            Snackbar.make(requireActivity().findViewById(android.R.id.content),"Please Enter Bank Address", Snackbar.LENGTH_LONG).show()
            false
        } else {
            true
        }
    }

    private fun addBank() {
        progressView.view()
        val call: Call<MessageData?>? = com.mkxonline.smapp.api.ApiMain.getClient.updateBankDetails("Bearer "+helper.getAuth().toString()
            , etUsername.text.toString(), etAccountNumber.text.toString(),etIfscCode.text.toString(),etBankName.text.toString(),etBankAddress.text.toString())
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    val bundle = Bundle()
                    bundle.putString("page", "2")

                    val fragment = WalletFragment()
                    fragment.arguments = bundle

                    requireActivity().supportFragmentManager.beginTransaction()
                        .replace(R.id.content_frame,fragment)
                        .commit()
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }else{

                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

}