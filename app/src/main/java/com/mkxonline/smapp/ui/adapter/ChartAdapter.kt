package com.mkxonline.smapp.ui.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.ResultChartData

internal class ChartAdapter(private val courseList: List<ResultChartData.Data.ResultItem>, private val context: Context) : BaseAdapter() {
    private var layoutInflater: LayoutInflater? = null
    private lateinit var txtDay: TextView
    private lateinit var txtDate: TextView
    private lateinit var txtLeft: TextView
    private lateinit var txtCenter: TextView
    private lateinit var txtRight: TextView

    override fun getCount(): Int {
        return courseList.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }
    
    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var mConvertView = convertView

        if (layoutInflater == null) {
            layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        if (mConvertView == null) {
            mConvertView = layoutInflater!!.inflate(R.layout.item_chart, null)
        }

        txtDay = mConvertView!!.findViewById(R.id.txt_day)
        txtDate = mConvertView.findViewById(R.id.txt_date)
        txtLeft = mConvertView.findViewById(R.id.txt_left)
        txtCenter = mConvertView.findViewById(R.id.txt_center)
        txtRight = mConvertView.findViewById(R.id.txt_right)

        txtDay.text = courseList[position].date!!.substring(0, courseList[position].date!!.indexOf(' '))
        txtDate.text = courseList[position].date!!.substring(courseList[position].date!!.indexOf(' ') + 1)
        txtLeft.text = courseList[position].left
        txtCenter.text = courseList[position].center
        txtRight.text = courseList[position].right

        txtLeft.setTextColor(Color.parseColor(courseList[position].color))
        txtCenter.setTextColor(Color.parseColor(courseList[position].color))
        txtRight.setTextColor(Color.parseColor(courseList[position].color))

        return mConvertView
    }
}