package com.mkxonline.smapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.ChartData

class SubChartAdapter(var context: Context, var arrayList: List<ChartData>, val type: Int) : RecyclerView.Adapter<SubChartAdapter.ViewHolderContact>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_sub_chart, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {
        if(type==1){
            viewholder.txtDay.text = arrayList[position].text1
            viewholder.txtCenter.text = arrayList[position].text2
        }else {
            val typeface = ResourcesCompat.getFont(context, R.font.interbold)
            viewholder.txtDay.typeface = typeface
            viewholder.txtDay.textSize = 7f
            viewholder.txtDay.text = arrayList[position].text1
            viewholder.txtCenter.visibility = View.GONE
        }
    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {
        var txtDay: TextView = view.findViewById(R.id.txt_day)
        var txtCenter: TextView = view.findViewById(R.id.txt_center)

    }
}
