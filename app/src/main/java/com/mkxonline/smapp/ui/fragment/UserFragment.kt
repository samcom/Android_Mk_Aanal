package com.mkxonline.smapp.ui.fragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.apputil.interfaces.UserNameChangeInterface
import com.mkxonline.smapp.api.model.DashboardData
import com.mkxonline.smapp.api.model.MessageData
import com.mkxonline.smapp.apputil.Utility
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class UserFragment : Fragment() {

    private lateinit var edtUsername: TextInputEditText
    private lateinit var btnUpdate: AppCompatButton
    private lateinit var txtPhone: TextView

    lateinit var progressView: ProgressView
    lateinit var helper: SessionManager

    private var profilePic: File? = null

    private lateinit var swipeContainer: SwipeRefreshLayout
    private lateinit var userNameInterface: UserNameChangeInterface

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_user, container, false)

        progressView = ProgressView(requireActivity())
        helper = SessionManager(requireActivity())

        edtUsername = view.findViewById(R.id.et_username)
        txtPhone = view.findViewById(R.id.txt_phone)
        btnUpdate = view.findViewById(R.id.btn_update)
        BounceView.addAnimTo(btnUpdate)

        activity?.let {
            instantiateNavigationInterface(it)
        }

        swipeContainer = view.findViewById(R.id.swipeContainer)
        swipeContainer.setOnRefreshListener {
            if (Utility.isInternetConnection(requireActivity())) {
                dashboard()
            }else{
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
            }
        }

        edtUsername.setText(helper.getName().toString())
        txtPhone.text = helper.getPhone().toString()

        btnUpdate.setOnClickListener {
            if (Utility.isInternetConnection(requireActivity())) {
                update()
            }else{
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
            }
        }

        if (Utility.isInternetConnection(requireActivity())) {
            dashboard()
        }else{
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    private fun instantiateNavigationInterface(context: FragmentActivity) {
        userNameInterface = context as UserNameChangeInterface
    }

    private fun update() {
        progressView.view()

        var body = MultipartBody.Part.createFormData("profile_pic", "", RequestBody.create(MediaType.parse("text/plain"), ""))
        if(profilePic!=null){
            val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), profilePic!!)
            body = MultipartBody.Part.createFormData("profile_pic", profilePic!!.name, reqFile)
        }

        val name = RequestBody.create(MediaType.parse("text/plain"), edtUsername.text.toString())

        val call: Call<MessageData?>? = com.mkxonline.smapp.api.ApiMain.getClient.updateProfile("Bearer "+helper.getAuth().toString(),
        name,body)

        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Internal Server error, Please try after some time")
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    Utility.successSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                    userNameInterface.changeUserName(edtUsername.text.toString())
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
                
            }
        })

    }

    private fun dashboard() {
        progressView.view()
        val call: Call<DashboardData?>? = com.mkxonline.smapp.api.ApiMain.getClient.dashboard("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<DashboardData?> {
            override fun onResponse(call: Call<DashboardData?>, response: Response<DashboardData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Internal Server error, Please try after some time")
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    helper.setName(response.body()!!.data!!.result!!.name)
                    helper.setProfilePic(response.body()!!.data!!.result!!.profilePicture)
                    helper.setPhone(response.body()!!.data!!.result!!.phoneNumber)
                    helper.setEmail(response.body()!!.data!!.result!!.email)
                    helper.setBalance(response.body()!!.data!!.result!!.availableBalance)
                    helper.setTransfer(response.body()!!.data!!.result!!.transfer)
                    helper.setBetting(response.body()!!.data!!.result!!.betting)
                    helper.setNotifyStatus(response.body()!!.data!!.result!!.notiStatus)

                    edtUsername.setText(response.body()!!.data!!.result!!.username)
                    txtPhone.text = response.body()!!.data!!.result!!.phoneNumber

                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<DashboardData?>, t: Throwable) {
                progressView.hide()
                
            }
        })

    }
}