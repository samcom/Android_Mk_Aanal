package com.mkxonline.smapp.ui.fragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.RateData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.ui.adapter.RateAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RateFragment : Fragment() {

    private val dataDetails: ArrayList<Array<String>> = ArrayList()

    private lateinit var rateAdapter: RateAdapter
    lateinit var progressView: ProgressView
    lateinit var rvRate : RecyclerView
    lateinit var helper: SessionManager

    private lateinit var swipeContainer: SwipeRefreshLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_rate, container, false)

        progressView = ProgressView(requireActivity())
        helper = SessionManager(requireActivity())

        rvRate = view.findViewById(R.id.rv_rate)
        rvRate.layoutManager = LinearLayoutManager(activity)
        rvRate.itemAnimator = DefaultItemAnimator()

        swipeContainer = view.findViewById(R.id.swipeContainer)
        swipeContainer.setOnRefreshListener {
            onResume()
        }
        rateAdapter = RateAdapter(dataDetails)


        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    override fun onResume() {
        super.onResume()

        if (Utility.isInternetConnection(requireActivity())) {
            getRate()
        }else{
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
        }
    }

    private fun getRate(){
        progressView.view()

        dataDetails.clear()
        val call: Call<RateData?>? = com.mkxonline.smapp.api.ApiMain.getClient.gameRate("Bearer "+helper.getAuth().toString())
        call!!.enqueue(object : Callback<RateData?> {
            override fun onResponse(call: Call<RateData?>, response: Response<RateData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false
                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    if (isAttachedToActivity()) {
                        dataDetails.addAll(response.body()!!.data!!.result!!)
                        rvRate.adapter = rateAdapter
                    }
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<RateData?>, t: Throwable) {
                progressView.hide()

                swipeContainer.isRefreshing = false

                
            }
        })
    }

    fun isAttachedToActivity(): Boolean {
        return isVisible && activity != null
    }

}