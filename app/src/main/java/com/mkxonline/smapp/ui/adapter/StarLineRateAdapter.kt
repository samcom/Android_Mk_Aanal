package com.mkxonline.smapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R

class StarLineRateAdapter(private var arrayList: ArrayList<Array<String>>) : RecyclerView.Adapter<StarLineRateAdapter.ViewHolderContact>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_starline_rate, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {
        val s = arrayList[position]
        viewholder.txtName.text = s[0]
        viewholder.txtRate.text = s[1]
    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {
        var txtName: TextView = view.findViewById(R.id.txt_name)
        var txtRate: TextView = view.findViewById(R.id.txt_rate)
    }

}
