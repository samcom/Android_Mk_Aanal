package com.mkxonline.smapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.WithdrawData
import com.mkxonline.smapp.apputil.Utility.Companion.mFormatDate1
import com.mkxonline.smapp.apputil.Utility.Companion.mFormatDate2
import java.text.ParseException
import java.util.*

class WithdrawHistoryAdapter(var context: Context, private var arrayList: ArrayList<WithdrawData.Data.Result.DataItem>) : RecyclerView.Adapter<WithdrawHistoryAdapter.ViewHolderContact>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_withdraw, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {
        val s = arrayList[position]

        viewholder.txtRemark.text = "Remark: "+s.remark
        viewholder.txtHistoryDetails.text = "Payment Method: "+s.paymentMethod

        if(s.status.equals("1")){
            viewholder.txtBalance.text = "-"+s.amount
            viewholder.txtLabel.text = "Approved"
            viewholder.txtLabel.backgroundTintList = context.resources.getColorStateList(R.color.green)
        }else if(s.status.equals("0")){
            viewholder.txtBalance.text = "+"+s.amount
            viewholder.txtLabel.text = "Reject"
            viewholder.txtLabel.backgroundTintList =  context.resources.getColorStateList(R.color.red)
        }else{
            viewholder.txtBalance.text = "-"+s.amount
            viewholder.txtLabel.text = "Pending"
            viewholder.txtLabel.backgroundTintList = context.resources.getColorStateList(R.color.Dblue)
        }

        try {
            val date = mFormatDate1.parse(s.requestDate!!)
            val calendar = Calendar.getInstance()
            calendar.time = date!!
            viewholder.txtTime.text = mFormatDate2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {
        var txtLabel: TextView = view.findViewById(R.id.txt_label)
        var txtRemark: TextView = view.findViewById(R.id.txt_remark)
        var txtTime: TextView = view.findViewById(R.id.txt_time)
        var txtBalance: TextView = view.findViewById(R.id.txt_balance)
        var txtHistoryDetails: TextView = view.findViewById(R.id.txt_history_details)
    }

}
