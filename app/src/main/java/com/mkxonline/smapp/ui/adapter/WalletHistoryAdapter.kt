package com.mkxonline.smapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.WalletHistoryData
import com.mkxonline.smapp.apputil.Utility.Companion.mFormatDate1
import com.mkxonline.smapp.apputil.Utility.Companion.mFormatDate3
import com.mkxonline.smapp.apputil.Utility.Companion.mFormatDate4
import java.text.ParseException
import java.util.Calendar

class WalletHistoryAdapter(private var arrayList: ArrayList<WalletHistoryData.Data.Result.DataItem>) : RecyclerView.Adapter<WalletHistoryAdapter.ViewHolderContact>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_wallet_history, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {
        val s = arrayList[position]

        viewholder.txtHistoryDetails.text = s.transactionNote
        if(s.type.equals("credit")){
            viewholder.txtBalance.text = "+"+s.amount
        }else{
            viewholder.txtBalance.text = "-"+s.amount
        }

        try {
            val date = mFormatDate1.parse(s.date!!)
            val calendar = Calendar.getInstance()

            calendar.time = date!!
            viewholder.txtTime.text = mFormatDate3.format(date)
            viewholder.txtDate.text = mFormatDate4.format(date)

        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {
        var txtTime: TextView = view.findViewById(R.id.txt_time)
        var txtDate: TextView = view.findViewById(R.id.txt_date)
        var txtBalance: TextView = view.findViewById(R.id.txt_balance)
        var txtHistoryDetails: TextView = view.findViewById(R.id.txt_history_details)
    }

}
