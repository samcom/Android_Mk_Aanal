package com.mkxonline.smapp.ui.fragment

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.api.model.DashboardData
import com.mkxonline.smapp.api.model.WalletNumberData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NumberFragment : DialogFragment() {

    companion object {
        fun newInstance(index: Int): NumberFragment {
            val f = NumberFragment()
            val args = Bundle()
            args.putInt("index", index)
            f.arguments = args
            return f
        }
    }

    private lateinit var imageUpi: ImageView

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView
    private lateinit var etPhone: TextInputEditText

    var googleNumber = ""
    var phonePayNumber = ""
    var paytmNumber = ""
    var index = 0

    override fun onStart() {
        super.onStart()
        val height = (resources.displayMetrics.heightPixels * 0.60).toInt()

        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, height)

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.setGravity(Gravity.BOTTOM)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_upi, container, false)

        index = requireArguments().getInt("index", 0)

        helper = SessionManager(requireActivity())
        progressView = ProgressView(requireActivity())

        imageUpi = view.findViewById(R.id.image_upi)
        etPhone = view.findViewById(R.id.et_phone)

        if(index == 1){
            imageUpi.setImageResource(R.drawable.method_paytm)
        }else if(index == 2){
            imageUpi.setImageResource(R.drawable.method_google_pay)
        }else{
            imageUpi.setImageResource(R.drawable.method_phonepe)
        }

        view.findViewById<TextView>(R.id.btn_submit).setOnClickListener {
            if (!validPhone()) {
                return@setOnClickListener
            }
            addNumberWallet()
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        dashboard()
    }

    private fun validPhone(): Boolean {
        val str: String = etPhone.text.toString().trim { it <= ' ' }
        return if (str.isEmpty()) {
            Snackbar.make(requireActivity().findViewById(android.R.id.content),"Please Enter Phone Number", Snackbar.LENGTH_LONG).show()
            false
        } else if(str.length < 10){
            Snackbar.make(requireActivity().findViewById(android.R.id.content),"Please Enter Phone Number", Snackbar.LENGTH_LONG).show()
            false
        }else {
            true
        }
    }

    private fun addNumberWallet() {
        progressView.view()
        val call: Call<WalletNumberData?>? = com.mkxonline.smapp.api.ApiMain.getClient.walletsNumber("Bearer "+helper.getAuth().toString()
            ,index.toString(),etPhone.text.toString())
        call!!.enqueue(object : Callback<WalletNumberData?> {
            override fun onResponse(call: Call<WalletNumberData?>, response: Response<WalletNumberData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    dismiss()
                    val bundle = Bundle()
                    bundle.putString("page", "4")

                    val fragment = WalletFragment()
                    fragment.arguments = bundle

                    requireActivity().supportFragmentManager.beginTransaction()
                        .replace(R.id.content_frame,fragment)
                        .commit()

                    Utility.successSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<WalletNumberData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    private fun dashboard() {
        progressView.view()
        val call: Call<DashboardData?>? = com.mkxonline.smapp.api.ApiMain.getClient.dashboard("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<DashboardData?> {
            override fun onResponse(call: Call<DashboardData?>, response: Response<DashboardData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    if (isAttachedToActivity()) {

                        if (response.body()!!.data!!.result!!.googlepayNumber != null && response.body()!!.data!!.result!!.googlepayNumber!!.isNotEmpty()) {
                            googleNumber = response.body()!!.data!!.result!!.googlepayNumber!!
                        }

                        if (response.body()!!.data!!.result!!.phonepayNumber != null && response.body()!!.data!!.result!!.phonepayNumber!!.isNotEmpty()) {
                            phonePayNumber = response.body()!!.data!!.result!!.phonepayNumber!!
                        }

                        if (response.body()!!.data!!.result!!.paytmNumber != null && response.body()!!.data!!.result!!.paytmNumber!!.isNotEmpty()) {
                            paytmNumber = response.body()!!.data!!.result!!.paytmNumber!!
                        }

                        if(index==1){
                            etPhone.setText(paytmNumber)
                        }else if(index==2){
                            etPhone.setText(googleNumber)
                        }else{
                            etPhone.setText(phonePayNumber)
                        }
                    }
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<DashboardData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    fun isAttachedToActivity(): Boolean {
        return isVisible && activity != null
    }
}