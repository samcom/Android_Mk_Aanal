package com.mkxonline.smapp.ui.fragment

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.SettingsData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUsFragment : Fragment() {

    private lateinit var txtPhone: TextView
    private lateinit var txtWhatsapp: TextView
    private lateinit var txtTelegram: TextView

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    var phone: String = ""
    var whatsapp: String = ""
    var telegram: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_contact_us, container, false)

        helper = SessionManager(requireActivity())
        progressView = ProgressView(requireActivity())

        txtPhone = view.findViewById(R.id.txt_phone)
        txtWhatsapp = view.findViewById(R.id.txt_whatsapp)
        txtTelegram = view.findViewById(R.id.txt_telegram)

        txtPhone.setOnClickListener {
            val number = Uri.parse("tel:$phone")
            val callIntent = Intent(Intent.ACTION_DIAL, number)
            startActivity(callIntent)
        }

        txtWhatsapp.setOnClickListener {
            val url = "https://api.whatsapp.com/send?phone=${whatsapp}"
            try {
                val pm: PackageManager = requireActivity().packageManager
                pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES)
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            } catch (e: PackageManager.NameNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
            }
        }

        txtTelegram.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=$telegram"))
                startActivity(intent)
            }catch (e: ActivityNotFoundException) {
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),"Something went wrong, Please try after some time")
            }
        }

        if (Utility.isInternetConnection(requireActivity())) {
            getSettings()
        }else{
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    private fun getSettings() {
        progressView.view()
        val call: Call<SettingsData?>? = com.mkxonline.smapp.api.ApiMain.getClient.settings("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<SettingsData?> {
            override fun onResponse(call: Call<SettingsData?>, response: Response<SettingsData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){

                    if(!response.body()!!.data!!.result!!.mobileNumber.isNullOrBlank()){
                        phone = response.body()!!.data!!.result!!.mobileNumber!!
                    }

                    if(!response.body()!!.data!!.result!!.telegramNumber.isNullOrBlank()){
                        telegram = response.body()!!.data!!.result!!.telegramNumber!!
                    }

                    if(!response.body()!!.data!!.result!!.whatsappNumber.isNullOrBlank()){
                        whatsapp = response.body()!!.data!!.result!!.whatsappNumber!!
                    }

                    txtPhone.text = phone
                    txtWhatsapp.text = whatsapp
                    txtTelegram.text = telegram

                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<SettingsData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }
}