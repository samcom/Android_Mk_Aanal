package com.mkxonline.smapp.ui.fragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.ListData
import com.mkxonline.smapp.api.model.ResultChartData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.ui.adapter.ChartAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChartFragment : Fragment() {

    private val dataDetails: ArrayList<ResultChartData.Data.ResultItem> = ArrayList()
    private lateinit var gridAdapter: ChartAdapter

    private lateinit var idGRV: GridView
    private lateinit var txtResult: TextView
    private lateinit var listDetails: ListData.Data.ResultItem

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_chart, container, false)

        idGRV = view.findViewById(R.id.idGRV)
        txtResult = view.findViewById(R.id.txt_result)
        progressView = ProgressView(requireActivity())
        helper = SessionManager(requireActivity())

        listDetails = arguments?.getSerializable("GameDetails") as ListData.Data.ResultItem
        txtResult.text = listDetails.gameName+" Result Chart"
        gridAdapter = ChartAdapter(dataDetails,requireActivity())

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    override fun onResume() {
        super.onResume()
        if (Utility.isInternetConnection(requireActivity())) {
            getChartData()
        }else{
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
        }
    }

    private fun getChartData(){
        progressView.view()
        dataDetails.clear()
        val call: Call<ResultChartData?>? = com.mkxonline.smapp.api.ApiMain.getClient.resultChart("Bearer "+helper.getAuth().toString(),listDetails.id.toString())
        call!!.enqueue(object : Callback<ResultChartData?> {
            override fun onResponse(call: Call<ResultChartData?>, response: Response<ResultChartData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    if (isAttachedToActivity()) {
                        dataDetails.addAll(response.body()!!.data!!.result!!)
                        idGRV.adapter = gridAdapter
                    }
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<ResultChartData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    fun isAttachedToActivity(): Boolean {
        return isVisible && activity != null
    }
}