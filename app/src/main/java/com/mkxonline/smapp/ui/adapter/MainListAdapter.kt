package com.mkxonline.smapp.ui.adapter

import android.content.Context
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.ListData
import com.mkxonline.smapp.apputil.Utility.Companion.mFormatDate
import com.mkxonline.smapp.apputil.Utility.Companion.mFormatDate3
import java.text.ParseException
import java.util.*

class MainListAdapter(var context: Context, private var arrayList: ArrayList<ListData.Data.ResultItem>, private val customClick: Callbacks) : RecyclerView.Adapter<MainListAdapter.ViewHolderContact>() {

    interface Callbacks {
        fun onClick(s: ListData.Data.ResultItem)
        fun onChartClick(s: ListData.Data.ResultItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_main, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {
        var isClose = false
        val s = arrayList[viewholder.adapterPosition]

        viewholder.txtName.text = s.gameName!!.toUpperCase()
        viewholder.txtMarketInfo.text = s.result

        try {
            val openDate = mFormatDate.parse(s.openTime)
            val endDate = mFormatDate.parse(s.closeTime)
            val calendarEnd = Calendar.getInstance()

            calendarEnd.time = endDate
            calendarEnd.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
            calendarEnd.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
            calendarEnd.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

            viewholder.txtOpen.text = "Open "+ mFormatDate3.format(openDate)+" | Close "+mFormatDate3.format(endDate)

            if(s.openToday.equals("1")) {
                if(Calendar.getInstance().time.before(calendarEnd.time)) {
                    isClose = false
                    viewholder.imgStatus.setImageResource(R.drawable.ic_market_open)
//                    viewholder.txtMarketStatus.text = "Online"
//                    viewholder.txtMarketStatus.setTextColor(context.getColor(R.color.green))
                } else {
                    isClose = true
                    viewholder.imgStatus.setImageResource(R.drawable.ic_market_closed)
//                    viewholder.txtMarketStatus.text = "Offline"
//                    viewholder.txtMarketStatus.setTextColor(context.getColor(R.color.red))
                }
            }else{
                isClose = true
                viewholder.imgStatus.setImageResource(R.drawable.ic_market_closed)
//                viewholder.txtMarketStatus.text = "Offline"
//                viewholder.txtMarketStatus.setTextColor(context.getColor(R.color.red))
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        viewholder.itemView.setOnClickListener {
            if(!isClose){
                customClick.onClick(arrayList[viewholder.adapterPosition])
            }else{
                val vibe = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
                vibe!!.vibrate(100)
            }
        }

        viewholder.imgChart.setOnClickListener {
            customClick.onChartClick(arrayList[viewholder.adapterPosition])
        }
    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {
        var txtName: TextView = view.findViewById(R.id.txt_name)
        var txtOpen: TextView = view.findViewById(R.id.txt_open)
        var txtMarketInfo: TextView = view.findViewById(R.id.txt_market_info)
        var imgChart: ImageView = view.findViewById(R.id.img_chart)
        var imgStatus: ImageView = view.findViewById(R.id.img_status)
//        var txtMarketStatus: TextView = view.findViewById(R.id.txt_market_status)
    }

}
