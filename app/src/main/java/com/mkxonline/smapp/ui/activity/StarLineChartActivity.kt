package com.mkxonline.smapp.ui.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.StarLineChartData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.ui.adapter.StarLineChartAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StarLineChartActivity : AppCompatActivity() {

    private lateinit var rvChart : RecyclerView

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView
    private lateinit var starlineChartAdapter: StarLineChartAdapter
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chart)

        helper = SessionManager(this@StarLineChartActivity)
        progressView = ProgressView(this@StarLineChartActivity)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title =  ""

        rvChart = findViewById(R.id.rv_chart)
        rvChart.layoutManager = LinearLayoutManager(this)
        rvChart.itemAnimator = DefaultItemAnimator()

        getChart()
    }

    private fun getChart() {
        progressView.view()
        val call: Call<StarLineChartData?>? = com.mkxonline.smapp.api.ApiMain.getClient.starlineResultChart("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<StarLineChartData?> {
            override fun onResponse(call: Call<StarLineChartData?>, response: Response<StarLineChartData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.code() == 401){
                    Utility.unAuthPopUp(this@StarLineChartActivity)
                }else if(response.body()!!.error == false){
                    starlineChartAdapter = StarLineChartAdapter(this@StarLineChartActivity,response.body()!!.gameList!!,response.body()!!.data!!.result!!)
                    rvChart.adapter = starlineChartAdapter
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<StarLineChartData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}