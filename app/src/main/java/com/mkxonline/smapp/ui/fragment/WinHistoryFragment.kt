package com.mkxonline.smapp.ui.fragment

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.github.hariprasanths.bounceview.BounceView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.api.model.WinHistoryData
import com.mkxonline.smapp.ui.adapter.WinHistoryAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class WinHistoryFragment : Fragment() {

    private lateinit var etStartDate: TextInputEditText
    private lateinit var etEndDate: TextInputEditText
    private lateinit var btnSearch: AppCompatButton

    private val dataDetails: ArrayList<WinHistoryData.Data.Result.DataItem> = ArrayList()
    private lateinit var winHistoryAdapter: WinHistoryAdapter
    lateinit var progressView: ProgressView
    private lateinit var rvWinHistory : RecyclerView
    lateinit var helper: SessionManager

    private lateinit var swipeContainer: SwipeRefreshLayout
    private var startDate = ""
    private var endDate = ""

    private var currentPage = 1
    private var lastPage = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_win_history, container, false)

        etStartDate = view.findViewById(R.id.et_start_date)
        etEndDate = view.findViewById(R.id.et_end_date)
        btnSearch = view.findViewById(R.id.btn_search)

        progressView = ProgressView(requireActivity())
        helper = SessionManager(requireActivity())

        swipeContainer = view.findViewById(R.id.swipeContainer)
        swipeContainer.setOnRefreshListener {
            onResume()
        }

        val df = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val formattedDate: String = df.format(Calendar.getInstance().time)
        etStartDate.setText(formattedDate)
        etEndDate.setText(formattedDate)

        val df1 = SimpleDateFormat("yyyy-MM-ddd", Locale.getDefault())
        startDate = df1.format(Calendar.getInstance().time)
        endDate = df1.format(Calendar.getInstance().time)

        etStartDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(requireActivity(), { view, year, monthOfYear, dayOfMonth ->
                etStartDate.setText(dayOfMonth.toString() + "-" + (monthOfYear+1) + "-" + year.toString())
                startDate = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth.toString()
            }, Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
            datePickerDialog.show()
        }

        etEndDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(requireActivity(), { view, year, monthOfYear, dayOfMonth ->
                etEndDate.setText(dayOfMonth.toString() + "-" + (monthOfYear+1) + "-" + year.toString())
                endDate = year.toString() + "-" + (monthOfYear+1) + "-" + dayOfMonth.toString()
            }, Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
            datePickerDialog.show()
        }

        rvWinHistory = view.findViewById(R.id.rv_win_history)
        rvWinHistory.layoutManager = LinearLayoutManager(activity)
        rvWinHistory.itemAnimator = DefaultItemAnimator()

        winHistoryAdapter = WinHistoryAdapter(dataDetails)
        rvWinHistory.adapter = winHistoryAdapter

        rvWinHistory.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    if (currentPage == lastPage) {
                        return
                    }
                    getWinHistory(++currentPage)
                }
            }
        })

        BounceView.addAnimTo(btnSearch)
        btnSearch.setOnClickListener {
            onResume()
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    override fun onResume() {
        super.onResume()
        getWinHistory(currentPage)
    }

    private fun getWinHistory(page: Int){
        if (page == 1) {
            dataDetails.clear()
            winHistoryAdapter.notifyDataSetChanged()
        }

        progressView.view()
        val call: Call<WinHistoryData?>? = com.mkxonline.smapp.api.ApiMain.getClient.winHistory("Bearer "+helper.getAuth().toString(),page.toString(),startDate,endDate)
        call!!.enqueue(object : Callback<WinHistoryData?> {
            override fun onResponse(call: Call<WinHistoryData?>, response: Response<WinHistoryData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false) {
                    if (isAttachedToActivity()) {
                        currentPage = response.body()!!.data!!.result!!.currentPage!!
                        lastPage = response.body()!!.data!!.result!!.lastPage!!
                        dataDetails.addAll(response.body()!!.data!!.result!!.data!!)
                        winHistoryAdapter.notifyDataSetChanged()
                    }
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<WinHistoryData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    fun isAttachedToActivity(): Boolean {
        return isVisible && activity != null
    }
}