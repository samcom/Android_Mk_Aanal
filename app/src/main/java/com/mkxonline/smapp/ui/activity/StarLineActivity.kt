package com.mkxonline.smapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.api.model.ListData
import com.mkxonline.smapp.api.model.RateData
import com.mkxonline.smapp.apputil.Utility
import com.github.hariprasanths.bounceview.BounceView
import com.mkxonline.smapp.ui.adapter.StarLineGameAdapter
import com.mkxonline.smapp.ui.adapter.StarLineRateAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StarLineActivity : AppCompatActivity()  {

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    lateinit var rvList : RecyclerView
    lateinit var rvRate : RecyclerView

    private lateinit var starLineGameAdapter: StarLineGameAdapter
    private lateinit var starLineRateAdapter: StarLineRateAdapter
    private val dataDetails: ArrayList<ListData.Data.ResultItem> = ArrayList()
    private val rateDetails: ArrayList<Array<String>> = ArrayList()

    private lateinit var swipeContainer: SwipeRefreshLayout
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_starline)

        progressView = ProgressView(this@StarLineActivity)
        helper = SessionManager(this@StarLineActivity)

        swipeContainer = findViewById(R.id.swipeContainer)
        toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(toolbar)
        supportActionBar!!.title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        rvList = findViewById(R.id.rv_list)
        rvList.layoutManager = LinearLayoutManager(this)
        rvList.itemAnimator = DefaultItemAnimator()

        rvRate = findViewById(R.id.rv_rate)
        rvRate.layoutManager = LinearLayoutManager(this)
        rvRate.itemAnimator = DefaultItemAnimator()

        BounceView.addAnimTo(findViewById<AppCompatButton>(R.id.btn_bid_history))
        findViewById<AppCompatButton>(R.id.btn_bid_history).setOnClickListener {
            if(helper.isBetting().equals("yes")) {
                startActivity(Intent(this@StarLineActivity, BidHistoryActivity::class.java))
            }
        }

//        BounceView.addAnimTo(findViewById<AppCompatButton>(R.id.btn_chart))
//        findViewById<AppCompatButton>(R.id.btn_chart).setOnClickListener {
//            if(helper.isBetting().equals("yes")) {
//                startActivity(Intent(this@StarLineActivity, StarLineChartActivity::class.java))
//            }
//        }

        BounceView.addAnimTo(findViewById<AppCompatButton>(R.id.btn_win_history))
        findViewById<AppCompatButton>(R.id.btn_win_history).setOnClickListener {
            if(helper.isBetting().equals("yes")) {
                startActivity(Intent(this@StarLineActivity, WinHistoryActivity::class.java))
            }
        }

        swipeContainer.setOnRefreshListener {
            onResume()
        }

        starLineGameAdapter = StarLineGameAdapter(this@StarLineActivity,dataDetails,object : StarLineGameAdapter.Callbacks {
            override fun onClick(s: ListData.Data.ResultItem) {
                if(helper.isBetting().equals("yes")){
                    startActivity(Intent(this@StarLineActivity, StarLineGameActivity::class.java).putExtra("GameDetails",s))
                }
            }
        })

        starLineRateAdapter = StarLineRateAdapter(rateDetails)

    }

    override fun onResume() {
        super.onResume()
        getList()
        getRate()
    }

    private fun getList(){
        progressView.view()
        dataDetails.clear()

        val call: Call<ListData?>? = com.mkxonline.smapp.api.ApiMain.getClient.starlineGameList("Bearer "+helper.getAuth().toString())
        call!!.enqueue(object : Callback<ListData?> {
            override fun onResponse(call: Call<ListData?>, response: Response<ListData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.code() == 401){
                    Utility.unAuthPopUp(this@StarLineActivity)
                }else if(response.body()!!.error == false){
                    dataDetails.addAll(response.body()!!.data!!.result!!)
                    rvList.adapter = starLineGameAdapter
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<ListData?>, t: Throwable) {
                progressView.hide()
                swipeContainer.isRefreshing = false
            }
        })
    }

    private fun getRate(){
        progressView.view()

        rateDetails.clear()
        val call: Call<RateData?>? = com.mkxonline.smapp.api.ApiMain.getClient.starlineGameRate("Bearer "+helper.getAuth().toString())
        call!!.enqueue(object : Callback<RateData?> {
            override fun onResponse(call: Call<RateData?>, response: Response<RateData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@StarLineActivity)
                }else if(response.body()!!.error == false){
                    rateDetails.addAll(response.body()!!.data!!.result!!)
                    rvRate.adapter = starLineRateAdapter
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<RateData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}