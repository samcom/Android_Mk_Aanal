package com.mkxonline.smapp.ui.activity

import android.Manifest
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.SettingsData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.apputil.fcm.NotifyingService
import com.mkxonline.smapp.apputil.interfaces.ToolbarTitleInterface
import com.mkxonline.smapp.apputil.interfaces.UserNameChangeInterface
import com.mkxonline.smapp.ui.fragment.BidHistoryFragment
import com.mkxonline.smapp.ui.fragment.ChangePasswordFragment
import com.mkxonline.smapp.ui.fragment.ContactUsFragment
import com.mkxonline.smapp.ui.fragment.DashboardFragment
import com.mkxonline.smapp.ui.fragment.HowToPlayFragment
import com.mkxonline.smapp.ui.fragment.RateFragment
import com.mkxonline.smapp.ui.fragment.UserFragment
import com.mkxonline.smapp.ui.fragment.WinHistoryFragment
import com.mkxonline.smapp.ui.fragment.WalletFragment
import com.google.android.material.navigation.NavigationView
import com.google.firebase.messaging.FirebaseMessaging
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import com.mkxonline.smapp.ui.fragment.DeleteAccountFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class MainActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener, ToolbarTitleInterface, UserNameChangeInterface {

    private lateinit var navigationView: NavigationView
    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView
    private lateinit var toolbar: Toolbar

//    private lateinit var txtNumber: TextView
    private lateinit var txtName: TextView
    private lateinit var toolbarTitle: TextView

    var shareMessage = ""

    private var receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                val fragmentManager = supportFragmentManager
                fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment()).commit()
            }
        }
    }

    private lateinit var localBroadcastManager : LocalBroadcastManager

    override fun changeTitle(s: String) {
        toolbar.title = ""
        toolbarTitle.text = s
        if(s.isEmpty()) {
            navigationView.menu.getItem(0).isChecked = true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        helper = SessionManager(this@MainActivity)
        progressView = ProgressView(this@MainActivity)


        toolbar = findViewById(R.id.toolbar)

        toolbarTitle = findViewById(R.id.toolbar_title)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = ""
        val drawer: DrawerLayout = findViewById(R.id.drawer_layout)
        navigationView = findViewById(R.id.nav_view)
        txtName = navigationView.getHeaderView(0).findViewById(R.id.txt_name)

        val toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)
        navigationView.itemIconTintList = null

        localBroadcastManager = LocalBroadcastManager.getInstance(this)
        localBroadcastManager.registerReceiver(receiver, IntentFilter("REFRESH_BALANCE"))
        NotifyingService.subscribeTopic()

        if(!helper.isAppLaunched()){
            bidDialog(helper.getLoginMsg()!!)
            helper.setAppLaunched(true)
        }

        val fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction()
            .replace(R.id.content_frame, DashboardFragment()).commit()



        val permissionListener: PermissionListener = object : PermissionListener {
            override fun onPermissionGranted() {
            }

            override fun onPermissionDenied(deniedPermissions: List<String?>) {
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            TedPermission.create()
                .setPermissionListener(permissionListener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.POST_NOTIFICATIONS)
                .check()
        }
    }

    private fun bidDialog(message: String) {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Share this App!")
        builder.setCancelable(false)
        builder.setMessage(message)

        builder.setNegativeButton("Done") { dialog, which ->
            dialog.dismiss()
        }
        if (!builder.create().isShowing) builder.create().show()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.nav_home) {
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content_frame, DashboardFragment()).commit()
        }

        if (id == R.id.nav_profile) {
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content_frame, UserFragment()).commit()

            toolbarTitle.text = getString(R.string.profile)
        }

        if (id == R.id.nav_wallet) {
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content_frame, WalletFragment()).commit()

            toolbarTitle.text = getString(R.string.wallet)
        }

        if (id == R.id.nav_win_history) {
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content_frame, WinHistoryFragment()).commit()

            toolbarTitle.text = getString(R.string.win_history)
        }

        if (id == R.id.nav_bid_history) {
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content_frame, BidHistoryFragment()).commit()

            toolbarTitle.text = getString(R.string.bid_history)
        }

        if (id == R.id.nav_rate) {
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content_frame, RateFragment()).commit()

            toolbarTitle.text = getString(R.string.rate_list)
        }

        if (id == R.id.nav_how_to_play) {
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content_frame, HowToPlayFragment()).commit()

            toolbarTitle.text = getString(R.string.how_to_play)
        }

        if(id == R.id.nav_share){
            getSettings()
        }

        if (id == R.id.nav_rate_app) {
            val uri: Uri = Uri.parse("market://details?id=${this@MainActivity.packageName}")
            val goToMarket = Intent(Intent.ACTION_VIEW, uri)
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
            try {
                startActivity(goToMarket)
            } catch (e: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=${this@MainActivity.packageName}")))
            }
        }

        if (id == R.id.nav_contact_us) {
            val fragmentManager =  supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content_frame, ContactUsFragment()).commit()

            toolbarTitle.text = getString(R.string.contact_us)
        }

        if (id == R.id.nav_change_password) {
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content_frame, ChangePasswordFragment()).commit()

            toolbarTitle.text = getString(R.string.change_password)
        }

        if (id == R.id.nav_delete_account) {
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.content_frame, DeleteAccountFragment()).commit()

            toolbarTitle.text = getString(R.string.delete_account)
        }

        if(id == R.id.nav_logout){
            logoutDialog()
        }

        val drawer: DrawerLayout = findViewById(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        val drawer: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            val f = supportFragmentManager.findFragmentById(R.id.content_frame)
            if (f is DashboardFragment) {
                exitDialog()
            }else{
                supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment()).commit()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        localBroadcastManager.unregisterReceiver(receiver)
        NotifyingService.unsubscribeTopic()
    }

    private fun getSettings() {
        progressView.view()
        val call: Call<SettingsData?>? = com.mkxonline.smapp.api.ApiMain.getClient.settings("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<SettingsData?> {
            override fun onResponse(call: Call<SettingsData?>, response: Response<SettingsData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@MainActivity)
                }else if(response.body()!!.error == false){
                    if(!response.body()!!.data!!.result!!.shareMessage.isNullOrBlank()){
                        shareMessage = response.body()!!.data!!.result!!.shareMessage!!
                        try {
                            val shareIntent = Intent(Intent.ACTION_SEND)
                            shareIntent.type = "text/plain"
                            shareIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name)
                            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                            startActivity(Intent.createChooser(shareIntent, "choose one"))
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<SettingsData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    private fun exitDialog() {
        val mBottomSheetDialog = Dialog(this@MainActivity, R.style.MaterialDialogSheet)
        mBottomSheetDialog.setContentView(R.layout.dialog_exit)
        mBottomSheetDialog.setCancelable(false)
        mBottomSheetDialog.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        mBottomSheetDialog.setCanceledOnTouchOutside(false)

        val btnYes = mBottomSheetDialog.findViewById<AppCompatButton>(R.id.btn_yes)
        val btnNo = mBottomSheetDialog.findViewById<AppCompatButton>(R.id.btn_no)

        btnYes.setOnClickListener {
            mBottomSheetDialog.dismiss()
            finish()
        }

        btnNo.setOnClickListener {
            mBottomSheetDialog.dismiss()
        }

        mBottomSheetDialog.show()
    }

    private fun logoutDialog() {
        val mBottomSheetDialog = Dialog(this@MainActivity, R.style.MaterialDialogSheet)
        mBottomSheetDialog.setContentView(R.layout.dialog_logout)
        mBottomSheetDialog.setCancelable(false)
        mBottomSheetDialog.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        mBottomSheetDialog.setCanceledOnTouchOutside(false)

        val btnSubmit = mBottomSheetDialog.findViewById<AppCompatButton>(R.id.btn_submit)
        val imgClose = mBottomSheetDialog.findViewById<ImageView>(R.id.img_close)

        btnSubmit.setOnClickListener {
            mBottomSheetDialog.dismiss()
            try {
                FirebaseMessaging.getInstance().deleteToken()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            SessionManager(this@MainActivity).clearAllPrefs()
            startActivity(Intent(this@MainActivity, SignUpActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
            finish()
        }

        imgClose.setOnClickListener {
            mBottomSheetDialog.dismiss()
        }

        mBottomSheetDialog.show()
    }

    override fun changeUserName(s: String) {
        txtName.text = s

        if(!helper.isBetting().equals("yes")){
            navigationView.menu.findItem(R.id.nav_win_history).isVisible = false
            navigationView.menu.findItem(R.id.nav_bid_history).isVisible = false
            navigationView.menu.findItem(R.id.nav_wallet).isVisible = false
            navigationView.menu.findItem(R.id.nav_rate).isVisible = false
        }else{
            navigationView.menu.findItem(R.id.nav_win_history).isVisible = true
            navigationView.menu.findItem(R.id.nav_bid_history).isVisible = true
            navigationView.menu.findItem(R.id.nav_wallet).isVisible = true
            navigationView.menu.findItem(R.id.nav_rate).isVisible = true
        }
    }
}