package com.mkxonline.smapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.SessionManager

class SplashActivity : AppCompatActivity() {

    lateinit var helper: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        helper = SessionManager(this@SplashActivity)
    }

    override fun onResume() {
        super.onResume()

        Handler(Looper.getMainLooper()).postDelayed({
            if (!helper.isLogin()) {
                startActivity(Intent(this@SplashActivity, SignUpActivity::class.java))
            } else {
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            }
            finish()
        }, 2000)
    }
}