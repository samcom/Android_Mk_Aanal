package com.mkxonline.smapp.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.ApiMain
import com.mkxonline.smapp.api.model.MessageData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.customview.ProgressView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity : AppCompatActivity() {

    private lateinit var etPhone: TextInputEditText
    lateinit var progressView: ProgressView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        etPhone = findViewById(R.id.et_phone)
        progressView = ProgressView(this@ForgotPasswordActivity)

        BounceView.addAnimTo(findViewById<AppCompatButton>(R.id.btn_proceed))
        findViewById<AppCompatButton>(R.id.btn_proceed).setOnClickListener {
            if (Utility.isValidPhoneNo(etPhone.text.toString())) {
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.error_phone))
                return@setOnClickListener
            }

            sendOTP()
        }
    }

    private fun sendOTP() {
        progressView.view()
        val call: Call<MessageData?>? = ApiMain.getClient.sendotp(etPhone.text.toString(),"forgot")
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id .content),getString(R.string.internal_server_msg))
                    return
                } else if(response.body()!!.error == false){
                    val intent = Intent(this@ForgotPasswordActivity, OTPVerifyActivity::class.java)
                    intent.putExtra("number",etPhone.text.toString())
                    intent.putExtra("type","forgot")
                    startActivityForResult(intent, 2)
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 2 && resultCode == RESULT_OK) {
            startActivity(Intent(this@ForgotPasswordActivity, ResetPasswordActivity::class.java).putExtra("number",etPhone.text.toString()))
            finish()
        }
    }
}
