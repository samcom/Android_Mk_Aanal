package com.mkxonline.smapp.ui.adapter

import android.content.Context
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.ListData
import com.mkxonline.smapp.apputil.Utility.Companion.mFormatDate
import com.mkxonline.smapp.apputil.Utility.Companion.mFormatDate3
import java.text.ParseException
import java.util.*

class StarLineGameAdapter(var context: Context, private var arrayList: ArrayList<ListData.Data.ResultItem>, private val customClick: Callbacks) : RecyclerView.Adapter<StarLineGameAdapter.ViewHolderContact>() {

    interface Callbacks {
        fun onClick(s: ListData.Data.ResultItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_starline_main, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {
        var isClose = false
        val s = arrayList[position]

        try {
            val openDate = mFormatDate.parse(s.openTime)
            val calendarOpen = Calendar.getInstance()

            calendarOpen.time = openDate
            calendarOpen.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
            calendarOpen.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
            calendarOpen.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

            viewholder.txtOpenClose.text = mFormatDate3.format(openDate)

            if(s.openToday.equals("1")) {
                if(Calendar.getInstance().time.before(calendarOpen.time)) {
                    isClose = false
                    viewholder.imgStatus.setImageResource(R.drawable.ic_market_open)
                    viewholder.txtMarketStatus.text = "Market Open"
                    viewholder.txtMarketStatus.setTextColor(context.getColor(R.color.green))
                } else {
                    isClose = true
                    viewholder.imgStatus.setImageResource(R.drawable.ic_market_closed)
                    viewholder.txtMarketStatus.text = "Market Closed"
                    viewholder.txtMarketStatus.setTextColor(context.getColor(R.color.red))
                }
            }else{
                isClose = true
                viewholder.imgStatus.setImageResource(R.drawable.ic_market_closed)
                viewholder.txtMarketStatus.text = "Market Closed"
                viewholder.txtMarketStatus.setTextColor(context.getColor(R.color.red))
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        viewholder.txtMarketInfo.text = s.result

        viewholder.itemView.setOnClickListener {
            if(!isClose){
                customClick.onClick(arrayList[position])
            }else{
                val vibe = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
                vibe!!.vibrate(100)
            }
        }
    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {
        var txtOpenClose: TextView = view.findViewById(R.id.txt_open_close)
        var txtMarketInfo: TextView = view.findViewById(R.id.txt_market_info)
        var txtMarketStatus: TextView = view.findViewById(R.id.txt_market_status)
        var imgStatus: ImageView = view.findViewById(R.id.img_status)
    }

}
