package com.mkxonline.smapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.WinHistoryData
import com.mkxonline.smapp.apputil.Utility

class WinHistoryAdapter(private var arrayList: ArrayList<WinHistoryData.Data.Result.DataItem>) : RecyclerView.Adapter<WinHistoryAdapter.ViewHolderContact>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_win_history, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {
        val s = arrayList[position]

        viewholder.txtTitle.text = s.gameName + " in "+s.type+" for bid amount - "+s.points+" won"

        viewholder.txtPoints.text = "+"+ Utility.decimalFormat(s.winingAmount!!)+" points"
        viewholder.txtTime.text = s.bidDate

    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {
        var txtTitle: TextView = view.findViewById(R.id.txt_title)
        var txtPoints: TextView = view.findViewById(R.id.txt_points)
        var txtTime: TextView = view.findViewById(R.id.txt_time)
    }

}