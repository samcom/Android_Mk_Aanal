package com.mkxonline.smapp.ui.fragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.ApiMain
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.api.model.MessageData
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordFragment : Fragment() {

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    private lateinit var etPassword: TextInputEditText
    private lateinit var etNewPassword: TextInputEditText
    private lateinit var etNewConfirmPassword: TextInputEditText
    private lateinit var btnUpdate: AppCompatButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_change_password, container, false)

        helper = SessionManager(requireActivity())
        progressView = ProgressView(requireActivity())

        etPassword = view.findViewById(R.id.et_password)
        etNewPassword = view.findViewById(R.id.et_new_password)
        etNewConfirmPassword = view.findViewById(R.id.et_new_confirm_password)
        btnUpdate = view.findViewById(R.id.btn_update)
        BounceView.addAnimTo(btnUpdate)

        btnUpdate.setOnClickListener {

            if(Utility.isStringNull(etPassword.text.toString())){
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.error_password))
                return@setOnClickListener
            }

            if(Utility.isValidPassword(etPassword.text.toString())){
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.error_password_min))
                return@setOnClickListener
            }

            if(Utility.isStringNull(etNewPassword.text.toString())){
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.error_password))
                return@setOnClickListener
            }

            if(Utility.isValidPassword(etNewPassword.text.toString())){
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.error_password_min))
                return@setOnClickListener
            }

            if(Utility.isStringNull(etNewConfirmPassword.text.toString())){
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.error_password))
                return@setOnClickListener
            }

            if(etNewConfirmPassword.text.toString() != etNewPassword.text.toString()){
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.error_password_not_match))
                return@setOnClickListener
            }

            if (Utility.isInternetConnection(requireActivity())) {
                changePassword()
            }else{
                Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
            }

        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    private fun changePassword() {
        progressView.view()
        val call: Call<MessageData?>? = ApiMain.getClient.changePassword("Bearer "+helper.getAuth().toString()
            ,etPassword.text.toString(),etNewPassword.text.toString())
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    Utility.successSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }
}