package com.mkxonline.smapp.ui.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.github.hariprasanths.bounceview.BounceView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.Utility

class DeleteAccountFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_delete_account, container, false)

        BounceView.addAnimTo(view.findViewById<AppCompatButton>(R.id.btn_link))
        view.findViewById<AppCompatButton>(R.id.btn_link).setOnClickListener {
            if (Utility.isInternetConnection(requireActivity())){
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(Utility.MAIN_URL+"deleteRequest")
                startActivity(i)
            }
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }
}