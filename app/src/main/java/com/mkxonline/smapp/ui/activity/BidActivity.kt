package com.mkxonline.smapp.ui.activity

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.InputFilter
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.*
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.mkxonline.smapp.ui.adapter.BidAdapter
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class BidActivity : AppCompatActivity() {

    private var dataDetails: ArrayList<String> = ArrayList()
    private var openData: ArrayList<String> = ArrayList()
    private var closeData: ArrayList<String> = ArrayList()
    private var bidDataList: ArrayList<BidData> = ArrayList()
    private lateinit var txtPoints: TextView

    private lateinit var etDate: TextInputEditText
    private lateinit var etOpenDigit: AutoCompleteTextView
    private lateinit var etCloseDigit: AutoCompleteTextView
    private lateinit var etOpenPana: AutoCompleteTextView
    private lateinit var etClosePana: AutoCompleteTextView
    private lateinit var etPoints: TextInputEditText

    private lateinit var tlyOpenDigit: TextInputLayout
    private lateinit var tlyCloseDigit: TextInputLayout
    private lateinit var tlyOpenPana: TextInputLayout
    private lateinit var tlyClosePana: TextInputLayout

    private lateinit var toggle: RadioGroup
    private lateinit var llToggle: LinearLayout
    private var minPoints = 0
    private var maxPoints = 0

    private lateinit var toolbar: Toolbar
    private lateinit var toolbarTitle: TextView

    lateinit var bidAdapter: BidAdapter
    lateinit var progressView: ProgressView
    private lateinit var rvBid: RecyclerView
    lateinit var helper: SessionManager
    private lateinit var dataType: TypeData.Data.ResultItem
    private lateinit var listDetails: ListData.Data.ResultItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_point)

        dataType = intent.extras!!.getSerializable("GameType") as TypeData.Data.ResultItem
        listDetails = intent.extras!!.getSerializable("GameDetails") as ListData.Data.ResultItem

        progressView = ProgressView(this@BidActivity)
        helper = SessionManager(this@BidActivity)

        rvBid = findViewById(R.id.rv_bid)

        llToggle = findViewById(R.id.ll_toggle)
        toggle = findViewById(R.id.toggle)
        etDate = findViewById(R.id.et_date)
        etOpenDigit = findViewById(R.id.et_open_digit)
        etCloseDigit = findViewById(R.id.et_close_digit)
        etOpenPana = findViewById(R.id.et_open_pana)
        etClosePana = findViewById(R.id.et_close_pana)

        tlyOpenDigit = findViewById(R.id.tly_open_digit)
        tlyCloseDigit = findViewById(R.id.tly_close_digit)
        tlyOpenPana = findViewById(R.id.tly_open_pana)
        tlyClosePana = findViewById(R.id.tly_close_pana)
        etPoints = findViewById(R.id.et_points)

        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.toolbar_title)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbarTitle.text =  dataType.type

        rvBid.layoutManager = LinearLayoutManager(this@BidActivity)
        rvBid.itemAnimator = DefaultItemAnimator()

        bidAdapter = BidAdapter(0, bidDataList, dataType.type!!, object : BidAdapter.Callbacks {
            override fun onClick(s: BidData) {
                bidDataList.remove(s)
                bidAdapter.notifyDataSetChanged()

                if (bidDataList.size == 0){
                    findViewById<AppCompatButton>(R.id.btn_submit).visibility = View.GONE
                }
            }
        })

        rvBid.adapter = bidAdapter

        if(dataType.type.equals(Utility.JODI_DIGIT)){
            toggle.visibility = View.GONE
            llToggle.visibility = View.GONE
            tlyOpenDigit.visibility = View.VISIBLE
            tlyCloseDigit.visibility = View.GONE
            tlyOpenPana.visibility = View.GONE
            tlyClosePana.visibility = View.GONE
            etOpenDigit.hint = "Jodi Digit"
            etOpenDigit.filters = arrayOf(InputFilter.LengthFilter(2))
            etCloseDigit.filters = arrayOf(InputFilter.LengthFilter(2))
        }else if(dataType.type.equals(Utility.SINGLE_PANA) || dataType.type.equals(
                Utility.DOUBLE_PANA) || dataType.type.equals(Utility.TRIPLE_PANA)){
            tlyOpenDigit.visibility = View.GONE
            tlyCloseDigit.visibility = View.GONE
            tlyOpenPana.visibility = View.VISIBLE
            tlyClosePana.visibility = View.GONE
            etOpenPana.filters = arrayOf(InputFilter.LengthFilter(3))
            etClosePana.filters = arrayOf(InputFilter.LengthFilter(3))
        }else if(dataType.type.equals(Utility.HALF_SANGAM)){
            tlyOpenDigit.visibility = View.VISIBLE
            tlyCloseDigit.visibility = View.GONE
            tlyOpenPana.visibility = View.GONE
            tlyClosePana.visibility = View.VISIBLE
            etOpenDigit.filters = arrayOf(InputFilter.LengthFilter(1))
            etCloseDigit.filters = arrayOf(InputFilter.LengthFilter(1))
            etOpenPana.filters = arrayOf(InputFilter.LengthFilter(3))
            etClosePana.filters = arrayOf(InputFilter.LengthFilter(3))
        }else if(dataType.type.equals(Utility.FULL_SANGAM)){
            tlyOpenDigit.visibility = View.GONE
            tlyCloseDigit.visibility = View.GONE
            tlyOpenPana.visibility = View.VISIBLE
            tlyClosePana.visibility = View.VISIBLE
            toggle.visibility = View.GONE
            llToggle.visibility = View.GONE
            etOpenPana.filters = arrayOf(InputFilter.LengthFilter(3))
            etClosePana.filters = arrayOf(InputFilter.LengthFilter(3))
        }

        val df = SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault())
        val formattedDate: String = df.format(Calendar.getInstance().time)
        etDate.setText(formattedDate)

        try {
            val openDate = Utility.mFormatDate.parse(listDetails.openTime!!)
            val calendarOpen = Calendar.getInstance()

            calendarOpen.time = openDate!!
            calendarOpen.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
            calendarOpen.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
            calendarOpen.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

            if(Calendar.getInstance().time.after(calendarOpen.time)) {
                toggle.check(R.id.close)
                for (i in 0 until toggle.childCount) {
                    (toggle.getChildAt(i) as RadioButton).isEnabled = false
                    (toggle.getChildAt(i) as RadioButton).isClickable = false
                }

                if(dataType.type.equals(Utility.SINGLE_PANA) || dataType.type.equals(
                        Utility.DOUBLE_PANA) || dataType.type.equals(Utility.TRIPLE_PANA)){
                    tlyOpenDigit.visibility = View.GONE
                    tlyCloseDigit.visibility = View.GONE
                    tlyOpenPana.visibility = View.GONE
                    tlyClosePana.visibility = View.VISIBLE
                    etClosePana.filters = arrayOf(InputFilter.LengthFilter(3))
                }else{
                    tlyOpenDigit.visibility = View.GONE
                    tlyCloseDigit.visibility = View.VISIBLE
                    tlyOpenPana.visibility = View.GONE
                    tlyClosePana.visibility = View.GONE
                    etClosePana.filters = arrayOf(InputFilter.LengthFilter(1))
                }
            }

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        etOpenDigit.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) etOpenDigit.hint =
                "" else etOpenDigit.hint = getString(R.string.hint_open_digit)
        }

        etCloseDigit.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) etCloseDigit.hint = "" else etCloseDigit.hint = getString(R.string.hint_close_digit)
        }

        etOpenPana.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) etOpenPana.hint = "" else etOpenPana.hint = getString(R.string.hint_open_pana)
        }

        etClosePana.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) etClosePana.hint = "" else etClosePana.hint = getString(R.string.hint_close_pana)
        }

        BounceView.addAnimTo(findViewById<AppCompatButton>(R.id.btn_process))
        findViewById<AppCompatButton>(R.id.btn_process).setOnClickListener {
            if(!validPoints()){
                return@setOnClickListener
            }

            if (Utility.isInternetConnection(this@BidActivity)) {
                dashboard(1)
            }else{
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internet_msg))
            }
        }

        BounceView.addAnimTo(findViewById<AppCompatButton>(R.id.btn_submit))
        findViewById<AppCompatButton>(R.id.btn_submit).setOnClickListener {
            listDataDetails()
        }

        toggle.setOnCheckedChangeListener { radioGroup, i ->

            if(i == R.id.open){
                if(dataType.type.equals(Utility.SINGLE_DIGIT)){
                    tlyOpenDigit.visibility = View.VISIBLE
                    tlyCloseDigit.visibility = View.GONE
                    tlyOpenPana.visibility = View.GONE
                    tlyClosePana.visibility = View.GONE
                }else if(dataType.type.equals(Utility.SINGLE_PANA) || dataType.type.equals(
                        Utility.DOUBLE_PANA) || dataType.type.equals(Utility.TRIPLE_PANA) ){
                    tlyOpenDigit.visibility = View.GONE
                    tlyCloseDigit.visibility = View.GONE
                    tlyOpenPana.visibility = View.VISIBLE
                    tlyClosePana.visibility = View.GONE
                }else if(dataType.type.equals(Utility.HALF_SANGAM)){
                    tlyOpenDigit.visibility = View.VISIBLE
                    tlyCloseDigit.visibility = View.GONE
                    tlyOpenPana.visibility = View.GONE
                    tlyClosePana.visibility = View.VISIBLE
                }
            }else{
                if(dataType.type.equals(Utility.SINGLE_DIGIT)){
                    tlyOpenDigit.visibility = View.GONE
                    tlyCloseDigit.visibility = View.VISIBLE
                    tlyOpenPana.visibility = View.GONE
                    tlyClosePana.visibility = View.GONE
                }else if(dataType.type.equals(Utility.SINGLE_PANA) || dataType.type.equals(
                        Utility.DOUBLE_PANA) || dataType.type.equals(Utility.TRIPLE_PANA) ){
                    tlyOpenDigit.visibility = View.GONE
                    tlyCloseDigit.visibility = View.GONE
                    tlyOpenPana.visibility = View.GONE
                    tlyClosePana.visibility = View.VISIBLE
                }else if(dataType.type.equals(Utility.HALF_SANGAM)){
                    tlyOpenDigit.visibility = View.GONE
                    tlyCloseDigit.visibility = View.VISIBLE
                    tlyOpenPana.visibility = View.VISIBLE
                    tlyClosePana.visibility = View.GONE
                }
            }
        }

        if (Utility.isInternetConnection(this@BidActivity)) {
            getDigit()
            dashboard(0)
        }else{
            Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internet_msg))
        }
    }

    private fun validPoints(): Boolean {
        return if(etPoints.text.toString().isEmpty()){
            Utility.errorSnackBar(findViewById(android.R.id.content),"Please Enter Valid Points")
            false
        }else{
            val points: Int = etPoints.text.toString().trim { it <= ' ' }.toInt()
            if (points in (minPoints) until maxPoints) {
                true
            } else {
                Utility.errorSnackBar(findViewById(android.R.id.content),"Please Enter Valid Points")
                false
            }
        }
    }

    private fun getDigit(){
        progressView.view()

        lateinit var call: Call<DigitPanaData?>

        if(dataType.type == Utility.HALF_SANGAM || dataType.type == Utility.FULL_SANGAM){
            getDigitPana()
            return
        }else if(dataType.type == Utility.JODI_DIGIT){
            call = com.mkxonline.smapp.api.ApiMain.getClient.jodiDigit("Bearer "+helper.getAuth().toString())!!
        }else if (dataType.type == Utility.SINGLE_PANA){
            call = com.mkxonline.smapp.api.ApiMain.getClient.singlePana("Bearer "+helper.getAuth().toString())!!
        }else if (dataType.type == Utility.DOUBLE_PANA){
            call = com.mkxonline.smapp.api.ApiMain.getClient.doublePana("Bearer "+helper.getAuth().toString())!!
        }else if (dataType.type == Utility.TRIPLE_PANA){
            call = com.mkxonline.smapp.api.ApiMain.getClient.triplePana("Bearer "+helper.getAuth().toString())!!
        }else{
            call = com.mkxonline.smapp.api.ApiMain.getClient.singleDigit("Bearer "+helper.getAuth().toString())!!
        }

        call.enqueue(object : Callback<DigitPanaData?> {
            override fun onResponse(call: Call<DigitPanaData?>, response: Response<DigitPanaData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@BidActivity)
                }else if(response.body()!!.error == false){
                    val datas: ArrayList<DigitPanaData.Data.ResultItem> = ArrayList()
                    datas.addAll(response.body()!!.data!!.result!!)

                    minPoints = response.body()!!.data!!.minPoint!!.toInt()
                    maxPoints = response.body()!!.data!!.maxPoint!!.toInt()

                    for (item in datas){
                        dataDetails.add(item.number!!)
                    }

                    val openAdapter = ArrayAdapter(this@BidActivity, android.R.layout.simple_list_item_1, dataDetails)
                    val closeAdapter = ArrayAdapter(this@BidActivity, android.R.layout.simple_list_item_1, dataDetails)
                    etOpenDigit.setAdapter(openAdapter)
                    etOpenDigit.threshold = 1
                    etCloseDigit.setAdapter(closeAdapter)
                    etCloseDigit.threshold = 1

                    etOpenPana.setAdapter(openAdapter)
                    etOpenPana.threshold = 1
                    etClosePana.setAdapter(closeAdapter)
                    etClosePana.threshold = 1

                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<DigitPanaData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    private fun getDigitPana(){
        progressView.view()

        val call: Call<SangamData?> = when (dataType.type) {
            Utility.HALF_SANGAM -> {
                com.mkxonline.smapp.api.ApiMain.getClient.halfSangam("Bearer "+helper.getAuth().toString())!!
            }
            else -> {
                com.mkxonline.smapp.api.ApiMain.getClient.fullSangam("Bearer "+helper.getAuth().toString())!!
            }
        }

        call.enqueue(object : Callback<SangamData?> {
            override fun onResponse(call: Call<SangamData?>, response: Response<SangamData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@BidActivity)
                }else if(response.body()!!.error == false){
                    val datas: ArrayList<SangamData.Data.ResultItem> = ArrayList()
                    datas.addAll(response.body()!!.data!!.result!!)

                    minPoints = response.body()!!.data!!.minPoint!!.toInt()
                    maxPoints = response.body()!!.data!!.maxPoint!!.toInt()

                    for (item in datas){
                        if(item.type.equals("open")){
                            openData.add(item.number!!)
                        }else{
                            closeData.add(item.number!!)
                        }
                    }

                    val openAdapter = ArrayAdapter(this@BidActivity, android.R.layout.simple_list_item_1, openData)
                    val closeAdapter = ArrayAdapter(this@BidActivity, android.R.layout.simple_list_item_1, closeData)
                    if(dataType.type == Utility.HALF_SANGAM){
                        etOpenDigit.setAdapter(openAdapter)
                        etOpenDigit.threshold = 1
                        etCloseDigit.setAdapter(openAdapter)
                        etCloseDigit.threshold = 1

                        etOpenPana.setAdapter(closeAdapter)
                        etOpenPana.threshold = 1
                        etClosePana.setAdapter(closeAdapter)
                        etClosePana.threshold = 1
                    }else{
                        etOpenDigit.setAdapter(openAdapter)
                        etOpenDigit.threshold = 1
                        etCloseDigit.setAdapter(closeAdapter)
                        etCloseDigit.threshold = 1

                        etOpenPana.setAdapter(openAdapter)
                        etOpenPana.threshold = 1
                        etClosePana.setAdapter(closeAdapter)
                        etClosePana.threshold = 1
                    }
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<SangamData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    private fun dashboard(i: Int) {
        progressView.view()
        val call: Call<DashboardData?>? = com.mkxonline.smapp.api.ApiMain.getClient.dashboard("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<DashboardData?> {
            override fun onResponse(call: Call<DashboardData?>, response: Response<DashboardData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@BidActivity)
                }else if(response.body()!!.error == false){
                    helper.setBalance(response.body()!!.data!!.result!!.availableBalance)
                    Handler(Looper.getMainLooper()).postDelayed({
                        txtPoints.text = Utility.decimalFormat(helper.getBalance()!!)
                    }, 500)

                    if(i==1){
                        if(etPoints.text.toString().toInt() > helper.getBalance()!!.toInt()){
                            Utility.errorSnackBar(findViewById(android.R.id.content),"Not that much balance")
                            return
                        }

                        if(bidDataList.size > 0 ){
                            var balance = 0
                            for (j in 0 until bidDataList.size) {
                                balance = balance.plus(bidDataList[j].points.toInt())
                            }
                            val cBalance = helper.getBalance()!!.toInt() - balance

                            if(etPoints.text.toString().toInt() > cBalance){
                                Utility.errorSnackBar(findViewById(android.R.id.content),"Not that much balance")
                                return
                            }
                        }

                        addBid()
                    }

                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<DashboardData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    private fun addBid(){
        val b = BidData()

        var session = if(toggle.checkedRadioButtonId == R.id.open){ "open" }else{ "close" }
        var openDigit = ""
        var closeDigit = ""
        var openPana = ""
        var closePana = ""

        if(dataType.type == Utility.SINGLE_DIGIT){
            if(toggle.checkedRadioButtonId == R.id.open){
                openDigit = etOpenDigit.text.toString()
                if(!validEdittext(etOpenDigit,dataDetails)){
                    return
                }
            }else{
                closeDigit = etCloseDigit.text.toString()
                if(!validEdittext(etCloseDigit,dataDetails)){
                    return
                }
            }
        }else if (dataType.type == Utility.JODI_DIGIT){
            openDigit = etOpenDigit.text.toString()
            session = "close"
            if(!validEdittext(etOpenDigit,dataDetails)){
                return
            }
        }else if(dataType.type == Utility.SINGLE_PANA || dataType.type == Utility.DOUBLE_PANA || dataType.type == Utility.TRIPLE_PANA){
            if(toggle.checkedRadioButtonId == R.id.open){
                openPana = etOpenPana.text.toString()
                if(!validEdittext(etOpenPana,dataDetails)){
                    return
                }
            }else{
                closePana = etClosePana.text.toString()
                if(!validEdittext(etClosePana,dataDetails)){
                    return
                }
            }
        }else if(dataType.type == Utility.HALF_SANGAM){
            if(toggle.checkedRadioButtonId == R.id.open){
                openDigit = etOpenDigit.text.toString()
                closePana = etClosePana.text.toString()

                if(!validEdittext(etOpenDigit,openData)){
                    return
                }

                if(!validEdittext(etClosePana,closeData)){
                    return
                }

            }else{
                openPana = etOpenPana.text.toString()
                closeDigit = etCloseDigit.text.toString()

                if(!validEdittext(etOpenPana,closeData)){
                    return
                }

                if(!validEdittext(etCloseDigit,openData)){
                    return
                }
            }
        }else if(dataType.type == Utility.FULL_SANGAM){
            openPana = etOpenPana.text.toString()
            closePana = etClosePana.text.toString()
            session = "close"

            if(!validEdittext(etOpenPana,openData)){
                return
            }

            if(!validEdittext(etClosePana,closeData)){
                return
            }
        }

        b.session = session
        b.open_pana = openPana
        b.close_pana = closePana
        b.open_digit = openDigit
        b.close_digit = closeDigit
        b.points = etPoints.text.toString()

        for (i in 0 until toggle.childCount) {
            (toggle.getChildAt(i) as RadioButton).isEnabled = false
            (toggle.getChildAt(i) as RadioButton).isClickable = false
        }

        bidDataList.add(b)
        findViewById<AppCompatButton>(R.id.btn_submit).visibility = View.VISIBLE

        bidAdapter.notifyDataSetChanged()

        etOpenDigit.setText("")
        etOpenPana.setText("")
        etCloseDigit.setText("")
        etClosePana.setText("")
        etPoints.setText("")
    }

    private fun validEdittext(et : AutoCompleteTextView,dataDetails: ArrayList<String>) : Boolean {
        val openDigit = et.text.toString()
        return if (openDigit.isEmpty()){
            Utility.errorSnackBar(findViewById(android.R.id.content),"Please Enter Digit")
            false
        }else if(!dataDetails.contains(openDigit)){
            Utility.errorSnackBar(findViewById(android.R.id.content),"Digit not Allowed")
            false
        }else{
            true
        }
    }

    private fun listDataDetails(){
        progressView.view()
        val call: Call<ListDetailData?>? = com.mkxonline.smapp.api.ApiMain.getClient.gameDetails("Bearer "+helper.getAuth().toString(),listDetails.id!!)
        call!!.enqueue(object : Callback<ListDetailData?> {
            override fun onResponse(call: Call<ListDetailData?>, response: Response<ListDetailData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.code() == 401){
                    Utility.unAuthPopUp(this@BidActivity)
                }else if(response.body()!!.error == false){
                    listDetails.openTime = response.body()!!.data!!.result!!.openTime!!
                    listDetails.closeTime = response.body()!!.data!!.result!!.closeTime!!
                    if(toggle.checkedRadioButtonId == R.id.open){
                        try {
                            val endDate = Utility.mFormatDate.parse(response.body()!!.data!!.result!!.openTime!!)
                            val calendarEnd = Calendar.getInstance()

                            calendarEnd.time = endDate!!
                            calendarEnd.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
                            calendarEnd.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                            calendarEnd.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

                            Log.e("Condition", Calendar.getInstance().time.before(calendarEnd.time).toString())
                            if(Calendar.getInstance().time.before(calendarEnd.time)) {
                                confirmBid()
                            } else {
                                Utility.errorSnackBar(findViewById(android.R.id.content),"Open Session Closed for Bid")
                                finish()
                                startActivity(Intent(this@BidActivity, MainActivity::class.java))
                            }

                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                    }else{
                        try {
                            val endDate = Utility.mFormatDate.parse(response.body()!!.data!!.result!!.closeTime!!)
                            val calendarEnd = Calendar.getInstance()

                            calendarEnd.time = endDate!!
                            calendarEnd.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
                            calendarEnd.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                            calendarEnd.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

                            Log.e("Condition", Calendar.getInstance().time.before(calendarEnd.time).toString())
                            if(Calendar.getInstance().time.before(calendarEnd.time)) {
                                confirmBid()
                            } else {
                                Utility.errorSnackBar(findViewById(android.R.id.content),"Market Close")
                                finish()
                                startActivity(Intent(this@BidActivity, MainActivity::class.java))
                            }

                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                    }
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<ListDetailData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    private fun confirmBid() {
        val height = (resources.displayMetrics.heightPixels * 0.80).toInt()

        val mBottomSheetDialog = Dialog(this@BidActivity, R.style.MaterialDialogSheet)
        mBottomSheetDialog.setContentView(R.layout.dialog_confirm)
        mBottomSheetDialog.setCancelable(false)
        mBottomSheetDialog.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,height)
        mBottomSheetDialog.setCanceledOnTouchOutside(false)
        mBottomSheetDialog.window!!.setGravity(Gravity.BOTTOM)

        val txtGame = mBottomSheetDialog.findViewById<TextView>(R.id.txt_game)
        val txtBids = mBottomSheetDialog.findViewById<TextView>(R.id.txtBids)
        val txtTotalBid = mBottomSheetDialog.findViewById<TextView>(R.id.txt_total_bid)
        val txtWalletBefore = mBottomSheetDialog.findViewById<TextView>(R.id.txt_wallet_before)
        val txtWalletAfter = mBottomSheetDialog.findViewById<TextView>(R.id.txt_wallet_after)
        val rvBid = mBottomSheetDialog.findViewById<RecyclerView>(R.id.rv_bid)
        val btnCancel = mBottomSheetDialog.findViewById<AppCompatButton>(R.id.btn_cancel)
        val btnSubmit = mBottomSheetDialog.findViewById<AppCompatButton>(R.id.btn_submit)

        rvBid.layoutManager = LinearLayoutManager(this@BidActivity)
        rvBid.itemAnimator = DefaultItemAnimator()

        txtGame.text = listDetails.gameName
        txtBids.text = bidDataList.size.toString()

        var balance = 0
        for (i in 0 until bidDataList.size) {
            balance = balance.plus(bidDataList[i].points.toInt())
        }

        txtTotalBid.text = balance.toString()
        txtWalletBefore.text = helper.getBalance()!!
        txtWalletAfter.text = (helper.getBalance()!!.toInt() - balance).toString()

        val bidAdapter = BidAdapter(1,bidDataList ,dataType.type!!,object : BidAdapter.Callbacks {
            override fun onClick(s: BidData) {
            }
        })

        rvBid.adapter = bidAdapter

        btnSubmit.setOnClickListener {
            mBottomSheetDialog.dismiss()
            createBid()
        }

        btnCancel.setOnClickListener { mBottomSheetDialog.dismiss() }

        mBottomSheetDialog.show()
    }

    private fun createBid(){
        val jsonArray = JSONArray()
        try {
            for (i in 0 until bidDataList.size) {
                val internalObject = JSONObject()
                internalObject.put("session", bidDataList[i].session)
                internalObject.put("open_digit", bidDataList[i].open_digit)
                internalObject.put("close_digit", bidDataList[i].close_digit)
                internalObject.put("open_pana", bidDataList[i].open_pana)
                internalObject.put("close_pana", bidDataList[i].close_pana)
                internalObject.put("points", bidDataList[i].points)
                jsonArray.put(internalObject)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.e("json",jsonArray.toString())
        progressView.view()
        val call: Call<MessageData?>? = com.mkxonline.smapp.api.ApiMain.getClient.createBid("Bearer "+helper.getAuth().toString(),listDetails.id!!,dataType.id!!, jsonArray.toString())
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@BidActivity)
                }else if(response.body()!!.error == false){
                    dashboard(0)
                    bidDialog(response.body()!!.message!!)
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    fun bidDialog(message: String) {
        val builder = AlertDialog.Builder(this@BidActivity)
        builder.setTitle("Success!")
        builder.setCancelable(false)
        builder.setMessage(message)
        builder.setPositiveButton("Play Again") { dialog, which ->
            recreate()
            dialog.dismiss()
        }

        builder.setNegativeButton("Done") { dialog, which ->

            dialog.dismiss()
            finish()
        }
        if (!builder.create().isShowing) builder.create().show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        txtPoints = menu!!.findItem(R.id.menu_wallet).actionView!!.findViewById(R.id.txt_current_balance)
        txtPoints.text = Utility.decimalFormat(helper.getBalance()!!)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}