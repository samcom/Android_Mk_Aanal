package com.mkxonline.smapp.ui.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.api.model.DashboardData
import com.mkxonline.smapp.api.model.WalletHistoryData
import com.mkxonline.smapp.apputil.interfaces.ToolbarTitleInterface
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.ui.adapter.WalletHistoryAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WalletHistoryFragment : Fragment() {

    private val dataDetails: ArrayList<WalletHistoryData.Data.Result.DataItem> = ArrayList()
    private lateinit var walletHistoryAdapter: WalletHistoryAdapter
    lateinit var progressView: ProgressView
    private lateinit var rvWalletHistory : RecyclerView
    lateinit var helper: SessionManager

    private lateinit var swipeContainer: SwipeRefreshLayout

    private lateinit var toolbarTitleInterface: ToolbarTitleInterface

    lateinit var txtCurrentBalance : TextView

    private var currentPage = 1
    private var lastPage = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_wallet_history, container, false)

        rvWalletHistory = view.findViewById(R.id.rv_wallet_history)
        rvWalletHistory.layoutManager = LinearLayoutManager(activity)
        rvWalletHistory.itemAnimator = DefaultItemAnimator()

        progressView = ProgressView(requireActivity())
        helper = SessionManager(requireActivity())

        activity?.let {
            instantiateNavigationInterface(it)
        }

        walletHistoryAdapter = WalletHistoryAdapter(dataDetails)
        rvWalletHistory.adapter = walletHistoryAdapter

        rvWalletHistory.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    if (currentPage == lastPage) {
                        return
                    }
                    getWalletHistory(++currentPage)
                }
            }
        })

        swipeContainer = view.findViewById(R.id.swipeContainer)
        swipeContainer.setOnRefreshListener {
            onResume()
        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, DashboardFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })

        return view
    }

    override fun onResume() {
        super.onResume()
        if (Utility.isInternetConnection(requireActivity())) {
            getWalletHistory(currentPage)
            dashboard()
        }else{
            Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internet_msg))
        }

        toolbarTitleInterface.changeTitle("My Wallet")
    }

    private fun instantiateNavigationInterface(context: FragmentActivity) {
        toolbarTitleInterface = context as ToolbarTitleInterface
    }

    private fun dashboard() {
        progressView.view()
        val call: Call<DashboardData?>? = com.mkxonline.smapp.api.ApiMain.getClient.dashboard("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<DashboardData?> {
            override fun onResponse(call: Call<DashboardData?>, response: Response<DashboardData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false
                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(
                        R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    helper.setName(response.body()!!.data!!.result!!.name)
                    helper.setPhone(response.body()!!.data!!.result!!.phoneNumber)
                    helper.setProfilePic(response.body()!!.data!!.result!!.profilePicture)
                    helper.setEmail(response.body()!!.data!!.result!!.email)
                    helper.setBalance(response.body()!!.data!!.result!!.availableBalance)
                    helper.setTransfer(response.body()!!.data!!.result!!.transfer)
                    helper.setBetting(response.body()!!.data!!.result!!.betting)
                    helper.setNotifyStatus(response.body()!!.data!!.result!!.notiStatus)

                    Handler(Looper.getMainLooper()).postDelayed({
                        txtCurrentBalance.text = Utility.decimalFormat(helper.getBalance()!!)
                    }, 500)

                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<DashboardData?>, t: Throwable) {
                progressView.hide()
                swipeContainer.isRefreshing = false
            }
        })

    }

    private fun getWalletHistory(page: Int){
        if (page == 1) {
            dataDetails.clear()
            walletHistoryAdapter.notifyDataSetChanged()
        }
        progressView.view()

        val call: Call<WalletHistoryData?>? = com.mkxonline.smapp.api.ApiMain.getClient.walletHistory("Bearer "+helper.getAuth().toString(),page.toString())
        call!!.enqueue(object : Callback<WalletHistoryData?> {
            override fun onResponse(call: Call<WalletHistoryData?>, response: Response<WalletHistoryData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false

                if(response.code() == 500){
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(requireActivity())
                }else if(response.body()!!.error == false){
                    if (isAttachedToActivity()) {
                        currentPage = response.body()!!.data!!.result!!.currentPage!!
                        lastPage = response.body()!!.data!!.result!!.lastPage!!
                        dataDetails.addAll(response.body()!!.data!!.result!!.data!!)
                        walletHistoryAdapter.notifyDataSetChanged()
                    }

                }else{
                    Utility.errorSnackBar(requireActivity().findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<WalletHistoryData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_main, menu)
        txtCurrentBalance = menu.findItem(R.id.menu_wallet).actionView!!.findViewById(R.id.txt_current_balance)
    }

    fun isAttachedToActivity(): Boolean {
        return isVisible && activity != null
    }
}