package com.mkxonline.smapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.BidHistoryData
import com.mkxonline.smapp.apputil.Utility

class BidHistoryAdapter(private var arrayList: ArrayList<BidHistoryData.Data.Result.DataItem>) : RecyclerView.Adapter<BidHistoryAdapter.ViewHolderContact>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderContact {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_bid_history, parent, false)
        return ViewHolderContact(itemView)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(viewholder: ViewHolderContact, position: Int) {

        val s = arrayList[position]

        viewholder.txtName.text = s.gameName!!.toUpperCase() + "("+s.type+")"
        viewholder.txtPoints.text = Utility.decimalFormat(s.points!!)+" Points"
        viewholder.txtMarketInfo.text = "Session: "+s.session
        viewholder.txtTime.text = s.bidDate
        viewholder.txtTax.text = "#"+s.txId

        if(s.type == Utility.SINGLE_DIGIT){
            if(s.session == "open"){
                viewholder.txtOpenClose.text = "Single Digit: "+s.openDigit
            }else{
                viewholder.txtOpenClose.text = "Single Digit: "+s.closeDigit
            }
        }else if (s.type == Utility.JODI_DIGIT){
            viewholder.txtOpenClose.text = "Jodi Digit: "+s.openDigit
        }else if(s.type == Utility.SINGLE_PANA){
            if(s.session == "open"){
                viewholder.txtOpenClose.text = "Single Pana: "+s.openPana
            }else{
                viewholder.txtOpenClose.text = "Single Pana: "+s.closePana
            }
        }else if(s.type == Utility.DOUBLE_PANA){
            if(s.session == "open"){
                viewholder.txtOpenClose.text = "Double Pana: "+s.openPana
            }else{
                viewholder.txtOpenClose.text = "Double Pana: "+s.closePana
            }
        }else if(s.type == Utility.TRIPLE_PANA){
            if(s.session == "open"){
                viewholder.txtOpenClose.text = "Triple Pana: "+s.openPana
            }else{
                viewholder.txtOpenClose.text = "Triple Pana: "+s.closePana
            }
        }else if(s.type == Utility.HALF_SANGAM){
            if(s.session == "open"){
                viewholder.txtOpenClose.text = "Half Digit: "+s.openDigit + "| Half Pana: "+s.closePana
            }else{
                viewholder.txtOpenClose.text = "Half Pana: "+s.openPana +"| Half Digit: "+s.closeDigit
            }
        }else if(s.type == Utility.FULL_SANGAM){
            viewholder.txtOpenClose.text = "Full Pana: "+s.openPana +"| Full Pana: "+s.closePana
        }
    }

    class ViewHolderContact(view: View) : RecyclerView.ViewHolder(view) {
        var txtName: TextView = view.findViewById(R.id.txt_list_name)
        var txtOpenClose: TextView = view.findViewById(R.id.txt_open_close)
        var txtMarketInfo: TextView = view.findViewById(R.id.txt_market_info)
        var txtPoints: TextView = view.findViewById(R.id.txt_points)
        var txtTime: TextView = view.findViewById(R.id.txt_time)
        var txtTax: TextView = view.findViewById(R.id.txt_tax)
    }
}
