package com.mkxonline.smapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.github.hariprasanths.bounceview.BounceView
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.ListData
import com.mkxonline.smapp.api.model.ListDetailData
import com.mkxonline.smapp.api.model.TypeData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.util.*

class StarLineGameActivity : AppCompatActivity() {

    private lateinit var imgSingleDigit: ImageView
    private lateinit var imgSinglePana: ImageView
    private lateinit var imgDoublePana: ImageView
    private lateinit var imgTriplePana: ImageView

    lateinit var listDetails: ListData.Data.ResultItem
    private val dataDetails: ArrayList<TypeData.Data.ResultItem> = ArrayList()

    private lateinit var swipeContainer: SwipeRefreshLayout

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView

    private lateinit var toolbar: Toolbar
    private lateinit var toolbarTitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_starline_list)

        listDetails = intent?.extras!!.getSerializable("GameDetails") as ListData.Data.ResultItem

        progressView = ProgressView(this@StarLineGameActivity)
        helper = SessionManager(this@StarLineGameActivity)

        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.toolbar_title)

        setSupportActionBar(toolbar)
        supportActionBar!!.title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        toolbarTitle.text = listDetails.openTime

        imgSingleDigit = findViewById(R.id.img_single_digit)
        imgSinglePana = findViewById(R.id.img_single_pana)
        imgDoublePana = findViewById(R.id.img_double_pana)
        imgTriplePana = findViewById(R.id.img_triple_pana)

        BounceView.addAnimTo(imgSingleDigit)
        BounceView.addAnimTo(imgSinglePana)
        BounceView.addAnimTo(imgDoublePana)
        BounceView.addAnimTo(imgTriplePana)

        swipeContainer = findViewById(R.id.swipeContainer)
        swipeContainer.setOnRefreshListener {
            onResume()
        }

        imgSingleDigit.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.SINGLE_DIGIT)){
                    onClickType(item)
                    break
                }
            }
        }

        imgSinglePana.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.SINGLE_PANA)){
                    onClickType(item)
                    break
                }
            }
        }

        imgDoublePana.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.DOUBLE_PANA)){
                    onClickType(item)
                    break
                }
            }
        }

        imgTriplePana.setOnClickListener {
            for (item in dataDetails) {
                if(item.type.equals(Utility.TRIPLE_PANA)){
                    onClickType(item)
                    break
                }
            }
        }

    }

    private fun onClickType(s: TypeData.Data.ResultItem){
        startActivity(
            Intent(this@StarLineGameActivity, StarLineBidActivity::class.java)
                .putExtra("GameType",s)
                .putExtra("GameDetails",listDetails))
    }

    override fun onResume() {
        super.onResume()

        if (Utility.isInternetConnection(this@StarLineGameActivity)) {
            getType()
            listDetails()
        }else{
            Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internet_msg))
        }
    }

    private fun getType(){
        progressView.view()
        dataDetails.clear()
        val call: Call<TypeData?>? = com.mkxonline.smapp.api.ApiMain.getClient.starlineGameType("Bearer "+helper.getAuth().toString())
        call!!.enqueue(object : Callback<TypeData?> {
            override fun onResponse(call: Call<TypeData?>, response: Response<TypeData?>) {
                progressView.hide()
                swipeContainer.isRefreshing = false

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@StarLineGameActivity)
                }else if(response.body()!!.error == false){
                    dataDetails.addAll(response.body()!!.data!!.result!!)
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<TypeData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    private fun listDetails(){
        progressView.view()
        val call: Call<ListDetailData?>? = com.mkxonline.smapp.api.ApiMain.getClient.starlineGameDetails("Bearer "+helper.getAuth().toString(),listDetails.id!!)
        call!!.enqueue(object : Callback<ListDetailData?> {
            override fun onResponse(call: Call<ListDetailData?>, response: Response<ListDetailData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.code() == 401){
                    Utility.unAuthPopUp(this@StarLineGameActivity)
                }else if(response.body()!!.error == false){
                    listDetails.openTime = response.body()!!.data!!.result!!.openTime!!
                    try {
                        val endDate = Utility.mFormatDate.parse(listDetails.openTime)
                        val calendarEnd = Calendar.getInstance()

                        calendarEnd.time = endDate
                        calendarEnd.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
                        calendarEnd.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                        calendarEnd.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

                        if (!Calendar.getInstance().time.before(calendarEnd.time)) {
                            onBackPressed()
                        }

                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<ListDetailData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}