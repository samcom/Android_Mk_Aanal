package com.mkxonline.smapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.material.textfield.TextInputEditText
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.ApiMain.getClient
import com.mkxonline.smapp.api.model.MessageData
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.customview.ProgressView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OTPVerifyActivity : AppCompatActivity() {

    var number=""
    var type=""
    private lateinit var txtNumber: TextView
    private lateinit var pinView: TextInputEditText
    lateinit var progressView: ProgressView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verify)

        progressView = ProgressView(this@OTPVerifyActivity)
        number = intent?.extras!!.getString("number").toString()
        type = intent?.extras!!.getString("type").toString()

        txtNumber = findViewById(R.id.txt_number)
        pinView = findViewById(R.id.et_pin)

        txtNumber.text = number

        findViewById<TextView>(R.id.txt_resend).setOnClickListener {
            sendOTP()
        }

        BounceView.addAnimTo(findViewById<AppCompatButton>(R.id.btn_continue))
        findViewById<AppCompatButton>(R.id.btn_continue).setOnClickListener {
            if(pinView.text.toString().isNotEmpty() && pinView.text.toString().length == 4){
                verifyOTP(pinView.text.toString())
            }else{
                Utility.errorSnackBar(findViewById(android.R.id.content),"Please Enter OTP")
            }
        }
    }

    private fun sendOTP() {
        progressView.view()
        val call: Call<MessageData?>? = getClient.sendotp(number,type)
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                } else if(response.body()!!.error == true){
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
                
                val returnIntent = Intent()
                setResult(RESULT_CANCELED, returnIntent)
                finish()
            }
        })
    }

    private fun verifyOTP(otp: String) {
        progressView.view()
        val call: Call<MessageData?>? = getClient.verifyotp(number,otp)
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }else if(response.body()!!.error == false){
                    val returnIntent = Intent()
                    setResult(RESULT_OK, returnIntent)
                    finish()
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }
            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
                
            }
        })
    }
}