package com.mkxonline.smapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.hariprasanths.bounceview.BounceView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.mkxonline.smapp.api.model.StarLineBidData
import com.mkxonline.smapp.R
import com.mkxonline.smapp.api.model.*
import com.mkxonline.smapp.apputil.Utility
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView
import com.mkxonline.smapp.ui.adapter.StarLineBidAdapter
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class StarLineBidActivity : AppCompatActivity() {

    private var dataDetails: ArrayList<String> = ArrayList()
    private var bidList: ArrayList<StarLineBidData> = ArrayList()
    private lateinit var txtPoints: TextView

    private lateinit var etDate: TextInputEditText
    private lateinit var etOpenDigit: AutoCompleteTextView
    private lateinit var etOpenPana: AutoCompleteTextView

    private lateinit var etPoints: TextInputEditText
    private lateinit var tlyOpenDigit: TextInputLayout
    private lateinit var tlyOpenPana: TextInputLayout

    private lateinit var btnProcess: AppCompatButton
    private lateinit var btnSubmit: AppCompatButton

    private var minPoints = 0
    private var maxPoints = 0

    lateinit var bidAdapter: StarLineBidAdapter

    lateinit var progressView: ProgressView
    private lateinit var rvBid: RecyclerView
    lateinit var helper: SessionManager
    private lateinit var dataType: TypeData.Data.ResultItem
    private lateinit var listDetails: ListData.Data.ResultItem

    private lateinit var toolbar: Toolbar
    private lateinit var toolbarTitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_starline_bid)

        dataType = intent.extras!!.getSerializable("GameType") as TypeData.Data.ResultItem
        listDetails = intent.extras!!.getSerializable("GameDetails") as ListData.Data.ResultItem

        progressView = ProgressView(this@StarLineBidActivity)
        helper = SessionManager(this@StarLineBidActivity)

        rvBid = findViewById(R.id.rv_bid)

        etDate = findViewById(R.id.et_date)
        etOpenDigit = findViewById(R.id.et_open_digit)
        etOpenPana = findViewById(R.id.et_open_pana)

        tlyOpenDigit = findViewById(R.id.tly_open_digit)
        tlyOpenPana = findViewById(R.id.tly_open_pana)
        etPoints = findViewById(R.id.et_points)
        btnProcess = findViewById(R.id.btn_process)
        btnSubmit = findViewById(R.id.btn_submit)

        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.toolbar_title)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbarTitle.text = dataType.type

        rvBid.layoutManager = LinearLayoutManager(this@StarLineBidActivity)
        rvBid.itemAnimator = DefaultItemAnimator()

        bidAdapter = StarLineBidAdapter(bidList ,dataType.type!!,object : StarLineBidAdapter.Callbacks {
            override fun onClick(s: StarLineBidData) {
                bidList.remove(s)
                bidAdapter.notifyDataSetChanged()

                if (bidList.size == 0){

                    btnSubmit.visibility = View.GONE
                }
            }

        })
        rvBid.adapter = bidAdapter

        if(dataType.type.equals(Utility.SINGLE_PANA) || dataType.type.equals(Utility.DOUBLE_PANA) || dataType.type.equals(
                Utility.TRIPLE_PANA)){
            tlyOpenDigit.visibility = View.GONE
            tlyOpenPana.visibility = View.VISIBLE
        }

        val df = SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault())
        val formattedDate: String = df.format(Calendar.getInstance().time)
        etDate.setText(formattedDate)

        etOpenDigit.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) etOpenDigit.hint =
                "" else etOpenDigit.hint = getString(R.string.hint_open_digit)
        }

        etOpenPana.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) etOpenPana.hint = "" else etOpenPana.hint = getString(R.string.hint_open_pana)
        }

        BounceView.addAnimTo(btnProcess)
        btnProcess.setOnClickListener {
            if(!validPoints()){
                return@setOnClickListener
            }

            if (Utility.isInternetConnection(this@StarLineBidActivity)) {
                dashboard(1)
            }else{
                Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internet_msg))
            }
        }

        BounceView.addAnimTo(btnSubmit)
        btnSubmit.setOnClickListener {
            listDataDetails()
        }

        if (Utility.isInternetConnection(this@StarLineBidActivity)) {
            getDigit()
            dashboard(0)
        }else{
            Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internet_msg))
        }
    }

    private fun validPoints(): Boolean {
        return if(etPoints.text.toString().isEmpty()){
            Utility.errorSnackBar(findViewById(android.R.id.content),"Please Enter Valid Points")
            false
        }else{
            val points: Int = etPoints.text.toString().trim { it <= ' ' }.toInt()
            if (points in (minPoints) until maxPoints) {
                true
            } else {
                Utility.errorSnackBar(findViewById(android.R.id.content),"Please Enter Valid Points")
                false
            }
        }
    }

    private fun getDigit(){
        progressView.view()

        lateinit var call: Call<DigitPanaData?>

        if (dataType.type == Utility.SINGLE_PANA){
            call = com.mkxonline.smapp.api.ApiMain.getClient.singlePana("Bearer "+helper.getAuth().toString())!!
        }else if (dataType.type == Utility.DOUBLE_PANA){
            call = com.mkxonline.smapp.api.ApiMain.getClient.doublePana("Bearer "+helper.getAuth().toString())!!
        }else if (dataType.type == Utility.TRIPLE_PANA){
            call = com.mkxonline.smapp.api.ApiMain.getClient.triplePana("Bearer "+helper.getAuth().toString())!!
        }else{
            call = com.mkxonline.smapp.api.ApiMain.getClient.singleDigit("Bearer "+helper.getAuth().toString())!!
        }

        call.enqueue(object : Callback<DigitPanaData?> {
            override fun onResponse(call: Call<DigitPanaData?>, response: Response<DigitPanaData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@StarLineBidActivity)
                }else if(response.body()!!.error == false){
                    val datas: ArrayList<DigitPanaData.Data.ResultItem> = ArrayList()
                    datas.addAll(response.body()!!.data!!.result!!)

                    minPoints = response.body()!!.data!!.minPoint!!.toInt()
                    maxPoints = response.body()!!.data!!.maxPoint!!.toInt()

                    for (item in datas){
                        dataDetails.add(item.number!!)
                    }

                    val openAdapter = ArrayAdapter(this@StarLineBidActivity, android.R.layout.simple_list_item_1, dataDetails)
                    etOpenDigit.setAdapter(openAdapter)
                    etOpenDigit.threshold = 1

                    etOpenPana.setAdapter(openAdapter)
                    etOpenPana.threshold = 1

                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<DigitPanaData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    private fun dashboard(i: Int) {
        progressView.view()
        val call: Call<DashboardData?>? = com.mkxonline.smapp.api.ApiMain.getClient.dashboard("Bearer "+helper.getAuth().toString())

        call!!.enqueue(object : Callback<DashboardData?> {
            override fun onResponse(call: Call<DashboardData?>, response: Response<DashboardData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@StarLineBidActivity)
                }else if(response.body()!!.error == false){
                    helper.setBalance(response.body()!!.data!!.result!!.availableBalance)

                    if(i==1){
                        if(etPoints.text.toString().toInt() > helper.getBalance()!!.toInt()){
                            Utility.errorSnackBar(findViewById(android.R.id.content),"Not that much balance")
                            return
                        }

                        if(bidList.size > 0 ){
                            var balance = 0
                            for (i in 0 until bidList.size) {
                                balance = balance.plus(bidList[i].points.toInt())
                            }
                            val cBalance = helper.getBalance()!!.toInt() - balance

                            if(etPoints.text.toString().toInt() > cBalance){
                                Utility.errorSnackBar(findViewById(android.R.id.content),"Not that much balance")
                                return
                            }
                        }

                        addBid()
                    }else{
                        Handler(Looper.getMainLooper()).postDelayed({
                            txtPoints.text = Utility.decimalFormat(helper.getBalance()!!)
                        }, 500)
                    }

                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<DashboardData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    private fun addBid(){
        val b = StarLineBidData()

        var openDigit = ""
        var openPana = ""

        if(dataType.type == Utility.SINGLE_DIGIT){
            openDigit = etOpenDigit.text.toString()
            if(!validEdittext(etOpenDigit,dataDetails)){
                return
            }
        }else if(dataType.type == Utility.SINGLE_PANA || dataType.type == Utility.DOUBLE_PANA || dataType.type == Utility.TRIPLE_PANA){
            openPana = etOpenPana.text.toString()
            if(!validEdittext(etOpenPana,dataDetails)){
                return
            }
        }

        b.open_pana = openPana
        b.open_digit = openDigit
        b.points = etPoints.text.toString()

        bidList.add(b)
        btnSubmit.visibility = View.VISIBLE
        bidAdapter.notifyDataSetChanged()

        etOpenDigit.setText("")
        etOpenPana.setText("")
        etPoints.setText("")
    }

    private fun validEdittext(et : AutoCompleteTextView,dataDetails: ArrayList<String>) : Boolean {
        val openDigit = et.text.toString()
        return if (openDigit.isEmpty()){
            Utility.errorSnackBar(findViewById(android.R.id.content),"Please Enter Digit")
            false
        }else if(!dataDetails.contains(openDigit)){
            Utility.errorSnackBar(findViewById(android.R.id.content),"Digit not Allowed")
            false
        }else{
            true
        }
    }

    private fun listDataDetails(){
        progressView.view()
        val call: Call<ListDetailData?>? = com.mkxonline.smapp.api.ApiMain.getClient.starlineGameDetails("Bearer "+helper.getAuth().toString(),listDetails.id!!)
        call!!.enqueue(object : Callback<ListDetailData?> {
            override fun onResponse(call: Call<ListDetailData?>, response: Response<ListDetailData?>) {
                progressView.hide()
                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }
                if(response.code() == 401){
                    Utility.unAuthPopUp(this@StarLineBidActivity)
                }else if(response.body()!!.error == false){
                    listDetails.openTime = response.body()!!.data!!.result!!.openTime!!

                    try {
                        val endDate = Utility.mFormatDate.parse(response.body()!!.data!!.result!!.openTime!!)
                        val calendarEnd = Calendar.getInstance()

                        calendarEnd.time = endDate
                        calendarEnd.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH))
                        calendarEnd.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                        calendarEnd.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR))

                        if(Calendar.getInstance().time.before(calendarEnd.time)) {
                            createBid()
                        } else {
                            Utility.errorSnackBar(findViewById(android.R.id.content),"Open Session Closed for Bid")
                            finish()
                            startActivity(Intent(this@StarLineBidActivity, MainActivity::class.java))
                        }

                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<ListDetailData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    private fun createBid(){
        val jsonArray = JSONArray()
        try {
            for (i in 0 until bidList.size) {
                val internalObject = JSONObject()
                internalObject.put("open_digit", bidList[i].open_digit)
                internalObject.put("open_pana", bidList[i].open_pana)
                internalObject.put("points", bidList[i].points)
                jsonArray.put(internalObject)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        progressView.view()
        val call: Call<MessageData?>? = com.mkxonline.smapp.api.ApiMain.getClient.starlineCreateBid("Bearer "+helper.getAuth().toString(),listDetails.id!!,dataType.id!!, jsonArray.toString())
        call!!.enqueue(object : Callback<MessageData?> {
            override fun onResponse(call: Call<MessageData?>, response: Response<MessageData?>) {
                progressView.hide()

                if(response.code() == 500){
                    Utility.errorSnackBar(findViewById(android.R.id.content),getString(R.string.internal_server_msg))
                    return
                }

                if(response.code() == 401){
                    Utility.unAuthPopUp(this@StarLineBidActivity)
                }else if(response.body()!!.error == false){
                    dashboard(0)
                    bidDialog(response.body()!!.message!!)
                }else{
                    Utility.errorSnackBar(findViewById(android.R.id.content), response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<MessageData?>, t: Throwable) {
                progressView.hide()
            }
        })
    }

    fun bidDialog(message: String) {
        val builder = AlertDialog.Builder(this@StarLineBidActivity)
        builder.setTitle("Success!")
        builder.setCancelable(false)
        builder.setMessage(message)
        builder.setPositiveButton("Play Again") { dialog, which ->
            recreate()
            dialog.dismiss()
        }

        builder.setNegativeButton("Done") { dialog, which ->

            dialog.dismiss()
            finish()
        }
        if (!builder.create().isShowing) builder.create().show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        txtPoints = menu!!.findItem(R.id.menu_wallet).actionView!!.findViewById(R.id.txt_current_balance)
        txtPoints.text = Utility.decimalFormat(helper.getBalance()!!)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}