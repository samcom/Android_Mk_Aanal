package com.mkxonline.smapp.ui.fragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.mkxonline.smapp.R
import com.mkxonline.smapp.apputil.SessionManager
import com.mkxonline.smapp.apputil.customview.ProgressView

class MethodFragment : Fragment() {

    lateinit var helper: SessionManager
    lateinit var progressView: ProgressView
    private lateinit var llgpay: ImageView
    private lateinit var llphonepe: ImageView
    private lateinit var llpaytm: ImageView

    var number = ""
    var s = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_method, container, false)

        helper = SessionManager(requireActivity())
        progressView = ProgressView(requireActivity())

        llgpay = view.findViewById(R.id.llgpay)
        llphonepe = view.findViewById(R.id.llphonepe)
        llpaytm = view.findViewById(R.id.llpaytm)

        llgpay.setOnClickListener {
            val dialogFragment = NumberFragment.newInstance(2)
            dialogFragment.isCancelable = true
            dialogFragment.show(requireActivity().supportFragmentManager, "My  Fragment")
        }

        llphonepe.setOnClickListener {
            val dialogFragment = NumberFragment.newInstance(3)
            dialogFragment.isCancelable = true
            dialogFragment.show(requireActivity().supportFragmentManager, "My  Fragment")
        }

        llpaytm.setOnClickListener {
            val dialogFragment = NumberFragment.newInstance(1)
            dialogFragment.isCancelable = true
            dialogFragment.show(requireActivity().supportFragmentManager, "My  Fragment")
        }

//        llbank.setOnClickListener {
//            val fragmentManager = requireActivity().supportFragmentManager
//            fragmentManager.beginTransaction()
//                .replace(R.id.content_frame, AddBankFragment()).commit()
//        }

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame, WalletFragment())
                    .commit()
                return@OnKeyListener true
            }
            false
        })
        return view
    }
}